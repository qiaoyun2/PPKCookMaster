//
//  AddressListView.h
//  PPK
//
//  Created by null on 2022/4/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddressListView : UIView

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
