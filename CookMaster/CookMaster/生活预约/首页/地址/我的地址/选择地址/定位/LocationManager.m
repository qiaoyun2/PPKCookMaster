//
//  LocationManager.m
//  ZZR
//
//  Created by null on 2019/9/3.
//  Copyright © 2019 null. All rights reserved.
//

#import "LocationManager.h"
#import "UIAlertController+Category.h"

#define CityNotifice @"cityNotifice"

@interface LocationManager ()<CLLocationManagerDelegate>
@property (nonatomic,copy) LocationManagerBlock locationManageBlock;
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation LocationManager
static LocationManager*manager=nil;

+(LocationManager*)shareManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (manager==nil) {
            manager=[[self alloc] init];
        }
    });
    return manager;
}
/**
 开始定位
 
 @param location BOOL 开始定位
 */
-(void)startLocation:(BOOL)location
{
    if (location)
    {
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
            
            NSLog(@"%@",[NSString stringWithFormat:@"请开启定位:设置 > 隐私 > 定位服务 > %@",[LocationManager getCFBundleDisplayName]]);
            [UIAlertController alertViewWithTitle:@"定位权限未开启" message:[NSString stringWithFormat:@"请开启定位:设置 > 隐私 > 定位服务 > %@",[LocationManager getCFBundleDisplayName]] block:^{
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                if ([[UIApplication sharedApplication] canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }
                
            }];
            
        }
        //开始定位
        [self.locationManager startUpdatingLocation];
    }
}

+(NSString *)getCFBundleDisplayName
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Name=@"";
    NSString * CFBundleDisplayName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString * CFBundleName = [infoDictionary objectForKey:@"CFBundleName"];
    if (CFBundleName!=nil&&CFBundleName.length!=0) {
        app_Name = CFBundleName;
    }else if (CFBundleDisplayName!=nil&&CFBundleDisplayName.length!=0) {
        app_Name = CFBundleDisplayName;
    }
    return app_Name;
}
/**
 开始定位
 
 @param location BOOL 开始定位
 */
-(void)startLocation:(BOOL)location withlocationManageBlock:(LocationManagerBlock)block
{
    if (location)
    {
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
            
            NSLog(@"%@",[NSString stringWithFormat:@"请开启定位:设置 > 隐私 > 定位服务 > %@",[LocationManager getCFBundleDisplayName]]);
            [UIAlertController alertViewWithTitle:@"定位权限未开启" message:[NSString stringWithFormat:@"请开启定位:设置 > 隐私 > 定位服务 > %@",[LocationManager getCFBundleDisplayName]] block:^{
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                if ([[UIApplication sharedApplication] canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }
                
            }];
            
        }
        //开始定位
        [self.locationManager startUpdatingLocation];
    }
    [self setLocationManageBlock:block];
}

#pragma mark 定位成功
#pragma mark -- CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if (locations && locations.count > 0) {
        for (CLLocation *location in locations) {
            // 根据获取到的location实例，反编译地理位置信息
            [self reverseGeocodeWithLocation:location];
        }
        // 根据需要，获取位置成功后是否要停止定位
        [self.locationManager stopUpdatingLocation];
    }
}
#pragma mark 定位失败
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"error:%@",error);
}
#pragma mark 反编译地理信息
- (void) reverseGeocodeWithLocation:(CLLocation *) location {
    
    if (!location) {
        return ;
    }
    
    CLGeocoder *coder = [[CLGeocoder alloc]init];
    [coder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        
        if (placemarks && placemarks.count > 0) {
            
            if (error){
                NSLog(@"Geocode failed with error: %@", error);
                return;
            }
            
            CLPlacemark *placemark = [placemarks firstObject];
            NSString *province = placemark.addressDictionary[@"State"];
            NSString *city = placemark.addressDictionary[@"City"];
            NSString *area = placemark.addressDictionary[@"SubLocality"];
            NSString *adress = @"";
            NSArray * addressArray = placemark.addressDictionary[@"FormattedAddressLines"];
            if ([addressArray isKindOfClass:[NSArray class]]&&addressArray.count) {
                adress = addressArray[0];
            }
            NSLog(@"address====%@",adress);
//            NSDictionary *locationsDic = @{@"pro":province,@"city":city,@"area":area,@"lat":[NSString stringWithFormat:@"%f",location.coordinate.latitude],@"lng":[NSString stringWithFormat:@"%f",location.coordinate.longitude]};
//            [[NSUserDefaults standardUserDefaults] setObject:locationsDic forKey:UserLocation];
//            [[NSUserDefaults standardUserDefaults] setObject:province forKey:LocationProvince];
//            [[NSUserDefaults standardUserDefaults] setObject:province forKey:CurrentProvince];
//            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:LocationChangeSuccess object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:CityNotifice object:nil];
            [[LJTools getAppDelegate] setLatitude:location.coordinate.latitude];
            [[LJTools getAppDelegate] setLongitude:location.coordinate.longitude];
            [[LJTools getAppDelegate] setCity:city];
            [[LJTools getAppDelegate] setAdress:adress];

            if (self.locationManageBlock)
            {
                self.locationManageBlock(YES, location, province, city, area,adress, location.coordinate.latitude, location.coordinate.longitude);
                
            }
        }
    }];
}
#pragma mark - Getter /  Setter
-(CLLocationManager *)locationManager
{
    if (!_locationManager)
    {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.distanceFilter = 50;
        _locationManager.pausesLocationUpdatesAutomatically = NO;
        [_locationManager  setDelegate:self];
    }
    return _locationManager;
}

@end
