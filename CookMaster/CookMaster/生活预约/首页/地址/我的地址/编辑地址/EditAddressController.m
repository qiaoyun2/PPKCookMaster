//
//  EditAddressController.m
//  ZZR
//
//  Created by null on 2020/12/3.
//

#import "EditAddressController.h"
#import "UIView+xib.h"
#import "AddAddressTagPopView.h"
#import "zhPopupController.h"
#import "SelectAddressViewController.h"
//#import <zhPopupController/zhPopupController.h>

@interface EditAddressController ()
{
    NSMutableArray *tags;
    NSMutableArray *btns;
    NSString *selectLableTag;
}
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UIButton *manButton;
@property (weak, nonatomic) IBOutlet UIButton *womanButton;
@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet UIButton *cityButton;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@property (weak, nonatomic) IBOutlet UISwitch *defaultSwitch;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIView *textBgView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *tagView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagViewWidth;
@property (weak, nonatomic) IBOutlet UISwitch *sw;

@property (nonatomic, copy) NSArray <NSNumber *> *addressSelectIndexs;
@property (nonatomic,strong) AddAddressTagPopView *bottomV;

///纬度（垂直方向）
@property (nonatomic, copy) NSString *latitude;
///经度（水平方向）
@property (nonatomic, copy) NSString *longitude;

@end

@implementation EditAddressController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sw.transform = CGAffineTransformMakeScale(0.6, 0.6);
    tags = [NSMutableArray new];
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:@"addressTags"];
    if (array) {
        [tags addObjectsFromArray:array];
    }else{
        [tags addObjectsFromArray:@[@"公司",@"家",@"学校"]];
    }
    self.mobileField.jk_maxLength = 11;
    self.nameField.jk_maxLength = 11;

    btns = [NSMutableArray new];
    if (_isNew) {
        self.navigationItem.title = @"新增地址";
    }else{
        self.navigationItem.title = @"编辑地址";
        [self setNavigationRightBarButtonWithTitle:@"删除" color:RGB(51, 51, 51)];
        self.nameField.text = self.model.name;
        self.manButton.selected = self.model.sex.intValue == 1;
        self.womanButton.selected = !self.manButton.selected;
        self.mobileField.text = self.model.mobile;
        self.cityLabel.text = self.model.city;
        self.longitude = self.model.lng;
        self.latitude = self.model.lat;
        self.cityLabel.textColor = UIColorFromRGB(0x333333);

        self.cityButton.selected = YES;
        self.textView.text = self.model.address;
        [self.defaultSwitch setOn:[self.model.is_default integerValue]];
//        self.nameField.text = self.model.name;

    }
    [self initWithTags];
    [self createBottom];
}

- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender{
    [self deleteMyAddress];
}
#pragma mark – UI
- (void)initWithTags{
    [[NSUserDefaults standardUserDefaults] setObject:tags forKey:@"addressTags"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [_tagView removeAllSubviews];
    _tagViewWidth.constant = 0;
    [btns removeAllObjects];
    int a = (int)tags.count+1;
    int x = 0;
    CGFloat maxH = 0;
    for (int i = 0; i<a; i++) {
        if (i==a-1) {
            UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, 28, 20)];
            [btn setTitle:@"+" forState:0];

            [btn setTitleColor:UIColorFromRGB(0x333333) forState:0];
            [btn setCornerRadius:2];
            btn.titleLabel.font = [UIFont systemFontOfSize:12];
            btn.layer.borderWidth = 0.5f;
            btn.layer.borderColor = UIColorFromRGB(0xE5E5E5).CGColor;
            [_tagView addSubview:btn];
            [btn addTarget:self action:@selector(tagBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = 1;
            x+=36;
            maxH = CGRectGetMaxX(btn.frame);
        }else{
            NSString * str = tags[i];
            CGFloat str_w = [str textSizeIn:CGSizeMake(100, 20) font:[UIFont systemFontOfSize:12]].width;
            UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, str_w+10, 20)];
            [btn setTitle:str forState:0];
            [btn setTitleColor:UIColorFromRGB(0x333333) forState:0];
            btn.titleLabel.font = [UIFont systemFontOfSize:12];
            [btn setCornerRadius:2];
            btn.layer.borderWidth = 0.5f;
            btn.layer.borderColor = UIColorFromRGB(0xE5E5E5).CGColor;
            [_tagView addSubview:btn];
            [btn addTarget:self action:@selector(tagBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = 10+i;
            x+=str_w+10+8;
            [btns addObject:btn];
            if (self.model && [self.model.label isEqualToString:str]) {
                [self tagBtnClick:btn];
            }
        }
    }
    _tagViewWidth.constant = maxH;
}

#pragma mark – Network
- (void)deleteMyAddress{
    [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除该地址?" titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
        [NetworkingTool postWithUrl:@"" params:@{@"address_ids":(self.model.address_id)} success:^(NSURLSessionDataTask *task, id responseObject) {
            [LJTools hideHud];
            if ([responseObject[@"code"]intValue]==SUCCESS) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [LJTools showNOHud:responseObject[@"msg"] delay:1];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:1.0];
            
        } IsNeedHub:YES];
    } okColor:MainColor cancleColor:[UIColor lightGrayColor] isHaveCancel:YES];
}

#pragma mark – Delegate
#pragma mark - Function
-(void)tagBtnClick:(UIButton*)sender{
    [self.view endEditing:YES];
    MJWeakSelf;
    if (sender.tag == 1) {
        weakSelf.bottomV.textField.text=@"";
        [weakSelf.bottomV.textField becomeFirstResponder];
        //底部弹起
        weakSelf.zh_popupController = [zhPopupController popupControllerWithMaskType:zhPopupMaskTypeBlackTranslucent];
        weakSelf.zh_popupController.layoutType = zhPopupLayoutTypeBottom;
        [weakSelf.zh_popupController presentContentView:weakSelf.bottomV duration:0 springAnimated:NO];
        
    }else{
        for (UIButton * btn in btns) {
            [btn setTitleColor:UIColorFromRGB(0x333333)forState:0];
            btn.backgroundColor = [UIColor whiteColor];
            btn.layer.borderColor = UIColorFromRGB(0xE5E5E5).CGColor;
        }
        sender.backgroundColor = MainColor;
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        selectLableTag = sender.currentTitle;
    }
}

-(void)createBottom{
    _bottomV = [[[NSBundle mainBundle] loadNibNamed:@"AddAddressTagPopView" owner:nil options:nil] firstObject];
    _bottomV.frame=CGRectMake(0, 0, SCREEN_WIDTH, 50);
    WeakSelf
    _bottomV.textFieldEnd = ^(NSString * _Nonnull result) {
        if (![tags containsObject:result]) {
            [tags addObject:result];
            [weakSelf initWithTags];
     
        }else{
            [LJTools showText:@"已有相同标签" delay:2];
        }
        [weakSelf.zh_popupController dismiss];
    };
    //    _bottomV.sd_layout.bottomEqualToView(self.view).leftEqualToView(self.view).rightEqualToView(self.view).heightIs(50);
    //    _bottomV.hidden=YES;
}

#pragma mark – XibFunction
- (IBAction)sexButtonAction:(UIButton *)sender {
    [self.view endEditing:YES];
    sender.selected = YES;
    if (sender==self.manButton) {
        self.womanButton.selected = NO;
    }else{
        self.manButton.selected = NO;
    }
}
- (IBAction)cityButtonAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    MJWeakSelf;

    SelectAddressViewController *vc = [SelectAddressViewController new];
    vc.Block = ^(AMapPOI * _Nonnull model,NSString *detailAddress) {
        //            self.proviceStr = model.province;
        //            self.cityStr = model.city;
        //            self.areaStr = model.district;
        //            self.areaTF.text = [NSString stringWithFormat:@"%@ %@ %@",self.proviceStr,self.cityStr,self.areaStr];
   
        NSString *string = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.district,model.name];

        weakSelf.cityLabel.text = string;
        weakSelf.cityLabel.textColor = UIColorFromRGB(0x333333);
        weakSelf.cityButton.selected = YES;
        weakSelf.longitude = @(model.location.longitude).stringValue;
        weakSelf.latitude = @(model.location.latitude).stringValue;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)defaultButtonAction:(id)sender {
    [self.view endEditing:YES];
}

/** 添加地址 **/
/** 编辑地址 **/

- (IBAction)submitButtonAction:(UIButton *)sender {
    [self.view endEditing:YES];

    if ([self.nameField.text length]==0) {

        [LJTools showText:@"请输入收货人姓名" delay:2];

        return;
    }
    if ([self.mobileField.text length]==0) {
        [LJTools showText:@"请输入手机号" delay:2];

        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的手机号" delay:2];


        return;
    }
    if (!self.cityButton.selected) {
        [LJTools showText:@"请选择所在区域" delay:2];

        return;
    }
    if ([self.textView.text length]==0) {
        [LJTools showText:@"请输入详细地址" delay:2];


        return;
    }
//    JBHUD_hide_Message(@"提交");
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"name"] = self.nameField.text;
    params[@"mobile"] = self.mobileField.text;
    params[@"address"] = self.textView.text;
    params[@"sex"] = self.manButton.selected ? @(1):@(2);

    params[@"city"] = self.cityLabel.text;
    params[@"lng"] = self.longitude;
    params[@"lat"] = self.latitude;
    
    params[@"is_default"] = @(self.defaultSwitch.on);
    if (selectLableTag) {
        [params setValue:selectLableTag forKey:@"label"];
    }
    if (_isNew == YES) {
        
        //kPostAddAddress
        [NetworkingTool postWithUrl:@"" params:params success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([responseObject[@"code"]intValue]==SUCCESS) {
                if (self.onAddAddress) {
                    self.onAddAddress();
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }else{
                [LJTools showNOHud:responseObject[@"msg"] delay:1];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:1.0];
        } IsNeedHub:YES];
    }else{
        params[@"address_id"] = _model.address_id;
        [NetworkingTool postWithUrl:@"" params:params success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([responseObject[@"code"]intValue]==SUCCESS) {
                [LJTools showText:responseObject[@"msg"] delay:1];
                AddressModel * model = [AddressModel mj_objectWithKeyValues:params];
                if (self.onEditAddress) {
                    self.onEditAddress(model);
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }else{
                [LJTools showNOHud:responseObject[@"msg"] delay:1];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:1.0];

        } IsNeedHub:YES];
    }
}

@end
