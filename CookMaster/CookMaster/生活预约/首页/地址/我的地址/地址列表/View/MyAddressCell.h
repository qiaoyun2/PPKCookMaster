//
//  MyAddressCell.h
//  ZZR
//
//  Created by null on 2020/9/15.
//  Copyright © 2020 null. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIButton *defaultBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *defaultButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftSpace;

@property(nonatomic,strong)AddressModel * model;
@property(nonatomic,weak)id delegate;

@end

@protocol MyAddressCellDelegate <NSObject>

@optional

-(void)MyAddressCell:(MyAddressCell*)cell didSelectBtn:(UIButton*)sender;

@end
NS_ASSUME_NONNULL_END
