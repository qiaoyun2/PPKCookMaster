//
//  AddressModel.h
//  ZhongRenJuMei
//
//  Created by null on 2019/4/12.
//  Copyright © 2019 null. All rights reserved.
//


#import "BaseModel.h"
NS_ASSUME_NONNULL_BEGIN


@interface AddressModel :BaseModel

@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city_id;
@property (nonatomic, copy) NSString *district;
@property (nonatomic, strong) NSNumber *user_id;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, strong) NSNumber *province_id;
@property (nonatomic, strong) NSNumber *sort;
@property (nonatomic, copy) NSString *district_id;
@property (nonatomic, copy) NSString *postal_code;
@property (nonatomic, strong) NSNumber *sex;
@property (nonatomic, copy) NSString *lng;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSNumber *address_id;
@property (nonatomic, copy) NSString *national_code;
@property (nonatomic, copy) NSString *label;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, strong) NSNumber *model;
@property (nonatomic, copy) NSString *lat;
@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *is_default;


@end

NS_ASSUME_NONNULL_END
