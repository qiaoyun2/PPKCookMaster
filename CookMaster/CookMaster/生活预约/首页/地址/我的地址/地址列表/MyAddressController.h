//
//  MyAddressController.h
//  ZZR
//
//  Created by null on 2021/5/10.
//

#import "BaseViewController.h"
#import "AddressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyAddressController : BaseViewController
@property (nonatomic , assign) BOOL isorderJump; //是否从确认订单跳转
@property (nonatomic , copy) void (^selectAddressBlock)(AddressModel *addressModel,NSDictionary *addressDic);
@end

NS_ASSUME_NONNULL_END
