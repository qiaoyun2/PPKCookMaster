//
//  RWMapViewController.m
//  PPK
//
//  Created by null on 2022/4/1.
//

#import "RWMapViewController.h"

#import "LocationManager.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <MAMapKit/MAMapKit.h>

@interface RWMapViewController () <AMapSearchDelegate,MAMapViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UILabel *districtLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextField *addressField;
@property (weak, nonatomic) IBOutlet UITextField *doorNumField;
@property (weak, nonatomic) IBOutlet UITextField *mobileField;

@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) MAPointAnnotation *pointAnnotation;

@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *district;
@property (nonatomic, strong) NSString *address;

@property (nonatomic) CLLocationCoordinate2D coordinate;

@end

@implementation RWMapViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"选择位置";
    [self setNavigationRightBarButtonWithTitle:@"取消"];
    
    if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        NSLog(@"定位服务已启用");
        [[LocationManager shareManager]startLocation:YES withlocationManageBlock:^(BOOL isLocation, CLLocation * _Nonnull nowLocation, NSString * _Nonnull province, NSString * _Nonnull city, NSString * _Nonnull area, NSString * _Nonnull address, double lat, double lng) {
            if (isLocation) {
                [self initMapView];
            }
        }];
    } else {
        [LJTools showNOHud:@"请开启定位:设置 > 隐私 > 位置 > 定位服务" delay:1.0];
    }
    [self initMapView];
}

#pragma mark - UI
-(void)initMapView
{
    ///地图需要v4.5.0及以上版本才必须要打开此选项（v4.5.0以下版本，需要手动配置info.plist）
    [AMapServices sharedServices].enableHTTPS = YES;
    ///初始化地图
    self.mapView = [[MAMapView alloc] initWithFrame:self.view.bounds];
    [self.mapView setDelegate:self];
    self.mapView.zoomLevel = 17;
    ///把地图添加至view
    [self.view addSubview:self.mapView];
    [self.view sendSubviewToBack:self.mapView];
    ///如果您需要进入地图就显示定位小蓝点，则需要下面两行代码
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeNone;
    
//
//    MAPointAnnotation *a1= [[MAPointAnnotation alloc] init];
//    a1.coordinate = self.coordinate;
//    [self.mapView showAnnotations:@[a1] animated:YES];
    
    if (_pointAnnotation==nil) {
        _pointAnnotation = [[MAPointAnnotation alloc] init];
    }
//    self.coordinate = CLLocationCoordinate2DMake(113.687777,34.673016);
    _pointAnnotation.coordinate = self.coordinate;
    //设置地图的定位中心点坐标
    [self.mapView setCenterCoordinate:self.coordinate animated:YES];
    //将点添加到地图上，即所谓的大头针
    [self.mapView addAnnotation:_pointAnnotation];
}

#pragma mark - Delegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id)annotation {
    //大头针标注
    if ([annotation isKindOfClass:[MAPointAnnotation class]]) {//判断是否是自己的定位气泡，如果是自己的定位气泡，不做任何设置，显示为蓝点，如果不是自己的定位气泡，比如大头针就会进入
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
        MAAnnotationView*annotationView = (MAAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil) {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
            annotationView.frame = CGRectMake(0, 0, 100, 100);
            annotationView.canShowCallout= NO;
            //设置大头针显示的图片
            annotationView.image = [UIImage imageNamed:@"redPin"];
        }
        return annotationView;
    }
    return nil;
    
}
- (void)mapViewDidFailLoadingMap:(MAMapView *)mapView withError:(NSError *)error
{
    NSLog(@"error======%@",error);
    [LJTools showNOHud:@"定位失败" delay:1.0];
}
- (void)mapViewWillStartLocatingUser:(MAMapView *)mapView
{

    
}
- (void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated wasUserAction:(BOOL)wasUserAction
{

}

-(void)setPoint:(AMapPOI *)model
{
    CLLocationCoordinate2D coor;
    coor.latitude = model.location.latitude;
    coor.longitude = model.location.longitude;
    if (_pointAnnotation==nil) {
        _pointAnnotation = [[MAPointAnnotation alloc] init];
    }
    _pointAnnotation.coordinate = coor;
    //设置地图的定位中心点坐标
    [self.mapView setCenterCoordinate:coor animated:YES];
    //将点添加到地图上，即所谓的大头针
    [self.mapView addAnnotation:_pointAnnotation];
}

- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)confirmButtonAction:(UIButton *)sender {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
