//
//  CityDropDownView.m
//  PPK
//
//  Created by null on 2022/3/5.
//

#import "CityDropDownView.h"
#import "ProvinceCell.h"
#import "ProvinceModel.h"

@interface CityDropDownView () <UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *provinceTableView;
@property (weak, nonatomic) IBOutlet UITableView *cityTableView;
@property (weak, nonatomic) IBOutlet UITableView *districtTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTop;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;


@property (nonatomic, assign) CGFloat contentViewHeight;
@property (nonatomic, strong) NSMutableArray *provinces;
@property (nonatomic, strong) ProvinceModel *lastProvince;
@property (nonatomic, strong) CityModel *lastCity;
@property (nonatomic, strong) AreaModel *lastArea;

@end

@implementation CityDropDownView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.clipsToBounds = YES;
    
    CGFloat top = NavAndStatusHight + 43;
    self.contentViewHeight = SCREEN_HEIGHT - top - TAB_BAR_HEIGHT;
    self.frame = CGRectMake(0, top, SCREEN_WIDTH, self.contentViewHeight);
    self.contentViewTop.constant = - self.contentViewHeight;


    self.provinceTableView.delegate = self;
    self.provinceTableView.dataSource = self;
    
    self.cityTableView.delegate = self;
    self.cityTableView.dataSource = self;
    
    self.districtTableView.delegate = self;
    self.districtTableView.dataSource = self;
    
//    self.provinceIndex = 0;
    self.cityLabel.text = [NSString stringWithFormat:@"当前定位：%@",[LJTools getAppDelegate].city];

    NSString *mainBundleDirectory=[[NSBundle mainBundle] bundlePath];
    NSString *path=[mainBundleDirectory stringByAppendingPathComponent:@"province.json"];
    NSURL *url=[NSURL fileURLWithPath:path];
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    self.provinces = [NSMutableArray array];
    for (NSDictionary *obj in dataArray) {
         ProvinceModel *model = [[ProvinceModel alloc] initWithDictionary:obj];
        [self.provinces addObject:model];
    }
//    NSLog(@"%@",dic);
}

- (void)setHiddenTabbar:(BOOL)hiddenTabbar
{
    _hiddenTabbar = hiddenTabbar;
    CGFloat top = NavAndStatusHight + 43;
    if (hiddenTabbar) {
        self.contentViewHeight = SCREEN_HEIGHT - top;
    }else {
        self.contentViewHeight = SCREEN_HEIGHT - top - TAB_BAR_HEIGHT;
    }
    self.frame = CGRectMake(0, top, SCREEN_WIDTH, self.contentViewHeight);
    self.contentViewTop.constant = - self.contentViewHeight;
}

- (void)setIsShowArea:(BOOL)isShowArea
{
    _isShowArea = isShowArea;
    self.districtTableView.hidden = !isShowArea;
}

- (void)setSelectedStr:(NSString *)selectedStr
{
    _selectedStr = selectedStr;
    for (ProvinceModel *model in self.provinces) {
        NSArray *cityArray = model.citys;
        for (CityModel *cityModel in cityArray) {
            if ([selectedStr containsString:cityModel.name] || [cityModel.name containsString:selectedStr]) {
                self.lastProvince = model;
                self.lastCity = cityModel;
                [self.provinceTableView reloadData];
                [self.cityTableView reloadData];
                return;
            }
        }
    }
}

#pragma mark - UIGestureRecognizerDelegate
/*
 解决didSelectRowAtIndexPath不能响应事件：
 UITapGestureRecognizer吞掉了touch事件，导致didSelectRowAtIndexPath方法无法响应。
 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch  {
    // 输出点击的view的类名
    NSLog(@"%@", NSStringFromClass([touch.view class]));
    // 若为UITableViewCellContentView（即点击了tableViewCell），则不截获Touch事件
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    return  YES;
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.provinceTableView) {
        return self.provinces.count;
    }
    
    if (tableView == self.cityTableView) {
        if (self.lastProvince) {
            return self.lastProvince.citys.count;
        }
        return 0;
    }

    if (self.lastCity) {
        return self.lastCity.areas.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProvinceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProvinceCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProvinceCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (tableView == self.provinceTableView) {
        ProvinceModel *model = self.provinces[indexPath.row];
        cell.nameLabel.text = model.name;
        if (model == self.lastProvince) {
            cell.lineImageView.hidden = NO;
            cell.nameLabel.textColor = MainColor;
            cell.contentView.backgroundColor = UIColorFromRGB(0xF6F7F9);
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }
        else {
            cell.lineImageView.hidden = YES;
            cell.nameLabel.textColor = UIColorFromRGB(0x333333);
            cell.contentView.backgroundColor = UIColorFromRGB(0xF6F7F9);
        }
        return cell;
    }
    
    if (tableView == self.cityTableView) {
        CityModel *cityModel = self.lastProvince.citys[indexPath.row];
        cell.nameLabel.text = cityModel.name;
        cell.nameLabel.textColor = cityModel == self.lastCity ? MainColor : UIColorFromRGB(0x333333);
        return cell;
    }
    
    if (tableView == self.districtTableView) {
        AreaModel *areaModel = self.lastCity.areas[indexPath.row];
        cell.nameLabel.text = areaModel.name;
        cell.nameLabel.textColor = areaModel == self.lastArea ? MainColor : UIColorFromRGB(0x333333);
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.provinceTableView) {
        self.lastProvince = self.provinces[indexPath.row];
        self.lastCity = nil;
        self.lastArea = nil;
        [self.provinceTableView reloadData];
        [self.cityTableView reloadData];
        [self.districtTableView reloadData];
    }
    else if (tableView == self.cityTableView) {
        self.lastCity = self.lastProvince.citys[indexPath.row];
        self.lastArea = nil;
        [self.cityTableView reloadData];
        [self.districtTableView reloadData];
        if (!self.isShowArea) {
            [self hideDropDown];
            if (self.didSelectBlock) {
                self.didSelectBlock(self.lastCity.name,@"");
            }
        }
    }
    else {
        self.lastArea = self.lastCity.areas[indexPath.row];
        [self.districtTableView reloadData];
        [self hideDropDown];
        if (self.didSelectBlock) {
            if (indexPath.row==0) {
                self.didSelectBlock(self.lastCity.name,@"");
            }else {
                self.didSelectBlock(self.lastCity.name,self.lastArea.name);
            }
        }
    }

}

#pragma mark - Function
//添加手势
- (void)addTapGestureRecognizerToSelf {
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapMaskView:)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
}

- (void)tapMaskView: (UITapGestureRecognizer *)tap {
    NSLog(@"收起");
    [self hideDropDown];
}

- (void)showDropDown {
    self.backgroundColor = [UIColor clearColor];
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        self.contentViewTop.constant = 0;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)hideDropDown {
    if (self.hiddenBlock) {
        self.hiddenBlock();
    }
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [UIColor clearColor];
        self.contentViewTop.constant = -self.contentViewHeight;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

- (void)hideDropDownAnimation:(BOOL)animation {
    if (animation) {
        [self hideDropDown];
    }else {
        if (self.hiddenBlock) {
            self.hiddenBlock();
        }
        self.backgroundColor = [UIColor clearColor];
        self.contentViewTop.constant = -self.contentViewHeight;
        self.hidden = YES;
    }
}

- (IBAction)locationButtonAction:(UIButton *)sender {
    [[LJTools getAppDelegate].locationManager startUpdatingHeading];
}

- (IBAction)curruntCityButtonAction:(UIButton *)sender {
    if ([LJTools getAppDelegate].city.length>0) {
        [self hideDropDown];
        if (self.didSelectBlock) {
            self.didSelectBlock([LJTools getAppDelegate].city,@"");
        }
    }
}



@end
