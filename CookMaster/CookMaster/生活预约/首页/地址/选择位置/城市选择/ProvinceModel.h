//
//  ProvinceModel.h
//  PPK
//
//  Created by null on 2022/3/13.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AreaModel : BaseModel
@property (nonatomic, strong) NSString *pname;
@property (nonatomic, strong) NSString *name;

@end

@interface CityModel : BaseModel
@property (nonatomic, strong) NSString *pname;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *areas;

@end

@interface ProvinceModel : BaseModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *citys;
@property (nonatomic, assign) BOOL selected;

@end

NS_ASSUME_NONNULL_END
