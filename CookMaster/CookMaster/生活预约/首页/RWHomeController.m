//
//  RWHomeController.m
//  PPK
//
//  Created by null on 2022/3/30.
//

#import "RWHomeController.h"
#import "RWCateButton.h"
#import "YNPageConfigration.h"
#import "YNPageScrollMenuView.h"
#import "AddressListView.h"
#import "RWMapViewController.h"
#import "RWReserveController.h"

@interface RWHomeController ()<YNPageScrollMenuViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *cateView;
@property (weak, nonatomic) IBOutlet UIView *subCateView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (nonatomic, strong) UIScrollView *cateScrollView;
@property (nonatomic, strong) RWCateButton *lastButton;
@property (nonatomic, strong) AddressListView *addressView;

@end

@implementation RWHomeController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 16, SCREEN_WIDTH, 64)];
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    [self.cateView addSubview:scrollView];
    
    CGFloat left = 16;
    CGFloat width = 58;
    CGFloat height = 64;
    CGFloat space = (SCREEN_WIDTH - left*2 - width*5)/4;
    for (int i = 0; i<10; i++) {
        int page = i/5;
        int col = i%5;
        RWCateButton *button = [[[NSBundle mainBundle] loadNibNamed:@"RWCateButton" owner:nil options:nil] firstObject];
        button.frame = CGRectMake(page*SCREEN_WIDTH + left + (width + space)*col, 0, width, height);
        button.nameLabel.backgroundColor = [UIColor clearColor];
        button.nameLabel.textColor = [UIColor whiteColor];
        button.alpha = 0.7;
        button.tag = 100+i;
        [button addTarget:self action:@selector(cateButtonsAction:) forControlEvents:UIControlEventTouchUpInside];
        if (i==0) {
            button.alpha = 1;
            button.nameLabel.backgroundColor = [UIColor whiteColor];
            button.nameLabel.textColor = MainColor;
            self.lastButton = button;
        }
        [scrollView addSubview:button];
        NSLog(@"%@",NSStringFromCGRect(button.frame));
    }
    scrollView.contentSize = CGSizeMake(SCREEN_WIDTH*2, 64);
    
    
    
    [self.subCateView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, 58)];
    
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    //scrollMenu = NO,aligmentModeCenter = NO 会变成平分
    configration.scrollMenu = YES;
    configration.aligmentModeCenter = NO;
    configration.showBottomLine = NO;
    configration.showScrollLine = NO;
    configration.menuHeight = 58;
    configration.normalItemColor = RGBA(51, 51, 51, 1);
    configration.selectedItemColor = RGBA(51, 51, 51, 1);
    configration.itemFont = [UIFont systemFontOfSize:16];
    configration.selectedItemFont = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    
    NSMutableArray *titleArray = [NSMutableArray arrayWithArray:@[@"充煤气",@"回收煤气罐"]];
    NSMutableArray *buttonArrayM = [NSMutableArray array];
    for (int i = 0; i<titleArray.count; i++) {
        ReLayoutButton *button = [ReLayoutButton buttonWithType:UIButtonTypeCustom];
        button.layoutType = 3;
        button.margin = 5;
        UIImage *selectImage = [UIImage imageNamed:@"矩形 1884"];
        [button setImage:[UIImage imageNamed:@"矩形 1884-1"] forState:UIControlStateNormal];
        [button setImage:selectImage forState:UIControlStateSelected];
        [buttonArrayM addObject:button];
    }
    configration.buttonArray = buttonArrayM;
    
    YNPageScrollMenuView *menuView = [YNPageScrollMenuView pagescrollMenuViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 58) titles:titleArray configration:configration delegate:self currentIndex:0];
    [self.subCateView addSubview:menuView];
    
}


/// 点击item
- (void)pagescrollMenuViewItemOnClick:(UIButton *)button index:(NSInteger)index
{
    
}

- (void)cateButtonsAction:(RWCateButton *)button
{
    self.lastButton.alpha = 0.7;
    self.lastButton.nameLabel.backgroundColor = [UIColor clearColor];
    self.lastButton.nameLabel.textColor = [UIColor whiteColor];
    
    button.alpha = 1;
    button.nameLabel.backgroundColor = [UIColor whiteColor];
    button.nameLabel.textColor = MainColor;
    
    self.lastButton = button;
}


- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)mapButtonAction:(UIButton *)sender {
    RWMapViewController *vc = [[RWMapViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)addressButtonAction:(id)sender {
    if (!self.addressView) {
        self.addressView = [[[NSBundle mainBundle] loadNibNamed:@"AddressListView" owner:nil options:nil] firstObject];
        self.addressView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.addressView];
    }
    [self.addressView show];
}


- (IBAction)appointmentButtonAction:(UIButton *)sender {
    RWReserveController *vc = [[RWReserveController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
