//
//  RWReserveTimeView.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWReserveTimeView.h"
#import "ProvinceCell.h"

@interface RWReserveTimeView ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (weak, nonatomic) IBOutlet UITableView *dateTableView;
@property (weak, nonatomic) IBOutlet UITableView *timeTableView;

@end

@implementation RWReserveTimeView


- (void)awakeFromNib
{
    [super awakeFromNib];
    self.hidden = YES;
    self.contentViewHeight.constant = 339+bottomBarH;
    self.contentViewBottom.constant = - self.contentViewHeight.constant;
    [self.contentView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, self.contentViewHeight.constant)];
    
    self.dateTableView.delegate = self;
    self.dateTableView.dataSource = self;
    
    self.timeTableView.delegate = self;
    self.timeTableView.dataSource = self;
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==self.dateTableView) {
        return 3;
    }
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProvinceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProvinceCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProvinceCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
//    if (tableView == self.dateTableView) {
//        ProvinceModel *model = self.provinces[indexPath.row];
//        cell.nameLabel.text = model.name;
//        cell.badgeView.hidden = !model.selected;
//        if (model == self.lastProvince) {
//            cell.lineImageView.hidden = NO;
//            cell.nameLabel.textColor = MainColor;
//            cell.contentView.backgroundColor = [UIColor whiteColor];
//        }
//        else {
//            cell.lineImageView.hidden = YES;
//            cell.nameLabel.textColor = UIColorFromRGB(0x333333);
//            cell.contentView.backgroundColor = UIColorFromRGB(0xF6F7F9);
//        }
//        return cell;
//    }
//    
//    if (tableView == self.cityTableView) {
//        CityModel *cityModel = self.lastProvince.citys[indexPath.row];
//        cell.nameLabel.text = cityModel.name;
//        cell.nameLabel.textColor = [self.selectedCitys containsObject:cityModel] ? MainColor : UIColorFromRGB(0x333333);
//        return cell;
//    }
//
//    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (tableView == self.provinceTableView) {
//        self.lastProvince = self.provinces[indexPath.row];
//        [self.provinceTableView reloadData];
//        [self.cityTableView reloadData];
//        return;
//    }
//
//    CityModel *cityModel = self.lastProvince.citys[indexPath.row];
//    if ([self.selectedCitys containsObject:cityModel]) {
//        [self.selectedCitys removeObject:cityModel];
//    }
//    else {
//        if (self.selectedCitys.count>=3) {
//            [LJTools showText:@"最多选择三个城市" delay:1.5];
//            return;
//        }
//        [self.selectedCitys addObject:cityModel];
//    }
//    self.lastProvince.selected = [self checkProvinceIsSelected];
//    [self.provinceTableView reloadData];
//    [self.cityTableView reloadData];
}


#pragma mark - Function
- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewBottom.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewBottom.constant = - self.contentViewHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

#pragma mark - XibFunction
- (IBAction)confirmButtonClick:(UIButton *)sender {
//    if (self.didSelectBlock) {
//        self.didSelectBlock([citys componentsJoinedByString:@","]);
//    }
    [self dismiss];
}

- (IBAction)closeButtonClick:(id)sender {
    [self dismiss];
}

@end
