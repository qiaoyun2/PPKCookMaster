//
//  AppDelegate.m
//  ZZR
//
//  Created by null on 2018/12/13.
//  Copyright © 2018 null. All rights reserved.
//

#import "AppDelegate.h"
#import "TabBarController.h"
#import "NetworkingTool.h"
#import "GuideView.h"
#import "IQKeyboardManager.h"
#import "AppDelegate+CheckNetworkState.h"
#import "NavigationController.h"
///第三方服务
#import "SOAComponentAppDelegate.h"
#import <AdSupport/AdSupport.h>
#import "LocalizedHelper.h"
#import "LoginViewController.h"
#import <Bugly/Bugly.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapLocationKit/AMapLocationKit.h>

#import "TabBarController.h"
#import "LoginViewController.h"
//#import "MaintainViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // Override point for customization after application launch.
    [[LocalizedHelper standardHelper] setUserLanguage:@"zh-Hans"];

    NSLog(@"");
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(application:didFinishLaunchingWithOptions:)]){
            [service application:application didFinishLaunchingWithOptions:launchOptions];
        }
    }
    
    [self keyboardManager];
    [self initLoactionManager];
    [self ml_checkNetworkState];
    [Bugly startWithAppId:BuglyAPPKEY];
    [AMapServices sharedServices].apiKey = AMapKEY;

    
    if ([User getMobile].length <= 0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:nil forKey:UserToken];
        [defaults synchronize];
    }
    
    if (@available(iOS 11.0, *)) {
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }
//
    if (@available(iOS 15.0, *)) {
        [UITableView appearance].sectionHeaderTopPadding = 0;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:AppIsOnline];
    [[NSUserDefaults standardUserDefaults] synchronize];

    /// window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    
//    TabBarController *tabBarController = [TabBarController new];
//    self.window.rootViewController = tabBarController;

    if ([LJTools islogin]) {
        TabBarController *tabBarController = [TabBarController new];
        self.window.rootViewController = tabBarController;
        ///维修
//        MaintainViewController *tabBarController = [MaintainViewController new];
//        self.window.rootViewController = tabBarController;
    }else {
        LoginViewController *vc = [LoginViewController new];
        self.window.rootViewController = [[NavigationController alloc] initWithRootViewController:vc];
    }
    [self.window makeKeyAndVisible];
//    [self showGuideView];
    [self checkVersion];
    return YES;
}


-(void)checkVersion
{
    NSString *iFeverAPPID = AppStoreAppID;
    //获取当前应用版本号
    NSDictionary *appInfo = [[NSBundle mainBundle] infoDictionary];
    NSString *currentVersion = [appInfo objectForKey:@"CFBundleShortVersionString"];

    NSString *updateUrlString = [NSString stringWithFormat:@"http://itunes.apple.com/lookup?id=%@",iFeverAPPID];
    
    NSString * urlStr = [updateUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.timeoutInterval = 10;
    [manager POST:urlStr parameters:nil headers:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments error:nil];
        NSArray *array = result[@"results"];
        NSLog(@"--------%@------",array);
        if (array.count != 0) {// 先判断返回的数据是否为空 没sj的时候是空的
            NSDictionary *dict = array[0];
            
            NSArray *ary1 = [currentVersion componentsSeparatedByString:@"."];
            NSArray *ary2 = [dict[@"version"] componentsSeparatedByString:@"."];
            
            if (ary2.count) {/** sj大于线上是  正在sj */
                if ([ary1[0] intValue] > [ary2[0] intValue]) {//2.0 1.9
                    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:AppIsOnline];
                    
                } else if ([ary1[0] intValue] == [ary2[0] intValue] && [ary1[1] intValue] > [ary2[1] intValue]) {/////1.1 1.2
                    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:AppIsOnline];
                    
                }
                else if ([ary1[0] intValue] == [ary2[0] intValue] && [ary1[1] intValue] == [ary2[1] intValue]) {///1.0.1 1.1
                    if (ary2.count < ary1.count) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:AppIsOnline];
                    }else {
                        ///线上2.0.6 本地 2.0.3
                        if ( [ary1[2] intValue] < [ary2[2] intValue]){
                            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:AppIsOnline];
                            //如果有新版本 这里要注意下如果你版本号写得是1.1.1或者1.1.1.1这样的格式，就不能直接转floatValue，自己想办法比较判断。
                            
                            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"更新提示" message:@"发现新版本。为保证各项功能正常使用，请您尽快更新。" preferredStyle:UIAlertControllerStyleAlert];
                            
                            [alert addAction:[UIAlertAction actionWithTitle:@"现在更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/cn/app/id%@?mt=8",iFeverAPPID]]];
                                //这里写的URL地址是该app在app store里面的下载链接地址，其中ID是该app在app store对应的唯一的ID编号。
                                NSLog(@"点击现在升级按钮,跳转");
                            }]];
                            
                            [alert addAction:[UIAlertAction actionWithTitle:@"下次再说" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                NSLog(@"点击下次再说按钮");  //如果不add这段Action，则弹窗中只有1个按钮，即强制用户更新
                            }]];
                            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
                        }else  if ( [ary1[2] intValue] > [ary2[2] intValue]){ //本地2.0.7 线上 2.0.6
                            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:AppIsOnline];
                        }
                        else{
                            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:AppIsOnline];
                        }
                    }
                }else {
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:AppIsOnline];
                }
               
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:LoginChangeSuccess object:nil];
            }
        } else {
            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",@"0"] forKey:AppIsOnline];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:LoginChangeSuccess object:nil];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",@"0"] forKey:AppIsOnline];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:LoginChangeSuccess object:nil];
    }];
}
#pragma mark - 键盘自适应高度
- (void)keyboardManager {
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager]; // 获取类库的单例变量
    keyboardManager.enable = YES; // 控制整个功能是否启用
    keyboardManager.shouldResignOnTouchOutside = YES; // 控制点击背景是否收起键盘
    keyboardManager.shouldToolbarUsesTextFieldTintColor = YES; // 控制键盘上的工具条文字颜色是否用户自定义
    keyboardManager.toolbarManageBehaviour = IQAutoToolbarBySubviews; // 有多个输入框时，可以通过点击Toolbar 上的“前一个”“后一个”按钮来实现移动到不同的输入框
    keyboardManager.enableAutoToolbar = NO; // 控制是否显示键盘上的工具条
    keyboardManager.placeholderFont = [UIFont boldSystemFontOfSize:17]; // 设置占位文字的字体
    keyboardManager.keyboardDistanceFromTextField = 10.0f; // 输入框距离键盘的距离
}
#pragma mark 引导页
- (void)showGuideView
{
    //判断是否首次使用
    BOOL isFirstUsing = GuideView.readAppStatus;
    if (!isFirstUsing)
    {
        // 非首次使用 保存首次使用的状态
        [GuideView saveAppStatus];
        // 实例化引导页
        NSArray *images = @[@"guideImage_1", @"guideImage_2", @"guideImage_3"];
        GuideView *guideView = [[GuideView alloc] initWithImages:images];
        guideView.animationType =GuideAnimationTypeZoomIn;
        guideView.buttonClick = ^(){
            //引导页最后一张点击事件
        };
    }
}
#pragma mark 初始化定位
- (void)initLoactionManager
{
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = 50;
    self.locationManager.pausesLocationUpdatesAutomatically = NO;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [_locationManager requestWhenInUseAuthorization];
    }
    if(![CLLocationManager locationServicesEnabled]) {
        NSLog(@"请开启定位:设置 > 隐私 > 位置 > 定位服务");
    }
    [_locationManager startUpdatingLocation];
}
#pragma mark 定位成功
#pragma mark -- CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *nowLocation = [locations lastObject];
    // 通过location  或得到当前位置的经纬度
    self.latitude = nowLocation.coordinate.latitude;
    self.longitude = nowLocation.coordinate.longitude;
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    //反地理编码
    [geoCoder reverseGeocodeLocation:nowLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if(error == nil)
        {
            CLPlacemark *placemark = [placemarks firstObject];
            NSLog(@"%@__%f__%f",placemark.name,self.latitude,self.longitude);
            self.city = placemark.addressDictionary[@"City"];
            self.adress = placemark.addressDictionary[@"FormattedAddressLines"][0];
            [[NSNotificationCenter defaultCenter] postNotificationName:LocationChangeSuccess object:nil];
        }
    }];
    [self.locationManager stopUpdatingLocation];//定位成功后停止定位
}
#pragma mark 定位失败
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"error:%@",error);
}

#pragma mark - <UIApplicationDelegate>
- (void)applicationWillResignActive:(UIApplication *)application {
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(applicationWillResignActive:)]){
            [service applicationWillResignActive:application];
        }
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(applicationDidEnterBackground:)]){
            [service applicationDidEnterBackground:application];
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(applicationWillEnterForeground:)]){
            [service applicationWillEnterForeground:application];
        }
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(applicationDidBecomeActive:)]){
            [service applicationDidBecomeActive:application];
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(applicationWillTerminate:)]){
            [service applicationWillTerminate:application];
        }
    }
}

#pragma mark - 如果需要使用 URL Scheme 或 通用链接相关功能，请实现以下方法
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options
{
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(application:openURL:options:)]){
            [service application:app openURL:url options:options];
        }
    }
    return YES;
}

//// 支持所有iOS系统
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(application:openURL:sourceApplication:annotation:)]){
            [service application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
        }
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray<id<UIUserActivityRestoring>> * __nullable restorableObjects))restorationHandler {
    // 通过通用链接唤起 App
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(application:continueUserActivity:restorationHandler:)]){
            [service application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
        }
    }
    return YES;
}

#pragma mark - 如需使用远程推送相关功能，请实现以下方法
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // 远程通知注册成功，收到 deviceToken 调用sdk方法，传入 deviceToken
    id<UIApplicationDelegate> service;
    for(service in [[SOAComponentAppDelegate instance] services]){
        if ([service respondsToSelector:@selector(application:didRegisterForRemoteNotificationsWithDeviceToken:)]){
            [service application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
        }
    }
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"ERROR:%@",error);
    // 远程通知注册失败
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // 收到远程推送消息
    completionHandler(UIBackgroundFetchResultNewData);
}

#pragma mark - 如需使用本地推送通知功能，请实现以下方法
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    // 收到本地推送消息
}


@end
