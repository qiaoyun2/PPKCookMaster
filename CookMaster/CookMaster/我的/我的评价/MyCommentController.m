//
//  MyCommentController.m
//  PPKMaster
//
//  Created by null on 2022/4/18.
//

#import "MyCommentController.h"
#import "MyCommentCell.h"
#import "SignInView.h"

@interface MyCommentController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, copy) NSString *start_data;
@property (nonatomic, copy) NSString *end_data;
@property (nonatomic, assign) NSInteger page;

@end

@implementation MyCommentController

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [SignInView reinitialize];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的评价";
    [self setNavigationRightBarButtonWithImage:@"路径 19589"];
    _start_data = @"";
    _end_data = @"";
    
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    [self refresh];
}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSString *type = @"";
    ///// 1:货运师傅；2：维修师傅；3：废品回收师傅
    if([User getAuthMasterType].intValue == 1) {//1-回收;2-搬运;3-维修
        type = @"2";
    }else if([User getAuthMasterType].intValue == 2) {
        type = @"3";
    }else  {
        type = @"1";
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"type"] = type;//1-回收;2-搬运;3-维修
    params[@"startDate"] = self.start_data;
    params[@"endDate"] = self.end_data;
    WeakSelf
    [NetworkingTool getWithUrl:kRWCommentListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                RWOrderCommentModel *model = [[RWOrderCommentModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCommentCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MyCommentCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    RWOrderCommentModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Function
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender {
    MJWeakSelf;
    [SignInView title:@"选择时间" show:^(NSString * _Nonnull start, NSString * _Nonnull end) {
        weakSelf.start_data = start;
        weakSelf.end_data = end;
        [weakSelf refresh];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
