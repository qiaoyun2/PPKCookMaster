//
//  PurchaseLogsModel.h
//  PPK
//
//  Created by null on 2022/3/14.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PurchaseLogsModel : BaseModel

@property (nonatomic, strong) NSString *createTime; //
@property (nonatomic, strong) NSString *paymentType; //
@property (nonatomic, strong) NSString *price; //
@property (nonatomic, strong) NSString *vipTypeName; // 次卡 季卡 年卡

@end

NS_ASSUME_NONNULL_END
