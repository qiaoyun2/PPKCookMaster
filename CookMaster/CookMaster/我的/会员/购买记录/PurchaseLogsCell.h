//
//  PurchaseLogsCell.h
//  PPK
//
//  Created by null on 2022/3/12.
//

#import <UIKit/UIKit.h>
#import "PurchaseLogsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PurchaseLogsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *coinLabel;

@property (nonatomic, strong) PurchaseLogsModel *model;

@end

NS_ASSUME_NONNULL_END
