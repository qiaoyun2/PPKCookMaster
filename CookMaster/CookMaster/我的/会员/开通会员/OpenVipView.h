//
//  OpenVipView.h
//  PPK
//
//  Created by null on 2022/3/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenVipView : UIView

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
