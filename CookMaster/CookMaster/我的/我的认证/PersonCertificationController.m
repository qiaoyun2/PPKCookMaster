//
//  PersonCertificationController.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "PersonCertificationController.h"
#import "LJImagePicker.h"
#import "UploadManager.h"
#import "ZJPickerView.h"

#import "PersonCertifyModel.h"

@interface PersonCertificationController ()

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UIButton *sexButton;
@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet UITextField *cardNumField;
@property (weak, nonatomic) IBOutlet UITextField *workYearsField;
@property (weak, nonatomic) IBOutlet UITextField *ageField;
@property (weak, nonatomic) IBOutlet UIButton *frontButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *handButton;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;
@property (weak, nonatomic) IBOutlet UIButton *typeButton;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;
///擅长维修/接单范围
@property (weak, nonatomic) IBOutlet UILabel *classNameLabel;
///标签
@property (weak, nonatomic) IBOutlet UIView *classTagView;
///tagView 高度
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *classTagViewHeight;
@property (weak, nonatomic) IBOutlet UIView *typeView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic , strong) NSMutableArray *dataSource;

@property (nonatomic, strong) NSString *frontStr;
@property (nonatomic, strong) NSString *backStr;
@property (nonatomic, strong) NSString *handStr;
@property (nonatomic, assign) NSInteger sex; // 性别 0男1女

@property (nonatomic, strong) NSString *type;
@end

@implementation PersonCertificationController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationItem.title = @"实名认证";
    self.backButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.frontButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.handButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [self initUI];
}
#pragma mark - UI
-(void)initUI{
    User *user = [User getUser];
    /// 1:货运师傅；2：维修师傅；3：废品回收师傅
    self.type = user.authMasterType;
    // authMasterStatus认证状态：0未认证；1认证中；2已完成；3审核失败
    if ([user.authMasterStatus integerValue]==2) {
        
        self.nameField.text = user.user_name;
        [self.sexButton setTitle:[user.sex integerValue]==1?@"女":@"男" forState:UIControlStateNormal];
        self.sexButton.selected = YES;
        self.mobileField.text = user.authMobile;
        self.cardNumField.text = user.cardId;
        self.workYearsField.text = user.workYear;
        self.ageField.text = user.age;
        /// @[@"货物搬运",@"物品维修",@"上门回收"]
        if (self.type.integerValue == 1) {
            [self.typeButton setTitle:@"货物搬运" forState:(UIControlStateNormal)];
        } else if (self.type.integerValue == 2){
            [self.typeButton setTitle:@"物品维修" forState:(UIControlStateNormal)];
        } else {
            [self.typeButton setTitle:@"上门回收" forState:(UIControlStateNormal)];
        }
        
        if (user.masterClassifyNames.length > 0) {
            NSArray *array = [user.masterClassifyNames componentsSeparatedByString:@";"];
            [self.dataArray removeAllObjects];
            for (NSString *name in array) {
                if(name.length > 0){
                    PersonCertifyModel *model = [PersonCertifyModel new];
                    model.name = name;
                    model.isSelect = YES;
                    [self.dataArray addObject:model];
                }
            }
            [self initTagView:self.dataArray];
        }else{
            self.typeView.hidden = YES;
        }
        [self.frontButton sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.frontCardId)] forState:UIControlStateNormal placeholderImage:DefaultImgWidth];
        [self.backButton sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.backCardId)] forState:UIControlStateNormal placeholderImage:DefaultImgWidth];
        [self.handButton sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.handCardId)] forState:UIControlStateNormal placeholderImage:DefaultImgWidth];
        
        self.completeButton.alpha = 0.5;
        [self.completeButton setTitle:@"认证已通过" forState:1.5];
        self.nameField.userInteractionEnabled  =  self.sexButton.userInteractionEnabled =  self.mobileField.userInteractionEnabled =  self.cardNumField.userInteractionEnabled = self.workYearsField.userInteractionEnabled  = self.ageField.userInteractionEnabled =  self.typeButton.userInteractionEnabled = self.classTagView.userInteractionEnabled  = self.frontButton.userInteractionEnabled = self.handButton.userInteractionEnabled  = self.backButton.userInteractionEnabled  = NO;
        self.bottomView.hidden = YES;
        self.bottomViewHeight.constant  = 0;
    }else{
        self.type = @"3";
        [self loadClass];
    }
}
#pragma mark - 维修分类
-(void)loadClass {
    //
    NSDictionary *dict = @{
        @"layer":@"1",//层级: 1-一级分类;2-二级分类
        @"parentId":@"0"
    };
    NSString *url = @"";
    /// 1:货运师傅；2：维修师傅；3：废品回收师傅
    if (self.type.integerValue == 1) {
        url = kTKTruckingCategoryListURL;
    } else if (self.type.integerValue == 2){
        url = kMTRepairClassifyURL;
    } else {
        return;
    }
    [NetworkingTool getWithUrl:url params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue] == 1) {
            [self.dataArray removeAllObjects];
            NSArray *array = responseObject[@"data"];
            for (NSDictionary *obj in array) {
                PersonCertifyModel *model = [[PersonCertifyModel alloc] initWithDictionary:obj];
                model.isSelect = NO;
                [self.dataArray addObject:model];
            }
            [self initTagView:self.dataArray];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}
#pragma mark - 创建按钮
-(void)initTagView:(NSArray *)array {
    
    [self.classTagView removeAllSubviews];
    if (array.count > 0){
        CGFloat nowWidth = 0;
        CGFloat nowHeight = 2;
        CGFloat tagsWidth = 0;
        CGFloat tagsHeight = 18;
        for (int i = 0; i < array.count; i++) {
            PersonCertifyModel * model = array[i];
            NSString *name = model.name;
            CGSize stringSize = [LJTools sizeForString:[NSString stringWithFormat:@"%@",name] withSize:CGSizeMake(CGFLOAT_MAX, 10) withFontSize:10];
            tagsWidth = stringSize.width + 20*2;
            if (name.length == 2) {
                tagsWidth = stringSize.width + 25*2;
            }
            UIButton *btn = [[UIButton alloc] init];
            [btn setTitle:[NSString stringWithFormat:@" %@ ", name] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(tagsClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.layer.borderWidth = 0;
            btn.tag = i + 100;
            btn.titleLabel.font = [UIFont systemFontOfSize:11 weight:(UIFontWeightBold)];
            btn.layer.cornerRadius = 9;
            [btn setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
            btn.backgroundColor = UIColorFromRGB(0xF5F6F9);
            
            [self.classTagView addSubview:btn];
            
            if (i == 0) {
                PersonCertifyModel  *model = self.dataArray[0];
                model.isSelect = YES;
                btn.selected = YES;
                [btn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
                btn.backgroundColor = UIColorFromRGB(0x00A2EA);
            }
            
            CGFloat willWidth = 0;
            willWidth = nowWidth + tagsWidth + 10;
            if (willWidth - 10 > SCREEN_WIDTH - 16 - 88 - 20) {
                nowWidth = 0;
                nowHeight = nowHeight + tagsHeight + 15;
            }
            btn.frame = CGRectMake(nowWidth, nowHeight, tagsWidth, tagsHeight);
            nowWidth = nowWidth + tagsWidth + 10;
        }
        self.classTagViewHeight.constant = nowHeight+15;
    }else{
        self.classTagViewHeight.constant = 0;
    }
    [self.view layoutIfNeeded];
    self.typeView.hidden = NO;
}
#pragma mark - 性格的选择
- (void)tagsClick:(UIButton *)btn {
    [self.view endEditing:YES];
    if(self.type.intValue == 1){
        for (UIButton *button in self.classTagView.subviews) {
            if ([button isKindOfClass:[UIButton class]]) {
                PersonCertifyModel  *model  = self.dataArray[button.tag-100];
                model.isSelect = button.selected = NO;
                [button setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
                button.backgroundColor = UIColorFromRGB(0xF5F6F9);
            }
        }
        PersonCertifyModel  *model  = self.dataArray[btn.tag-100];
        model.isSelect = YES;
        btn.selected = YES;
        [btn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
        btn.backgroundColor = UIColorFromRGB(0x00A2EA);
    }else{
        PersonCertifyModel  *model  = self.dataArray[btn.tag-100];
        NSMutableArray *array = [NSMutableArray array];
        for (PersonCertifyModel  *model in self.dataArray) {
            if(model.isSelect){
                [array addObject:model];
            }
        }
        btn.selected = !btn.selected;
        if (btn.selected ) {
            if(array.count > 2 && self.type.intValue == 2){
                return;
            }
            model.isSelect = YES;
            [btn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
            btn.backgroundColor = UIColorFromRGB(0x00A2EA);
        }else{
            model.isSelect = NO;
            [btn setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
            btn.backgroundColor = UIColorFromRGB(0xF5F6F9);
        }
    }
}
#pragma mark - 提交申请
- (void)requestForCertification {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"username"] = self.nameField.text;
    params[@"sex"] = @(self.sex); // 性别 0男1女
    params[@"authMobile"] = self.mobileField.text;
    params[@"cardId"] =  self.cardNumField.text;
    params[@"workYear"] =  self.workYearsField.text;
    params[@"age"] =  self.ageField.text;
    params[@"frontCardId"] =  self.frontStr;
    params[@"backCardId"] =  self.backStr;
    params[@"handCardId"] =  self.handStr;
    params[@"authMasterType"] =  self.type;
    
    if(self.dataArray.count > 0 && self.type.intValue < 3){
        if (self.dataSource.count > 0) {
            params[@"masterClassifyIds"] =  [self.dataSource componentsJoinedByString:@","];
        }
    }
    
    [NetworkingTool postWithUrl:kAuthenticationMasterURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools showText:responseObject[@"msg"] delay:1.5];
        if ([responseObject[@"code"] integerValue]==1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AuthenticationSuccess" object:nil userInfo:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
    } IsNeedHub:YES];
}

#pragma mark - XibFunction
- (IBAction)sexButtonAction:(UIButton *)sender {
    [self.view endEditing:YES];
    WeakSelf
    NSArray *array = @[@"男",@"女"];
    [UIAlertController actionSheetWithTitle:@"选择性别" message:nil titlesArry:array indexBlock:^(NSInteger index, id obj) {
        NSLog(@"%ld",index);
        weakSelf.sex = index;
        [sender setTitle:array[index] forState:UIControlStateNormal];
        sender.selected = YES;
    }];
}
#pragma mark - 身份证正面
- (IBAction)frontButtonAction:(UIButton *)sender {
    [self.view endEditing:YES];
    WeakSelf
    [LJImagePicker showImagePickerFromViewController:self allowsEditing:YES finishAction:^(UIImage *image) {
        if (image) {
            [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
                weakSelf.frontStr = imageUrl;
                [sender sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageUrl)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51102"]];
            }];
        }
    }];
}
#pragma mark - 身份证反面
- (IBAction)backButtonAction:(id)sender {
    [self.view endEditing:YES];
    WeakSelf
    [LJImagePicker showImagePickerFromViewController:self allowsEditing:YES finishAction:^(UIImage *image) {
        if (image) {
            [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
                weakSelf.backStr = imageUrl;
                [sender sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageUrl)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51102"]];
            }];
        }
    }];
}
#pragma mark - 手持身份证
- (IBAction)handButtonAction:(id)sender {
    [self.view endEditing:YES];
    WeakSelf
    [LJImagePicker showImagePickerFromViewController:self allowsEditing:YES finishAction:^(UIImage *image) {
        if (image) {
            [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
                weakSelf.handStr = imageUrl;
                [sender sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageUrl)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51102"]];
            }];
        }
    }];
}
#pragma mark - 类型选择
- (IBAction)typeButtonAction:(UIButton *)sender {
    [self.view endEditing:YES];
    WeakSelf  //// 1:货运师傅；2：维修师傅；3：废品回收师傅
    NSArray *array = @[@"货物搬运",@"物品维修",@"上门回收",];
    [UIAlertController actionSheetWithTitle:@"入驻分类" message:nil titlesArry:array indexBlock:^(NSInteger index, id obj) {
        NSLog(@"%ld",index);
        //        weakSelf.sex = index;
        weakSelf.type = [NSString stringWithFormat:@"%ld", index+1];
        [sender setTitle:array[index] forState:UIControlStateNormal];
        if (index < 2){
            [self loadClass];
        }else{
            self.typeView.hidden = YES;
        }
        self.classNameLabel.text = index == 1?@"擅长维修":@"接单范围";
        sender.selected = YES;
    }];
}
#pragma mark - 完成
- (IBAction)finishButtonAction:(id)sender {
    [self.view endEditing:YES];
    
    if (self.nameField.text.length==0) {
        [LJTools showText:@"请输入姓名" delay:1.5];
        return;
    }
    if (!self.sexButton.selected) {
        [LJTools showText:@"请选择性别" delay:1.5];
        return;
    }
    if (self.mobileField.text.length==0) {
        [LJTools showText:@"请输入联系方式" delay:1.5];
        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的联系方式" delay:1.5];
        return;
    }
    if (self.cardNumField.text.length==0) {
        [LJTools showText:@"请输入身份证号" delay:1.5];
        return;
    }
    if (![self.cardNumField.text isidentityCard]) {
        [LJTools showText:@"请输入正确的身份证号" delay:1.5];
        return;
    }
    if (self.workYearsField.text.length==0) {
        [LJTools showText:@"请输入工作年限" delay:1.5];
        return;
    }
    if (self.ageField.text.length==0) {
        [LJTools showText:@"请输入年领" delay:1.5];
        return;
    }
    if (![self.frontStr length]) {
        [LJTools showText:@"请上传正面证件照" delay:1.5];
        return;
    }
    if (![self.backStr length]) {
        [LJTools showText:@"请上传反面证件照" delay:1.5];
        return;
    }
    if (![self.handStr length]) {
        [LJTools showText:@"请上传手持证件照片" delay:1.5];
        return;
    }
    
    if ([self.type intValue] < 3 && self.dataArray.count > 0) {
        [self.dataSource removeAllObjects];
        for ( PersonCertifyModel  *model in self.dataArray) {
            if (model.isSelect) {
                [self.dataSource addObject:model.idStr];
            }
        }
        if ( self.dataSource.count == 0) { //// 1:货运师傅；2：维修师傅；3：废品回收师傅
            [LJTools showText: self.type.intValue == 1?@"请选择接单范围":@"请选择擅长维修" delay:1.5];
            return;
        }
        if ( self.dataSource.count > 3) { //// 1:货运师傅；2：维修师傅；3：废品回收师傅
            [LJTools showText: @"擅长维修类型最多可选择3个" delay:1.5];
            return;
        }
    }
    [self requestForCertification];
}

#pragma mark - 懒加载
-(NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
@end
