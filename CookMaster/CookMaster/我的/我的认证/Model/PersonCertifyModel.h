//
//  PersonCertifyModel.h
//  PPKMaster
//
//  Created by Apple on 2022/9/5.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersonCertifyModel : BaseModel
@property (nonatomic, strong) NSString *idStr; //
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *parentId; //
@property (nonatomic, strong) NSString *picture; // 完成时间
@property (nonatomic, assign) BOOL isSelect; // 是否选择
@end

NS_ASSUME_NONNULL_END
