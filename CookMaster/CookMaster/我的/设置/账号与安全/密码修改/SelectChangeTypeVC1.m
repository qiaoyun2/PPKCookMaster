//
//  SelectChangeTypeVC1.m
//  ZZR
//
//  Created by null on 2021/3/8.
//

#import "SelectChangeTypeVC1.h"
#import "ChangePasswordVC.h"
#import "VerificationPhoneVC.h"

@interface SelectChangeTypeVC1 ()

@end

@implementation SelectChangeTypeVC1
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"修改密码";
}

#pragma mark – UI
#pragma mark – Network
#pragma mark – Delegate
#pragma mark - Function
#pragma mark – XibFunction
- (IBAction)btnClick:(UIButton*)sender {
    if (sender.tag == 2) {
        VerificationPhoneVC *vc = [[VerificationPhoneVC alloc] init];
        vc.type = 3;
        [self.navigationController pushViewController:vc animated:YES];

    }else{
        ChangePasswordVC * vc = [[ChangePasswordVC alloc] init];
        vc.type = 2;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark – lazy load
@end
