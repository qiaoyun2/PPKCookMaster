//
//  ChangePayPasswordVC.h
//  ZZR
//
//  Created by null on 2020/9/11.
//  Copyright © 2020 null. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChangePayPasswordVC : BaseViewController

@property(nonatomic,assign) NSInteger type;//1 第一步//2.第二步 3.第三步
@property (nonatomic, strong) NSString *surePwd;// 确认的密码


@property (nonatomic, strong) NSString *oldPwd;// 旧密码 2选1
@property(nonatomic,copy)NSString *code; // 验证码 2选1

@end

NS_ASSUME_NONNULL_END
