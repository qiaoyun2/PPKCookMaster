//
//  SelectChangeTypeVC2.m
//  StarCard
//
//  Created by ZZR on 2022/3/26.
//

#import "SelectChangeTypeVC2.h"
#import "VerificationPhoneVC.h"
#import "ChangePayPasswordVC.h" // 修改支付密码

@interface SelectChangeTypeVC2 ()

@end

@implementation SelectChangeTypeVC2
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"修改支付密码";
}

#pragma mark – UI
#pragma mark – Network
#pragma mark – Delegate
#pragma mark - Function
#pragma mark – XibFunction
- (IBAction)btnClick:(UIButton*)sender {

    //1.旧支付密码验证修改 2.手机号验证修改
    if (sender.tag == 1) {
        ChangePayPasswordVC *vc = [[ChangePayPasswordVC alloc] init];
        vc.type = 1;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (sender.tag == 2) {
        VerificationPhoneVC *vc = [[VerificationPhoneVC alloc] init];
        vc.type = 2;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark – lazy load
@end
