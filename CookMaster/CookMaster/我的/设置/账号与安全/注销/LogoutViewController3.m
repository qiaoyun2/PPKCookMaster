//
//  LogoutViewController3.m
//  ZZR
//
//  Created by ZZR on 2021/10/7.
//

#import "LogoutViewController3.h"

@interface LogoutViewController3 ()

@end

@implementation LogoutViewController3

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    //YES：允许右滑返回  NO：禁止右滑返回
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);
    self.navigationItem.title = LocalizedString(@"注销账号");
}


-(void)backItemClicked{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
