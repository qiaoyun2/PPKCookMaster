//
//  LogoutViewController3.h
//  ZZR
//
//  Created by ZZR on 2021/10/7.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LogoutViewController4 : BaseViewController
@property (nonatomic, strong) NSString *reason;

@end

NS_ASSUME_NONNULL_END
