//
//  PhotosView.m
//  ZZR
//
//  Created by null on 2020/8/27.
//  Copyright © 2020 null. All rights reserved.
//

#import "PhotosView.h"
@interface PhotosView()

@property(nonatomic,strong)NSMutableArray * delBtns;

@property(nonatomic,assign)NSInteger type;

@end

@implementation PhotosView
-(instancetype)initWithFrame:(CGRect)frame withType:(NSInteger)type{
    if (self = [super initWithFrame:frame]) {
        _delBtns = [NSMutableArray new];
        _type = type;
        _maxCount = 9;
        _rowCount = 3;
        _defaultImg = @"add_pic";
        _delImg = @"bb_close";
        _imgSize = CGSizeMake(80, 80);
        if (_type == 1) {
            _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.width, self.height)];
            _scrollView.showsVerticalScrollIndicator = NO;
            _scrollView.showsHorizontalScrollIndicator = NO;
            [self addSubview:_scrollView];
        }
    }
    return self;
}
-(void)setDefaultImg:(NSString *)defaultImg{
    _defaultImg = defaultImg;
}
-(void)setMaxCount:(NSInteger)maxCount{
    _maxCount = maxCount;
}
-(void)setImgSize:(CGSize)imgSize{
    _imgSize = CGSizeMake(imgSize.width, imgSize.height);
}
-(void)setRowCount:(NSInteger)rowCount{
    _rowCount = rowCount;
}
-(void)setImages:(NSArray *)images{
    _images = images;
    int count = (int)images.count;
    if (count<_maxCount) {
        count+=1;
    }
    if (_type == 1) {
        [_scrollView removeAllSubviews];
        CGFloat item_w = _imgSize.width;
        for (int i= 0; i<count; i++) {
            id obj;
            UIImageView * imgV = [[UIImageView alloc]initWithFrame:CGRectMake((item_w+10)*i, 0, item_w, _imgSize.height)];
             imgV.contentMode = UIViewContentModeScaleAspectFill;
             imgV.clipsToBounds = YES;
            imgV.layer.cornerRadius = 4;
            
             [_scrollView addSubview:imgV];
            if (images.count==_maxCount) {
                obj = images[i];
                UIButton * delbtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgV.frame)-25, imgV.y, 25, 20)];
                [delbtn setImage:[UIImage imageNamed:_delImg] forState:0];
                delbtn.tag = 100+i;
                [delbtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
                [_scrollView addSubview:delbtn];
            }else{
                if (i== count-1) {
                    obj = [UIImage imageNamed:_defaultImg];
                    imgV.tag = 10;
                    imgV.userInteractionEnabled = YES;
                    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
                    [imgV addGestureRecognizer:tap];
                }else{
                    obj = images[i];
                    UIButton * delbtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgV.frame)-25, imgV.y, 25, 20)];
                    [delbtn setImage:[UIImage imageNamed:_delImg] forState:0];
                    delbtn.tag = 100+i;
                    [delbtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
                    [_scrollView addSubview:delbtn];
                }
            }
            if ([obj isKindOfClass:[UIImage class]]) {
                imgV.image = obj;
            }else if([obj isKindOfClass:[NSDictionary class]]){
                NSString * str = obj[@"path"];
                [imgV sd_setImageWithURL:[NSURL URLWithString:str]];
            }else{
                [imgV sd_setImageWithURL:[NSURL URLWithString:obj]];
            }
        }
        _scrollView.height = _imgSize.height;
        _scrollView.contentSize = CGSizeMake((item_w+10)*count, _scrollView.height);
         self.height = _scrollView.height+20;
    }else{
        [self removeAllSubviews];
        CGFloat item_w = (self.width-(_rowCount-1)*10)/_rowCount;
        CGFloat max_h = 0;
        for (int i= 0; i<count; i++) {
         int row = i%_rowCount;
         int hang = i/_rowCount;
         id obj;
        
         UIImageView * imgV = [[UIImageView alloc]initWithFrame:CGRectMake((item_w+10)*row, (item_w+10)*hang, item_w, item_w)];
         imgV.contentMode = UIViewContentModeScaleAspectFill;
         imgV.clipsToBounds = YES;
            imgV.layer.cornerRadius = 4;
         [self addSubview:imgV];
         if (images.count==_maxCount) {
             obj = images[i];
             UIButton * delbtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgV.frame)-25, imgV.y, 25, 20)];
             [delbtn setImage:[UIImage imageNamed:_delImg] forState:0];
             delbtn.tag = 100+i;
             [delbtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
             [self addSubview:delbtn];
         }else{
             if (i== count-1) {
                 obj = [UIImage imageNamed:_defaultImg];
                 imgV.tag = 10;
                 imgV.userInteractionEnabled = YES;
                 UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
                 [imgV addGestureRecognizer:tap];
             }else{
                 obj = images[i];
                 UIButton * delbtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imgV.frame)-25, imgV.y, 25, 20)];
                 [delbtn setImage:[UIImage imageNamed:_delImg] forState:0];
                 delbtn.tag = 100+i;
                 [delbtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
                 [self addSubview:delbtn];
             }
         }
         if ([obj isKindOfClass:[UIImage class]]) {
             imgV.image = obj;
         }else{
             [imgV sd_setImageWithURL:[NSURL URLWithString:obj]];
         }
          if (i == count-1) {
              max_h = CGRectGetMaxY(imgV.frame)+10;
          }
      }
      self.height = max_h;
    }
}
-(void)btnClick:(UIButton*)sender{
    if (self.click) {
        self.click(sender.tag);
    }
}
-(void)tap:(UITapGestureRecognizer*)tap{
    if (self.click) {
        self.click(tap.view.tag);
    }
}
@end
