//
//  FeedBackListVC.m
//  ZZR
//
//  Created by null on 2021/7/7.
//

#import "FeedBackListVC.h"
#import "FeedBackCell.h"
@interface FeedBackListVC ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FeedBackListVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"历史反馈";
    [self initUI];
    [self getListInfo];
}

#pragma mark – UI
- (void)initUI{
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 0.5;
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,0)];
    view.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorColor = UIColorFromRGB(0xEEEEEE);
    _tableView.tableFooterView = view;
    [_tableView registerNib:[UINib nibWithNibName:@"FeedBackCell" bundle:nil] forCellReuseIdentifier:@"FeedBackCell"];
}

#pragma mark – Network
- (void)getListInfo{
    [NetworkingTool getWithUrl:kFeedBackLogURL params:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            NSArray * dataList = responseObject[@"data"];
            for (NSDictionary * dict in dataList) {
                FeedBackModel * model = [FeedBackModel mj_objectWithKeyValues:dict];
                [self.dataArray addObject:model];
            }
        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = self.dataArray.count != 0;
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools hideHud];
    } IsNeedHub:YES];
}

#pragma mark – Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FeedBackCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FeedBackCell" forIndexPath:indexPath];
    cell.selectionStyle = 0;
    FeedBackModel * model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}
#pragma mark - Function
#pragma mark – XibFunction
#pragma mark – lazy load

@end
