//
//  FeedBackModel.h
//  ZZR
//
//  Created by null on 2021/7/7.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FeedBackModel : BaseModel

@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *contact;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *imgArray;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *replyContent; // 平台回复内容
@property (nonatomic, strong) NSArray *typeContents; // 类型
@property (nonatomic, strong) NSString *typeIds;
@property (nonatomic, strong) NSString *userId;


//
//@property (nonatomic, copy) NSString *update_time;
//@property (nonatomic, strong) NSNumber *status;
//@property (nonatomic, copy) NSString *replay;
//@property (nonatomic, strong) NSNumber *id;
//@property (nonatomic, copy) NSString *contact;
//@property (nonatomic, strong) NSNumber *is_replay;
//@property (nonatomic, strong) NSArray *thumb;
//@property (nonatomic, copy) NSString *create_time;
//@property (nonatomic, copy) NSString *type;
//@property (nonatomic, copy) NSString *body;
//@property (nonatomic, strong) NSNumber *user_id;

@end

NS_ASSUME_NONNULL_END


