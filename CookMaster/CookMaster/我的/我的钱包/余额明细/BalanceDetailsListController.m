//
//  BalanceDetailsListController.m
//  ZZR
//
//  Created by null on 2021/9/14.
//

#import "BalanceDetailsListController.h"
#import "BalanceDetailsListCell.h"

@interface BalanceDetailsListController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, copy) NSString *start_data;
@property (nonatomic, copy) NSString *end_data;

@end

@implementation BalanceDetailsListController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _start_data = @"";
    _end_data = @"";
    [self initTableView];
}

#pragma mark – UI
- (void)initTableView {
    [self.tableView registerNib:[UINib nibWithNibName:@"BalanceDetailsListCell" bundle:nil] forCellReuseIdentifier:@"BalanceDetailsListCell"];
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        @strongify(self);
        [self refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadMore];
    }];
    [self refresh];
}

#pragma mark - Network

- (void)refresh {
    self.pageNum = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}

- (void)loadMore {
    self.pageNum++;
    [self getDataArrayFromServerIsRefresh:NO];
}

/** 5cc45422e5c87    余额明细，我的钱包首页的30天记录也走这个，其中时间格式start_data: 2022-04-01 end_data: 2022-04-22
 
 **/
- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"pageNo"] = @(self.pageNum);
    params[@"pageSize"] = @(10);
    ///1收入 2支出
    if (self.fromType == 1){
        params[@"billType"] = @(1);
    }
    else if (self.fromType == 2){
        params[@"billType"] = @(2);
    }
    if(_start_data.length == 0 ){
        params[@"startTime"] = _start_data;
        params[@"endTime"] = _end_data;
    }else{
        params[@"startTime"] = [NSString stringWithFormat:@"%@ 00:00:00",_start_data];
        params[@"endTime"] = [NSString stringWithFormat:@"%@ 23:59:59",_end_data];
    }
  
//    page    Integer[整数]    选填            页码
//    change_type    Integer[整数]    必填    0        类型 0 全部 1 充值记录 2 消费明细 3 收入记录 4 支出记录
//    start_data    String[字符串]    选填            开始时间
//    end_data    String[字符串]    选填            结束时间
//
    [NetworkingTool getWithUrl:kFlowRecordURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSArray *tempList = [BalanceDetailsModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"records"]];
            [self.dataArray addObjectsFromArray:tempList];
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = self.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - Delegate
#pragma mark UITableViewDelgate
#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BalanceDetailsListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BalanceDetailsListCell" forIndexPath:indexPath];
    if (indexPath.row < self.dataArray.count) {
        cell.detailsModel = self.dataArray[indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark – Function
#pragma mark – XibFunction
#pragma mark - Lazy Loads

- (void)setTimeString:(NSString *)timeString {
    _timeString = timeString;
    _start_data = [[timeString componentsSeparatedByString:@"至"] firstObject];
    _end_data = [[timeString componentsSeparatedByString:@"至"] lastObject];
    [self refresh];
}

@end
