//
//  BalanceDetailsViewController.m
//  ZZR
//
//  Created by null on 2021/9/14.
//

#import "BalanceDetailsViewController.h"
#import "BalanceDetailsListController.h"
#import "FSSegmentTitleView.h"
#import "FSPageContentView.h"
#import "SignInView.h"

@interface BalanceDetailsViewController () <FSSegmentTitleViewDelegate, FSPageContentViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (nonatomic, strong) FSSegmentTitleView *titleDataView;
@property (nonatomic, strong) FSPageContentView *pageContentView;

@property (nonatomic, copy) NSArray *titleArray;
@property (nonatomic, strong) NSMutableArray *vcArray;

@end

@implementation BalanceDetailsViewController

#pragma mark - Life Cycle Methods

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [SignInView reinitialize];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"余额明细";
    [self setNavigationRightBarButtonWithImage:@"路径 19589"];
    [self initTitleView];
    [self initContentViewControllers];
}

#pragma mark – UI

- (void)initTitleView {
    self.titleArray = @[@"全部", @"收入记录", @"支出记录"];
    self.titleDataView = [[FSSegmentTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30) titles:self.titleArray delegate:self indicatorType:FSIndicatorTypeEqualTitle];
    self.titleDataView.backgroundColor = [UIColor clearColor];
    self.titleDataView.titleFont = [UIFont systemFontOfSize:16];
    self.titleDataView.titleSelectFont =  [UIFont systemFontOfSize:16];
    self.titleDataView.indicatorColor = MainColor;

    self.titleDataView.titleNormalColor = UIColorNamed(@"333333");
    self.titleDataView.titleSelectColor = MainColor;
    [self.titleDataView setSelectIndex:0];
    [self.topView addSubview:self.titleDataView];
    [self.titleDataView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.topView);
        make.height.equalTo(@44);
        make.centerY.equalTo(self.topView);
    }];
}

- (void)initContentViewControllers {
    [self.pageContentView removeFromSuperview];
    [self.pageContentView removeAllSubviews];
    for (int i = 0; i < self.titleArray.count; i++) {
        BalanceDetailsListController *listVC = [BalanceDetailsListController new];
        listVC.fromType = i;
        [self.vcArray addObject:listVC];
    }
    self.pageContentView = [[FSPageContentView alloc] initWithFrame:CGRectMake(0, 60, SCREEN_WIDTH, SCREEN_HEIGHT - NavAndStatusHight - 60 - bottomBarH) childVCs:self.vcArray parentVC:self delegate:self];
    [self.pageContentView setContentViewCurrentIndex:0];
    [self.pageContentView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.pageContentView];
}

#pragma mark - Network
#pragma mark - Delegate
#pragma mark UITableViewDelgate
#pragma mark - FSSegmentTitleViewDelegate

- (void)FSSegmentTitleView:(FSSegmentTitleView *)titleView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex {
    [self.pageContentView setContentViewCurrentIndex:endIndex];
}

#pragma mark - FSContenViewDidEndDecelerating

- (void)FSContenViewDidEndDecelerating:(FSPageContentView *)contentView startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex {
    [self.titleDataView setSelectIndex:endIndex];
}

#pragma mark – Function
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender
{
    MJWeakSelf;
    [SignInView title:@"选择时间" show:^(NSString * _Nonnull start, NSString * _Nonnull end) {

        BalanceDetailsListController *allListVC = weakSelf.vcArray[0];
        allListVC.timeString = [NSString stringWithFormat:@"%@至%@", start, end];
        BalanceDetailsListController *incomeListVC = weakSelf.vcArray[1];
        incomeListVC.timeString = [NSString stringWithFormat:@"%@至%@", start, end];
        BalanceDetailsListController *payListVC = weakSelf.vcArray[2];
        payListVC.timeString = [NSString stringWithFormat:@"%@至%@", start, end];
    }];
}

#pragma mark – XibFunction
#pragma mark - Lazy Loads

- (NSMutableArray *)vcArray {
    if (!_vcArray) {
        _vcArray = [NSMutableArray array];
    }
    return _vcArray;
}

@end
