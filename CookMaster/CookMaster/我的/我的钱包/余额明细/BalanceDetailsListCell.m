//
//  BalanceDetailsListCell.m
//  ZZR
//
//  Created by null on 2021/9/14.
//

#import "BalanceDetailsListCell.h"

@implementation BalanceDetailsListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setDetailsModel:(BalanceDetailsModel *)detailsModel {
    _detailsModel = detailsModel;
    
    [_titleLabel setText:detailsModel.title];
    [_timeLabel setText:detailsModel.createTime];
    BOOL isAdd = [detailsModel.sign isEqual:@"+"];
    NSString *symbol = detailsModel.sign;
    [_moneyLabel setText:[symbol stringByAppendingString:detailsModel.changeMoney]];
    [_moneyLabel setTextColor:isAdd ? UIColorNamed(@"333333") : UIColorNamed(@"999999")];
    self.moneyLabel.attributedText = [detailsModel.changeMoney attributedWithAdd:isAdd bigFont:16 smallFont:11];
//提现审核状态：0-未到账 1-审核未通过 2-已到账
    [_statusLabel setText:[self getStatus:detailsModel.status]];

    
}
-(NSString *) getStatus:(NSString *)status{
    NSString *statusStr = @"";
    switch (status.intValue) {
        case 0:
            statusStr = @"未到账";
            break;
        case 1:
            statusStr = @"审核未通过";
            break;
        case 2:
            statusStr = @"已到账";
            break;
        default:
            break;
    }
    return  statusStr;
}
- (void)setWModel:(WithdrawListModel *)wModel{
    _wModel = wModel;
    [_titleLabel setText:@"余额提现"];
    [_timeLabel setText:wModel.createTime];
    BOOL isAdd = NO;
    [_moneyLabel setTextColor:isAdd ? UIColorNamed(@"333333") : UIColorNamed(@"999999")];
    self.moneyLabel.attributedText = [wModel.money attributedWithAdd:isAdd bigFont:16 smallFont:11];
    [_statusLabel setText:[self getStatus:wModel.status]];

}

- (void)setIsWithdraw:(BOOL)isWithdraw {
    _isWithdraw = isWithdraw;
    _statusLabel.hidden = NO;
}

@end
