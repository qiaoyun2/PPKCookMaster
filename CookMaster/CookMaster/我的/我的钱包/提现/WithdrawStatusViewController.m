//
//  WithdrawStatusViewController.m
//  ZZR
//
//  Created by null on 2021/9/14.
//

#import "WithdrawStatusViewController.h"

@interface WithdrawStatusViewController ()

@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@end

@implementation WithdrawStatusViewController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"提现申请成功";
    self.isDark = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    NSString *way = @"";
    if (_withdrawType == 1) {
        way = @"微信";
    } else if (_withdrawType == 2) {
        way = @"支付宝";
    } else {
        way = @"银行卡";
    }
    NSString *attString = [NSString stringWithFormat:@"提现方式：%@\n提现金额：%@元", way, _money];
    [_moneyLabel setText:attString];
    
    NSMutableArray *childs = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [childs removeObjectAtIndex:childs.count - 2];
    self.navigationController.viewControllers = childs;
}

#pragma mark – UI

- (void)backItemClicked {
    [super backItemClicked];
}

#pragma mark - Network
#pragma mark - Delegate
#pragma mark UITableViewDelgate
#pragma mark – Function
#pragma mark – XibFunction
#pragma mark - Lazy Loads

@end
