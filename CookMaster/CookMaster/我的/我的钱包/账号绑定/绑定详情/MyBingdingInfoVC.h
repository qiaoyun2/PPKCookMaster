//
//  MyBingdingInfoVC.h
//  ZZR
//
//  Created by null on 2021/3/16.
//  Copyright © 2021 null. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyBingdingInfoVC : BaseViewController

/// 1:微信
@property (nonatomic, assign) NSInteger state;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/// 是否来自绑定页
@property (nonatomic, assign) BOOL isFromSetting;

@end

NS_ASSUME_NONNULL_END
