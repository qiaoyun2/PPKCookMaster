//
//  MyBingdingInfoCell2.m
//  ZZR
//
//  Created by null on 2021/3/16.
//  Copyright © 2021 null. All rights reserved.
//

#import "MyBingdingInfoCell2.h"


@interface MyBingdingInfoCell2 () <TZImagePickerControllerDelegate>

@end

@implementation MyBingdingInfoCell2

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

/// 相册选择头像、拍照
- (IBAction)showImagePickerAction:(id)sender {
    TZImagePickerController *imagePickerVC = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    imagePickerVC.statusBarStyle = UIStatusBarStyleDefault;
    if (@available(iOS 13.0, *)) {
        imagePickerVC.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    [[LJTools topViewController] presentViewController:imagePickerVC animated:YES completion:nil];
}

#pragma mark - TZImagePickerControllerDelegate

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto infos:(NSArray<NSDictionary *> *)infos {
    UIImage *image = [photos firstObject];
    [_addImg setImage:image];
    !_didChooseImage ?: _didChooseImage(image);
}

@end
