//
//  MyBindingVC.h
//  ZZR
//
//  Created by null on 2021/3/16.
//  Copyright © 2021 null. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyBindingVC : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
