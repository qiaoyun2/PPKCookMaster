//
//  MyBindingVC.m
//  ZZR
//
//  Created by null on 2021/3/16.
//  Copyright © 2021 null. All rights reserved.
//

#import "MyBindingVC.h"
#import "MyBindingCell.h"
#import "MyBingdingInfoVC.h"
//

@interface MyBindingVC ()

@end

@implementation MyBindingVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"账号绑定";
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    self.tableView.rowHeight = 62;
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor = UIColorFromRGB(0xEEEEEE);
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 16, 0, 16);
}

#pragma mark - Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *titleA = @[@"绑定支付宝", @"绑定微信", ];
//
    NSArray *imgA = @[@"路径 19587", @"路径 19218",];
    static NSString *identifier = @"MyBindingCell";
    MyBindingCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell= [[[NSBundle mainBundle] loadNibNamed:@"MyBindingCell" owner:self options:nil] lastObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor whiteColor];
    cell.titleL.text = titleA[indexPath.row];
    cell.img.image = [UIImage imageNamed:imgA[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MyBingdingInfoVC *info = [[MyBingdingInfoVC alloc] init];
    info.state = indexPath.row;
    info.isFromSetting = YES;
    [self.navigationController pushViewController:info animated:YES];
}

@end
