//
//  BalanceDetailsModel.h
//  ZZR
//
//  Created by null on 2021/12/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BalanceDetailsModel : NSObject

/// 变动金额
@property (nonatomic, copy) NSString *changeMoney;
/// 创建时间
@property (nonatomic, copy) NSString *createTime;
/// +
@property (nonatomic, copy) NSString *sign;
/// 提现审核状态
@property (nonatomic, copy) NSString *status; // 0提现审核状态：0-未到账 1-审核未通过 2-已到账
@property (nonatomic, copy) NSString *title; //



/**           废弃                    */

/// 变动后金额
@property (nonatomic, copy) NSString *after_money;
///
@property (nonatomic, copy) NSString *aid;
/// 变动前金额
@property (nonatomic, copy) NSString *before_money;
/// 变动金额
@property (nonatomic, copy) NSString *change_money;
/// 操作类型 1会员充值 2发布任务 3管理员操作 4会员提现 5管理员拒绝提现，返还金额 6会员分成 7余额支付 8分享赚 9自购返 10分销佣金
@property (nonatomic, assign) NSInteger change_type;
/// 状态为提现的时候 0:待审核 1:已到账 2:已拒绝
@property (nonatomic, assign) NSInteger check_status;
/// 创建时间
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *goods_id;
@property (nonatomic, copy) NSString *order_no;
/// 备注信息，开发看
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *t;
@property (nonatomic, copy) NSString *user_id;

@property (nonatomic, strong) NSNumber *sku_id;
@property (nonatomic, copy) NSString *check_reason;
/// 自己扩展
/// 状态为提现的时候 0:待审核 1:已到账 2:已拒绝
@property (nonatomic, copy) NSString *check_status_name;

@end

//"aid": 5,
//"user_id": 4,
//"before_money": "99407.00",
//"change_money": "-400.00",
//"after_money": "99007.00",
//"change_type": 2,
//"create_time": "2022-04-12 14:40:08",
//"remark": "余额支付",
//"order_no": "YJ20220412143954915489",
//"goods_id": 0,
//"sku_id": 0,
//"check_reason": "",
//"t": 2
NS_ASSUME_NONNULL_END
