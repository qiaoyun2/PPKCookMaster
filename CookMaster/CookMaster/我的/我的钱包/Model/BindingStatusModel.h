//
//  BindingStatusModel.h
//  ZZR
//
//  Created by null on 2021/12/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BindingStatusModel : NSObject

@property (nonatomic, assign) BOOL zfbIsBind;
@property (nonatomic, assign) BOOL is_bank;
@property (nonatomic, assign) BOOL wxIsBind;

@end

NS_ASSUME_NONNULL_END
