//
//  MyMessageTableViewCell.h
//  PPKMaster
//
//  Created by qiaoyun on 2022/10/12.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyMessageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIView *dianLabel;
@property (weak, nonatomic) IBOutlet UIButton *delectBtn;

@property (nonatomic, strong) MessageModel *model;

@end

NS_ASSUME_NONNULL_END
