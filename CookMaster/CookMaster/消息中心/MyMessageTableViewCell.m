//
//  MyMessageTableViewCell.m
//  PPKMaster
//
//  Created by qiaoyun on 2022/10/12.
//

#import "MyMessageTableViewCell.h"

@implementation MyMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setModel:(MessageModel *)model{
    _model = model;
    self.titleLabel.text = model.title;
    self.detailLabel.text = model.content;
    self.timeLabel.text = model.publishTime;
    self.dianLabel.hidden = model.readFlag.intValue == 1;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
