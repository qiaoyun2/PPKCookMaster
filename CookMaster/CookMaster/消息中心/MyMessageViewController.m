//
//  MyMessageViewController.m
//  PPKMaster
//
//  Created by qiaoyun on 2022/10/12.
//

#import "MyMessageViewController.h"
#import "MyMessageTableViewCell.h"
#import "MessageModel.h"

#import "MyMessageDetailViewController.h"

@interface MyMessageViewController ()

@property (nonatomic, assign) NSInteger page;
@end

@implementation MyMessageViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    self.navigationItem.title = @"消息中心";
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];

}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"type"] = @(self.type);
    NSString *url = kMessageListByTypeURL;

    WeakSelf
    [NetworkingTool getWithUrl:url params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [weakSelf.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                MessageModel *model = [[MessageModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf addBlankOnView:weakSelf.tableView];
        weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyMessageTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MyMessageTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    MessageModel *model = self.dataArray[indexPath.row];
   
    if(self.type == 2){
        model.readFlag = @"1";
    }
    cell.model = model;
    WeakSelf
    [cell.delectBtn jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除该消息吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForDeleteOrder:model];
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageModel *model = self.dataArray[indexPath.row];
    if (self.type == 1) {
        [self requestForRead:model];
        MyMessageDetailViewController *vc = [[MyMessageDetailViewController alloc] init];
        vc.titleStr = model.title;
        vc.messageId = model.messageId;
        [self.navigationController pushViewController:vc animated:YES];
    }
   
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}
/*
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 删除
    return UITableViewCellEditingStyleDelete;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageModel *model = self.dataArray[indexPath.row];
    WeakSelf
    [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
        [weakSelf requestForDeleteOrder:model];
    } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
}
 */

#pragma mark - 设为已读
- (void)requestForRead:(MessageModel *)model
{
    NSString *url = kMessageReadURL;
    WeakSelf
    [NetworkingTool postWithUrl:url params:@{@"messageId":model.messageId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - 删除
- (void)requestForDeleteOrder:(MessageModel *)model {
    
    NSString *url = kMessageDeleteByIdURL;
    WeakSelf
    [NetworkingTool postWithUrl:url params:@{@"messageId":model.messageId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            [self.dataArray removeObject:model];
            [self.tableView reloadData];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {

    } IsNeedHub:YES];
}
@end
