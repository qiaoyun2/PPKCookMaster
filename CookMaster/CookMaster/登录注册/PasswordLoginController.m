//
//  PasswordLoginController.m
//  PPK
//
//  Created by null on 2022/3/3.
//

#import "PasswordLoginController.h"
#import "ForgetPasswordViewController.h"
#import "RWTabbarController.h"

@interface PasswordLoginController ()
@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *secureButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (weak, nonatomic) IBOutlet UIView *threeLoginView;

@end

@implementation PasswordLoginController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.logoImageViewTop.constant = 40+StatusHight;
    self.contentViewHeight.constant = SCREEN_HEIGHT > 667 ? SCREEN_HEIGHT : 667;
    
    [self updateLogin];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLogin) name:LoginChangeSuccess object:nil];

}
#pragma mark – UI
-(void)updateLogin{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:AppIsOnline];
    self.threeLoginView.hidden = YES;
    if ([str isEqualToString:@"1"]) {
        self.threeLoginView.hidden = NO;
        [self.view layoutIfNeeded];
    }
}
- (IBAction)loginButtonAction:(id)sender {
    [self.view endEditing:YES];
    if (self.mobileField.text.length == 0) {
        [LJTools showNOHud:@"请输入手机号" delay:1];
        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showNOHud:@"请输入正确的手机号" delay:1];
        return;
    }
    if ([self.passwordField.text isBlankString]) {
        [LJTools showNOHud:@"请输入密码" delay:1];
        return;
    }
//    if ([self.passwordField.text isPassword]) {
//        [LJTools showNOHud:@"请输入密码" delay:1];
//        return;
//    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"mobile"] = self.mobileField.text;
    parameters[@"password"] = self.passwordField.text;
    [NetworkingTool postWithUrl:kMobileLoginURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *userDic = responseObject[@"data"][@"user"];
            User *user = [User mj_objectWithKeyValues:userDic];
            [User saveUser:user];
            [UIApplication sharedApplication].keyWindow.rootViewController = [[TabBarController alloc] init];
        } else {
            
        }
        [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];

}

- (IBAction)secureButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)codeLoginButtonAction:(id)sender {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)forgetButtonAction:(id)sender {
    ForgetPasswordViewController *vc = [[ForgetPasswordViewController alloc] init];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)thirdButtonAction:(UIButton *)sender {
    @weakify(self);
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:self completion:^(id result, NSError *error) {
        if (error) {
            /// 第三方登录数据(为空表示平台未提供)
            NSLog(@"=====> error : %@", error);
        } else {
            @strongify(self);
            [self wechatLogin:result];
        }
    }];
}
/// 微信登录
/// @param result <#result description#>
- (void)wechatLogin:(id)result {
    
    UMSocialUserInfoResponse *response = result;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//    parameters[@"type"] = @"1";
    parameters[@"openId"] = response.unionId;
    
    [NetworkingTool postWithUrl:kThirdLoginURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            User *user = [User mj_objectWithKeyValues:responseObject[@"data"][@"user"]];
            [User saveUser:user];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else if ([responseObject[@"code"] intValue] == 0) {
            ForgetPasswordViewController *vc = [ForgetPasswordViewController new];
            vc.openId = response.unionId;
            vc.type =  2;
            [self.navigationController pushViewController:vc animated:YES];
            
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

@end
