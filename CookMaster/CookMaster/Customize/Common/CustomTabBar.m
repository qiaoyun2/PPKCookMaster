//
//  CustomTabBar.m
//  ShenglongLive
//
//  Created by null on 2017/12/13.
//  Copyright © 2017年 尹冲. All rights reserved.
//

#import "CustomTabBar.h"
#import "LoginViewController.h"

@interface CustomTabBar ()

@property (nonatomic, strong) UIView *tempBgView;

@end

@implementation CustomTabBar

- (ReLayoutButton *)publishButton {
    if (!_publishButton) {
        _publishButton = [ReLayoutButton buttonWithType:UIButtonTypeCustom];
        _publishButton.layoutType = 1;
        _publishButton.margin = 1;
        [_publishButton setTitle:@"发布" forState:UIControlStateNormal];
        [_publishButton setTitleColor:UIColorFromRGB(0x85879B) forState:UIControlStateNormal];
        [_publishButton setImage:[UIImage imageNamed:@"组 52921"] forState:UIControlStateHighlighted];
        [_publishButton setImage:[UIImage imageNamed:@"组 52921"] forState:UIControlStateNormal];
        [_publishButton.titleLabel setFont:[UIFont systemFontOfSize:10]];
        [_publishButton addTarget:self action:@selector(publishClick) forControlEvents:UIControlEventTouchUpInside];
        //取消中间button点击事件
        _publishButton.userInteractionEnabled = NO;
        [self addSubview:_publishButton];
    }
    return _publishButton;
}

- (UIView *)tempBgView {
    if (!_tempBgView) {
        _tempBgView = [UIView new];
        _tempBgView.frame = CGRectMake(0, 0, 48, 48);
        _tempBgView.backgroundColor = [UIColor whiteColor];
        _tempBgView.layer.cornerRadius = 24;
        _tempBgView.clipsToBounds = YES;
        _tempBgView.userInteractionEnabled = NO;
        [self addSubview:_tempBgView];
    }
    return _tempBgView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    /**** 设置所有UITabBarButton的frame ****/
    // 按钮的尺寸
    CGFloat buttonW = self.frame.size.width / 5;
    CGFloat buttonH = 55;
    CGFloat buttonY = 0;
    // 按钮索引
    int buttonIndex = 0;
    
    for (UIView *subview in self.subviews) {
        // 过滤掉非UITabBarButton
        if (![@"UITabBarButton" isEqualToString:NSStringFromClass(subview.class)]) continue;
        if (subview.class != NSClassFromString(@"UITabBarButton")) continue;
        
        // 设置frame
        CGFloat buttonX = buttonIndex * buttonW;
        // 把发布按钮的位置预留出来
//        if (buttonIndex >= 2) { // 右边的2个UITabBarButton
//            buttonX += buttonW;
//        }
        subview.frame = CGRectMake(buttonX, buttonY, buttonW, buttonH);
        // 增加索引
        buttonIndex++;
    }
    
    self.tempBgView.center = CGPointMake(self.frame.size.width * 0.5, 48 * 0.5 - 16);
    
    /**** 设置中间的发布按钮的frame ****/
    self.publishButton.frame = CGRectMake(0, 0, 58, 58);
    self.publishButton.center = CGPointMake(self.frame.size.width * 0.5, self.tempBgView.centerY + 8);
}
//中间button点击事件
-(void)publishClick{
    
}

@end
