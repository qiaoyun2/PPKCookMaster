//
//  TabBarController.m
//  HOOLA
//
//  Created by null on 2018/8/18.
//  Copyright © 2018 null. All rights reserved.
//

#import "TabBarController.h"
#import "NavigationController.h"
#import "CustomTabBar.h"
#import "RWOrderController.h"
#import "HomeViewController.h"
#import "MineViewController.h"

@interface TabBarController ()<UITabBarControllerDelegate, UITabBarDelegate>

@property (nonatomic, strong) UIImageView *bgImageView;

@end

@implementation TabBarController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBar.tintColor = UIColorFromRGB(0x422E27);
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    // Do any additional setup after loading the view.
    self.tabBar.translucent = YES;

    [self setTabBar];
    self.selectedIndex = 0;
    self.delegate = self;
    // 设置一个自定义 View,大小等于 tabBar 的大小
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    // 给自定义 View 设置颜色
    bgView.backgroundColor = RGB(255, 255, 255);
    // 将自定义 View 添加到 tabBar 上
    [self.tabBar insertSubview:bgView atIndex:0];
    // 未选择颜色设置
    [self.tabBar setUnselectedItemTintColor:UIColorFromRGB(0xBFBFBF)];
    // 设置选择颜色
    self.tabBar.tintColor = MainColor;
    [self.tabBar setBackgroundImage:[UIImage new]];
    [self.tabBar setShadowImage:[UIImage new]];
    self.tabBar.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.tabBar.layer.shadowOffset = CGSizeMake(0, -1);
    self.tabBar.layer.shadowOpacity = 0.3;
}

- (void)setTabBar {
    /**** 添加子控制器 ****/
    [self setupOneChildViewController:[HomeViewController new] title:@"首页" image:@"组 14956" selectedImage:@"组 14956-1"];
    [self setupOneChildViewController:[RWOrderController new] title:@"订单" image:@"组 14959" selectedImage:@"组 14959-1"];
    [self setupOneChildViewController:[MineViewController new] title:@"我的" image:@"组 14960" selectedImage:@"组 14960-1"];
}

- (void)setupOneChildViewController:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage {
    if (title.length) { // 图片名有具体值，判断图片传入值是空还是nil
        UIImage *tabImage = [UIImage imageNamed:image];
        tabImage = [tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.image = tabImage;
        UIImage *selecttabImage = [UIImage imageNamed:selectedImage];
        selecttabImage = [selecttabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = selecttabImage;
        
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        //颜色属性
        attributes[NSForegroundColorAttributeName] = UIColorFromRGB(0xBFBFBF);
        //字体大小属性
        //还有一些其他属性的key可以去NSAttributedString.h文件里去找
        attributes[NSFontAttributeName] = [UIFont systemFontOfSize:10];
        
        NSMutableDictionary *selectAttri = [NSMutableDictionary dictionary];
        selectAttri[NSForegroundColorAttributeName] = MainColor;
        selectAttri[NSFontAttributeName] = [UIFont systemFontOfSize:10];
        
        vc.tabBarItem.title = title;
        //设置为选中状态的文字属性
        [vc.tabBarItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
        //设置选中状态的属性
        [vc.tabBarItem setTitleTextAttributes:selectAttri forState:UIControlStateSelected];
        
//        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(-4, 0, 4, 0);
        [vc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -3)];
    }
    NavigationController *nav = [[NavigationController alloc] initWithRootViewController:vc];
    [self addChildViewController:nav];
}

//判断是否跳转
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    if ([tabBarController.tabBar.selectedItem.title isEqualToString:@""] || [tabBarController.tabBar.selectedItem.title isEqualToString:@""]) {
        NavigationController *nav = tabBarController.selectedViewController;
        if ([LJTools panduanLoginWithViewContorller:nav.viewControllers[0] isHidden:NO]){
            return YES;
        }
        return NO;
    }else{
        return YES;
    }
}


@end
