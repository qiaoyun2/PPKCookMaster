//
//  BaseModel.h
//  地铁
//
//  Created by 李 on 16/12/5.
//  Copyright © 2016年 null. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface BaseModel : NSObject 

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
