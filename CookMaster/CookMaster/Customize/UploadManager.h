//
//  UploadManager.h
//  ShengYuanUser
//
//  Created by null on 2021/1/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UploadManager : NSObject
+(void)uploadImageArray:(NSArray *)imageArray block:(void (^)(NSString *ids, NSString *imageUrl))block;
+(void)uploadWithImageArray:(NSArray *)imageArray block:(void(^)(NSString *imageUrl))block;
+(void)uploadWithVideoData:(NSData *)VideoData block:(void(^)(NSString *videoUrl))block;
@end

NS_ASSUME_NONNULL_END
