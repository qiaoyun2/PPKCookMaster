//
//  NSObject+Category.m
//  daycareParent
//
//  Created by Git on 2017/11/27.
//  Copyright © 2017年 XueHui. All rights reserved.
//

#import "NSObject+Category.h"
#import "NSString+Category.h"

@implementation NSObject (Category)
-(id)safeStringZero
{
    if (self==nil||[self isKindOfClass:[NSNull class]])
    {
        return @"0";
    }
    else if([self isKindOfClass:[NSString class]])
    {
        NSString *title=(NSString *)self;
        if ([title isEqualToString:@"null"]||[title isEqualToString:@"<null>"]||[title isEqualToString:@"(null)"]||[title isEqualToString:@"nil"]||[title isEqualToString:@"(nil)"]||[title isEqualToString:@"<nil>"])
        {
            return @"0";
        }
        else
        {
            return [NSString stringWithFormat:@"%@", title];
        }
        
    }
    else if ([self isKindOfClass:[NSNumber class]])
    {
        NSNumber *valu = (NSNumber *)self;
        if (strcmp([valu objCType], @encode(float)) == 0)
        {
            return [NSString stringWithFormat:@"%.2f",[valu floatValue]];
        }
        else if (strcmp([valu objCType], @encode(double)) == 0)
        {
            return [NSString stringWithFormat:@"%.2f",[valu doubleValue]];
        }
        else if (strcmp([valu objCType], @encode(int)) == 0)
        {
            return [NSString stringWithFormat:@"%d",[valu intValue]];
        }
        else if (strcmp([valu objCType], @encode(BOOL)) == 0)
        {
            return [NSString stringWithFormat:@"%d",[valu boolValue]];
        }else if (strcmp([valu objCType], @encode(long)) == 0)
        {
            return [NSString stringWithFormat:@"%ld",[valu longValue]];
        }
        else
        {
            return [NSString stringWithFormat:@"%@",valu];
        }
    }
    else
    {
        return @"0";
    }
    return self;
    
}

-(id)safeString
{
    if([self isKindOfClass:[NSString class]])
    {
        NSString *title=(NSString *)self;
        if ([title isEqualToString:@"null"] || [title isEqualToString:@"<null>"]||[title isEqualToString:@"(null)"]||[title isEqualToString:@"nil"]||[title isEqualToString:@"(nil)"]||[title isEqualToString:@"<nil>"])
        {
            return @"";
        }
        else
        {
            return [NSString stringWithFormat:@"%@", title];
        }
        
    }
    else if ([self isKindOfClass:[NSNumber class]])
    {
        NSNumber *valu = (NSNumber *)self;
        if (strcmp([valu objCType], @encode(float)) == 0)
        {
            return [NSString stringWithFormat:@"%.2f",[valu floatValue]];
        }
        else if (strcmp([valu objCType], @encode(double)) == 0)
        {
            return [NSString stringWithFormat:@"%.2f",[valu doubleValue]];
        }
        else if (strcmp([valu objCType], @encode(int)) == 0)
        {
            return [NSString stringWithFormat:@"%d",[valu intValue]];
        }
        else if (strcmp([valu objCType], @encode(BOOL)) == 0)
        {
            return [NSString stringWithFormat:@"%d",[valu boolValue]];
        }else if (strcmp([valu objCType], @encode(long)) == 0)
        {
            return [NSString stringWithFormat:@"%ld",[valu longValue]];
        }
        else
        {
            return [NSString stringWithFormat:@"%@",valu];
        }
    }
    return @"";
}
-(NSDictionary *)safeDictionary
{
    if([self isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dic=(NSDictionary *)self;
        return dic;
        
    }
    return nil;
}
-(NSArray *)safeArray
{
    if([self isKindOfClass:[NSArray class]])
    {
        NSArray *array=(NSArray *)self;
        return array;
        
    }
    return nil;
}

-(void)setHeaderWithUrl:(NSString *)headerUrl
{
    if ([self isKindOfClass:[UIButton class]])
    {
        UIButton *btn=(UIButton *)self;
        [btn sd_setImageWithURL:[NSURL URLWithString:[LJTools chackUrl:headerUrl]] forState:UIControlStateNormal placeholderImage:DefaultImgHeader];
        return;
    }
    if ([self isKindOfClass:[UIImageView class]])
    {
        UIImageView *imageV=(UIImageView *)self;
        [imageV sd_setImageWithURL:[NSURL URLWithString:[LJTools chackUrl:headerUrl]] placeholderImage:DefaultImgHeader];
        return;
    }
}
-(void)setImageWithUrl:(NSString *)imageUrl
{
     
    if ([self isKindOfClass:[UIButton class]])
    {
        UIButton *btn=(UIButton *)self;
        if ([imageUrl isKindOfClass:[UIImage class]]) {
            [btn setImage:(UIImage *)imageUrl forState:UIControlStateNormal];
        }else
        {
           [btn sd_setImageWithURL:[NSURL URLWithString:[LJTools chackUrl:imageUrl]] forState:UIControlStateNormal placeholderImage:DefaultImgHeight];
        }
        
        return;
    }
    if ([self isKindOfClass:[UIImageView class]])
    {
        UIImageView *imageV=(UIImageView *)self;
        if ([imageUrl isKindOfClass:[UIImage class]]) {
            [imageV setImage:(UIImage *)imageUrl];
        }else
        {
          [imageV sd_setImageWithURL:[NSURL URLWithString:[LJTools chackUrl:imageUrl]] placeholderImage:DefaultImgHeight];
        }
        
        return;
    }
}
-(void)setSexImgView:(NSInteger)sex
{
    if ([self isKindOfClass:[UIButton class]])
    {
        UIButton *btn=(UIButton *)self;
        if (sex==1) {
            [btn setImage:[UIImage imageNamed:@"boy"] forState:UIControlStateNormal];
        }
        else
        {
            [btn setImage:[UIImage imageNamed:@"girl"] forState:UIControlStateNormal];
        }
        return;
    }
    if ([self isKindOfClass:[UIImageView class]])
    {
        UIImageView *imageV=(UIImageView *)self;
        if (sex==1) {
            imageV.image=[UIImage imageNamed:@"boy"];
        }
        else
        {
            imageV.image=[UIImage imageNamed:@"girl"];
        }
        return;
    }
}
#pragma mark 计算文本的宽度
- (CGFloat)getWidthWithTitle:(NSString *)title fontOfSize:(CGFloat)fontOfSize
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGFLOAT_MAX, 0)];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontOfSize];
    [label sizeToFit];
    return label.frame.size.width;
}
#pragma mark 计算文本的高度
- (CGFloat)getHeightWithTitle:(NSString *)title width:(CGFloat)width fontOfSize:(CGFloat)fontOfSize
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontOfSize];
    label.numberOfLines = 0;
    [label sizeToFit];
    CGFloat height = label.frame.size.height;
    return height;
}
#pragma mark - 获取行数
-(CGFloat)getRowsWithTitle:(NSString *)title width:(CGFloat)width fontOfSize:(CGFloat)fontOfSize spacing:(CGFloat)spacing
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGFLOAT_MAX, 0)];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontOfSize];
    [label sizeToFit];
    CGFloat rasd= (label.frame.size.width+spacing)/width;
    CGFloat rows= ceilf(rasd);
    return rows;
}
#pragma mark - 设置小数点前后字体大小
-(void)setAttributesPointFrontFont:(UIFont *)frontFont behindFont:(UIFont *)behindFont text:(NSString *)text
{
    //分隔字符串
       NSString *lastStr;
       NSString *firstStr;
       
       if ([text containsString:@"."]) {
           NSRange range = [text rangeOfString:@"."];
           lastStr = [text substringFromIndex:range.location];
           firstStr = [text substringToIndex:range.location];
       }
       NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc] initWithString:text];
       //小数点前面的字体大小
       [AttributedStr addAttribute:NSFontAttributeName
                             value:frontFont
                             range:NSMakeRange(0, firstStr.length)];
       //小数点后面的字体大小
       [AttributedStr addAttribute:NSFontAttributeName
                             value:behindFont
                             range:NSMakeRange(firstStr.length, lastStr.length)];
    [self setAttributedText:AttributedStr];
}

#pragma mark- 设置字体大小和颜色
-(void)setAttributesFrontTextColor:(UIColor *)frontTextColor frontFontOfSize:(UIFont *)frontFont fromeIndex:(NSInteger)fromeIndex toIndex:(NSInteger)toIndex behindTextColor:(UIColor *)behindTextColor behindFontOfSize:(UIFont *)behindFont text:(NSString *)text
{
    NSMutableAttributedString *str  = [[NSMutableAttributedString alloc] initWithString:text];
    
    [str addAttribute:NSForegroundColorAttributeName value:frontTextColor range:NSMakeRange(0, fromeIndex)];
    [str addAttribute:NSFontAttributeName value:frontFont range:NSMakeRange(0, fromeIndex)];
    
    [str addAttribute:NSForegroundColorAttributeName value:behindTextColor range:NSMakeRange(fromeIndex, toIndex)];
    [str addAttribute:NSFontAttributeName value:behindFont range:NSMakeRange(fromeIndex, toIndex)];
    [self setAttributedText:str];
}
#pragma mark- 设置字体大小
-(void)setAttributesFrontFontOfSize:(UIFont *)frontFont fromeIndex:(NSInteger)fromeIndex toIndex:(NSInteger)toIndex behindFontOfSize:(UIFont *)behindFont text:(NSString *)text
{
    NSMutableAttributedString *str  = [[NSMutableAttributedString alloc] initWithString:text];
    
    [str addAttribute:NSFontAttributeName value:frontFont range:NSMakeRange(0, fromeIndex)];
    
    [str addAttribute:NSFontAttributeName value:behindFont range:NSMakeRange(fromeIndex, toIndex)];
    [self setAttributedText:str];
}
#pragma mark- 设置字体颜色
-(void)setAttributesFrontTextColor:(UIColor *)frontTextColor fromeIndex:(NSInteger)fromeIndex toIndex:(NSInteger)toIndex behindTextColor:(UIColor *)behindTextColor text:(NSString *)text
{
    NSMutableAttributedString *str  = [[NSMutableAttributedString alloc] initWithString:text];
    
    [str addAttribute:NSForegroundColorAttributeName value:frontTextColor range:NSMakeRange(0, fromeIndex)];
    
    [str addAttribute:NSForegroundColorAttributeName value:behindTextColor range:NSMakeRange(fromeIndex, toIndex)];
    [self setAttributedText:str];
}
#pragma mark- 改变纯数字颜色和大小
-(void)setAttributesPureNumberColor:(UIColor *)color font:(UIFont *)font text:(NSString *)text
{
    NSArray *number = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    NSMutableAttributedString *attributeString  = [[NSMutableAttributedString alloc]initWithString:text];
    for (int i = 0; i < text.length; i ++)
    {
        //这里的小技巧，每次只截取一个字符的范围
        NSString *a = [text substringWithRange:NSMakeRange(i, 1)];
        //判断装有0-9的字符串的数字数组是否包含截取字符串出来的单个字符，从而筛选出符合要求的数字字符的范围NSMakeRange
        //NSUnderlineStyleSingle 带下划线
        if ([number containsObject:a])
        {
            [attributeString setAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleNone]} range:NSMakeRange(i, 1)];
        }
        
    }
   [self setAttributedText:attributeString];
}
#pragma mark- 改变纯数字颜色
-(void)setAttributesPureNumberColor:(UIColor *)color text:(NSString *)text
{
    NSArray *number = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    NSMutableAttributedString *attributeString  = [[NSMutableAttributedString alloc]initWithString:text];
    for (int i = 0; i < text.length; i ++)
    {
        //这里的小技巧，每次只截取一个字符的范围
        NSString *a = [text substringWithRange:NSMakeRange(i, 1)];
        //判断装有0-9的字符串的数字数组是否包含截取字符串出来的单个字符，从而筛选出符合要求的数字字符的范围NSMakeRange
        //NSUnderlineStyleSingle 带下划线
        if ([number containsObject:a])
        {
            [attributeString setAttributes:@{NSForegroundColorAttributeName:color,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleNone]} range:NSMakeRange(i, 1)];
        }
        
    }
   [self setAttributedText:attributeString];
}
#pragma mark- 改变纯数字大小
-(void)setAttributesPureNumberFont:(UIFont *)font text:(NSString *)text
{
    NSArray *number = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    NSMutableAttributedString *attributeString  = [[NSMutableAttributedString alloc]initWithString:text];
    for (int i = 0; i < text.length; i ++)
    {
        //这里的小技巧，每次只截取一个字符的范围
        NSString *a = [text substringWithRange:NSMakeRange(i, 1)];
        //判断装有0-9的字符串的数字数组是否包含截取字符串出来的单个字符，从而筛选出符合要求的数字字符的范围NSMakeRange
        //NSUnderlineStyleSingle 带下划线
        if ([number containsObject:a])
        {
            [attributeString setAttributes:@{NSFontAttributeName:font,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleNone]} range:NSMakeRange(i, 1)];
        }
        
    }
   [self setAttributedText:attributeString];
}
-(void)setAttributedStringFrontOfSize:(CGFloat)fontOfSize  behindFontOfSize:(CGFloat)behindFont text:(NSString *)text
{
   NSMutableAttributedString *str  = [[NSMutableAttributedString alloc] initWithString:text];

  [str addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:fontOfSize] range:NSMakeRange(0, 1)];

   [str addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:behindFont] range:NSMakeRange(1, text.length-1)];
     [self setAttributedText:str];
 }

#pragma mark--设置行间距和字间距
-(void)setAttributedStringSpaceWithFontOfSize:(CGFloat)fontOfSize withlineSpacing:(CGFloat)lineSpacing withAttributeNameFont:(CGFloat)attributeNameFont text:(NSString *)text
{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = lineSpacing; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSString *attributeName=[NSString stringWithFormat:@"%.1f",attributeNameFont];
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:fontOfSize], NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@([attributeName integerValue])};
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:text attributes:dic];
    
    [self setAttributedText:attributeStr];
}
#pragma mark--计算高度(带有行间距的情况)
-(CGFloat)getAttributedStringSpaceLabelHeight:(NSString*)str withFontOfSize:(CGFloat)fontOfSize withWidth:(CGFloat)width withlineSpacing:(CGFloat)lineSpacing withAttributeNameFont:(CGFloat)attributeNameFont
{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = lineSpacing;//行高
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSString *attributeName=[NSString stringWithFormat:@"%.1f",attributeNameFont];
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:fontOfSize], NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@([attributeName integerValue])};//字体间距
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingTruncatesLastVisibleLine |  NSStringDrawingUsesFontLeading attributes:dic context:nil].size;
    return size.height;
}

-(void)setAttributedText:(NSMutableAttributedString *)attributedString
{
    if ([self isKindOfClass:[UILabel class]])
    {
        UILabel *label=(UILabel *)self;
        [label setAttributedText:attributedString];
        return;
    }
    if ([self isKindOfClass:[UITextView class]])
    {
        UITextView *textView=(UITextView *)self;
        [textView setAttributedText:attributedString];
        return;
    }
    if ([self isKindOfClass:[UITextField class]])
    {
        UITextField *textField=(UITextField *)self;
        [textField setAttributedText:attributedString];
        return;
    }
    if ([self isKindOfClass:[UIButton class]])
    {
        UIButton *button=(UIButton *)self;
        [button.titleLabel setAttributedText:attributedString];
        return;
    }
}

#pragma mark- 判断是否为整数
-(BOOL)isPureInt:(NSString *)string
{
    NSScanner *scan=[NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val]&&[scan isAtEnd];
}
#pragma mark- 判断是否为纯数字
- (BOOL)validateNumber:(NSString*)number
{
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    int i = 0;
    while (i < number.length)
    {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0)
        {
            res = NO;
            break;
        }
        i++;
    }
    return res;

}
@end
