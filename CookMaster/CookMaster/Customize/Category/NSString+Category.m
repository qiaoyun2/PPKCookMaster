//
//  NSString+Category.m
//  daycareParent
//
//  Created by Git on 2017/11/27.
//  Copyright © 2017年 XueHui. All rights reserved.
//

#import "NSString+Category.h"

@implementation NSString (Category)


#pragma mark 字符串安全保护，防止字符串为 nil <null> 导致程序崩溃问题
/**
 @param title 传入的对象
 @return NSString 返回“ @"" ”的字符串
 */
+(id)safeString:(NSString*)title
{
    if([title isKindOfClass:[NSString class]])
    {
        if ([title isEqualToString:@"null"] || [title isEqualToString:@"<null>"]||[title isEqualToString:@"(null)"]||[title isEqualToString:@"nil"]||[title isEqualToString:@"(nil)"]||[title isEqualToString:@"<nil>"])
        {
            return @"";
        }
        else
        {
            return [NSString stringWithFormat:@"%@", title];
        }
        
    }
    else
    {
        return @"";
    }
   
}
+(id)safeStringZero:(NSString*)title
{
    if ([title isKindOfClass:[NSString class]])
    {
        if ([title isEqualToString:@"null"] || [title isEqualToString:@"<null>"]||[title isEqualToString:@"(null)"]||[title isEqualToString:@"nil"]||[title isEqualToString:@"(nil)"]||[title isEqualToString:@"<nil>"])
        {
            return @"0";
        }
        else
        {
             return [NSString stringWithFormat:@"%@",title];
        }
       
    }
    else
    {
          return @"0";
    }
  
}


-(NSString *)safeString
{
    if([self isKindOfClass:[NSString class]])
    {
        NSString *title=(NSString *)self;
        if ([title isEqualToString:@"null"] || [title isEqualToString:@"<null>"]||[title isEqualToString:@"(null)"]||[title isEqualToString:@"nil"]||[title isEqualToString:@"(nil)"]||[title isEqualToString:@"<nil>"])
        {
            return @"";
        }
        else
        {
            return [NSString stringWithFormat:@"%@", title];
        }
        
    }
    else if ([self isKindOfClass:[NSNumber class]])
    {
        NSNumber *valu = (NSNumber *)self;
        if (strcmp([valu objCType], @encode(float)) == 0)
        {
            return [NSString stringWithFormat:@"%.2f",[valu floatValue]];
        }
        else if (strcmp([valu objCType], @encode(double)) == 0)
        {
            return [NSString stringWithFormat:@"%.2f",[valu doubleValue]];
        }
        else if (strcmp([valu objCType], @encode(int)) == 0)
        {
            return [NSString stringWithFormat:@"%d",[valu intValue]];
        }
        else if (strcmp([valu objCType], @encode(BOOL)) == 0)
        {
            return [NSString stringWithFormat:@"%d",[valu boolValue]];
        }else if (strcmp([valu objCType], @encode(long)) == 0)
        {
            return [NSString stringWithFormat:@"%ld",[valu longValue]];
        }
        else
        {
            return [NSString stringWithFormat:@"%@",valu];
        }
    }
    return @"";
}
-(NSString *)safeStringZero
{
    if (self==nil||[self isKindOfClass:[NSNull class]])
    {
        return @"0";
    }
    else if([self isKindOfClass:[NSString class]])
    {
        NSString *title=(NSString *)self;
        if ([title isEqualToString:@"null"]||[title isEqualToString:@"<null>"]||[title isEqualToString:@"(null)"]||[title isEqualToString:@"nil"]||[title isEqualToString:@"(nil)"]||[title isEqualToString:@"<nil>"])
        {
            return @"0";
        }
        else
        {
            return [NSString stringWithFormat:@"%@", title];
        }
        
    }
    else if ([self isKindOfClass:[NSNumber class]])
    {
        NSNumber *valu = (NSNumber *)self;
        if (strcmp([valu objCType], @encode(float)) == 0)
        {
            return [NSString stringWithFormat:@"%.2f",[valu floatValue]];
        }
        else if (strcmp([valu objCType], @encode(double)) == 0)
        {
            return [NSString stringWithFormat:@"%.2f",[valu doubleValue]];
        }
        else if (strcmp([valu objCType], @encode(int)) == 0)
        {
            return [NSString stringWithFormat:@"%d",[valu intValue]];
        }
        else if (strcmp([valu objCType], @encode(BOOL)) == 0)
        {
            return [NSString stringWithFormat:@"%d",[valu boolValue]];
        }else if (strcmp([valu objCType], @encode(long)) == 0)
        {
            return [NSString stringWithFormat:@"%ld",[valu longValue]];
        }
        else
        {
            return [NSString stringWithFormat:@"%@",valu];
        }
    }
    else
    {
        return @"0";
    }
    return self;
    
}
//时间字符串转换指定格式时间字符串
+ (NSString *)chageDateString:(NSString *)dateString fromeFormatter:(NSString *)formatter toFormatter:(NSString *)toFormatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatter];
    NSDate *date=[dateFormatter dateFromString:dateString];
    
    NSDateFormatter *dateToFormatter = [[NSDateFormatter alloc] init];
    [dateToFormatter setDateFormat:toFormatter];
    NSString *string=[dateToFormatter stringFromDate:date];
    
    return string;
}
//全时间字符串转换指定格式时间字符串
+ (NSString *)chageDateString:(NSString *)dateString formatter:(NSString *)toFormatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date=[dateFormatter dateFromString:dateString];
    
    NSDateFormatter *dateToFormatter = [[NSDateFormatter alloc] init];
    [dateToFormatter setDateFormat:toFormatter];
    NSString *string=[dateToFormatter stringFromDate:date];
    
    return string;
}

/**
 获取字符串首字母(传入汉字字符串, 返回大写拼音首字母)
 
 @return 转换之后的大写首字母
 */
- (NSString *)getFirstLetterFromString
{
    //转成了可变字符串
    NSMutableString *str = [NSMutableString stringWithString:self];
    //先转换为带声调的拼音
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformMandarinLatin,NO);
    //再转换为不带声调的拼音
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformStripDiacritics,NO);
    //处理多音字
    if ([[(NSString *)self substringToIndex:1] compare:@"长"] == NSOrderedSame)
    {
        [str replaceCharactersInRange:NSMakeRange(0, 5) withString:@"chang"];
    }
    if ([[(NSString *)self substringToIndex:1] compare:@"沈"] == NSOrderedSame)
    {
        [str replaceCharactersInRange:NSMakeRange(0, 4) withString:@"shen"];
    }
    if ([[(NSString *)self substringToIndex:1] compare:@"厦"] == NSOrderedSame)
    {
        [str replaceCharactersInRange:NSMakeRange(0, 3) withString:@"xia"];
    }
    if ([[(NSString *)self substringToIndex:1] compare:@"地"] == NSOrderedSame)
    {
        [str replaceCharactersInRange:NSMakeRange(0, 3) withString:@"di"];
    }
    if ([[(NSString *)self substringToIndex:1] compare:@"重"] == NSOrderedSame)
    {
        [str replaceCharactersInRange:NSMakeRange(0, 5) withString:@"chong"];
    }
    //转化为大写拼音
    NSString *strPinYin = [str capitalizedString];
    //获取并返回首字母
    return [strPinYin substringToIndex:1];
}

+(NSString *)dateWithDateFormatter:(NSString *)dateFormatter Date:(NSDate *)date
{
    NSDateFormatter *dateFor = [[NSDateFormatter alloc]init];
    dateFor.dateFormat = dateFormatter;
    NSString * dateStr = [dateFor stringFromDate:date];
    return dateStr;
    
}
#pragma mark - 判断整数
- (BOOL)isPureInt:(NSString*)string
{
    NSScanner* scan = [NSScanner scannerWithString:string];
    
    int val;
    
    return[scan scanInt:&val] && [scan isAtEnd];
    
}
#pragma mark- 去除小数点后末尾的零
+(NSString*)removeFloatAllZeroByString:(NSString *)testNumber
{
    NSString * outNumber = [NSString stringWithFormat:@"%@",@(testNumber.floatValue)];
    return outNumber;
}
#pragma mark- 电话号码加星显示
+(NSString *)telephoneEncrypt:(NSString *)phone
{
    if (phone && phone.length > 7)
    {
        phone = [phone stringByReplacingCharactersInRange:NSMakeRange(phone.length-8, 4) withString:@"****"];//防止号码有前缀所以使用倒数第8位开始替换
        return phone;
    }
    return phone;
}

#pragma mark - 价格转换
+(NSString *)chageNumber:(NSString *)string
{
    NSString *str=[NSString stringWithFormat:@"%@",string];
    CGFloat number =[str floatValue];
    if ((int)number<10000) {
        return [NSString stringWithFormat:@"%.0f",number];
    }else
    {
        return [NSString stringWithFormat:@"%.1fw",number/10000];
    }
    
}
#pragma mark - 大数c相加和相乘
+ (NSString*)additionOfString:(NSString*)strOne AndString:(NSString*)strTwo{
    NSMutableString *One = [NSMutableString stringWithFormat:@"%@",strOne ];
    NSMutableString *Two = [NSMutableString stringWithFormat:@"%@",strTwo ];
    NSInteger longerLength =0;
    NSInteger t= 0;
    int jin =0;
    NSMutableString *strJ = [NSMutableString new];
    NSMutableString *sum = [NSMutableString new];
    
    // 补位：位数少的用0补齐，使两个字符串位数相等
    if(One.length> Two.length) {
        t = One.length- Two.length;
        for(NSInteger i =0;i < t;i++) {
            [Two insertString:[NSString stringWithFormat:@"0"] atIndex:0];
            
        }
        
    }else if(One.length< Two.length) {
        NSInteger t = Two.length- One.length;
        for(NSInteger i =0;i < t;i++) {
            [One insertString:[NSString stringWithFormat:@"0"] atIndex:0];
        }
        
    } else if (One.length== Two.length){
    } else {
        return @"您的输入有误！";
    }
    longerLength = One.length;for(NSInteger i = longerLength -1; i >=0;i--) {
        unichar onenum = [One characterAtIndex:i];
        unichar twonum = [Two characterAtIndex:i];
        int onum = [[NSString stringWithFormat:@"%c",onenum] intValue];
        int tnum = [[NSString stringWithFormat:@"%c",twonum] intValue];
        int c = onum + tnum +jin;
        int z = c%10;
        jin = c/10;
        if(i !=0) {[strJ appendFormat:@"%d",z];
        }else{// 注意是不是最后一位，别把进位数丢了
            [strJ appendFormat:@"%d",c%10];
            if(c/10!=0) {[strJ appendFormat:@"%d",c/10];
            }}
    }
    // 上面得到的是一个倒序字符串，需要变成正序
    for(NSInteger i = strJ.length-1; i>=0;i--) {
        unichar k = [strJ characterAtIndex:i];
        [sum appendFormat:@"%c",k];
        
    }
    return sum;
    
}
+ (NSString*)mutiplyOfString:(NSString*)strOne AndString:(NSString*)strTwo{
    NSMutableString *One = [NSMutableString stringWithFormat:@"%@",strOne ];
    NSMutableString *Two = [NSMutableString stringWithFormat:@"%@",strTwo ];
    int jin =0;
    NSMutableString *strJ = [NSMutableString new];
    NSMutableString *strT = [NSMutableString new];// strJ的正序
    NSString *sum = [NSString new];
    NSMutableArray * strJArr = [NSMutableArray array];
    for(NSInteger i = One.length-1; i >=0; i--) {
        strJ = [NSMutableString new];
        strT = [NSMutableString new];
        jin =0;
        unichar onenum= [One characterAtIndex:i];
        for(NSInteger j = Two.length-1; j >=0; j--){
            unichar
            twonum = [Two characterAtIndex:j];
            int onum = [[NSString stringWithFormat:@"%c",onenum]intValue];
            int tnum = [[NSString stringWithFormat:@"%c",twonum]intValue];
            int c = onum * tnum +jin;
            int z = c%10;
            jin = c/10;
            if(j !=0) {
                [strJ appendFormat:@"%d",z];
                
            }else{
                //是否最后一位
                [strJ appendFormat:@"%d",c%10];
                if(c/10!=0) {
                    [strJ appendFormat:@"%d",c/10];
                    
                }
                
            }
            
        }
        // 正序操作
        for(NSInteger a = strJ.length-1; a>=0;a--) {
            unichar c = [strJ characterAtIndex:a];
            [strT appendFormat:@"%c",c];
            
        }
        // 将strT放入数组，稍后加0后进行大数相加;
        [strJArr addObject:strT];
        
    }
    if(strJArr.count==0){
        return @"您的输入有误！";
        
    }
    for(NSInteger k =0; k < strJArr.count; k++){
        NSMutableString*strP = strJArr[k];
        // 高位数补0
        for(NSInteger i = k;i >0;i--) {
            [strP insertString:[NSString stringWithFormat:@"0"] atIndex:strP.length];
            
        }
        // 大数相加
        sum = [NSString additionOfString:sum AndString:strP];
        
    }
    NSLog(@"=========大数相乘%@",sum);
    return sum;
    
}

/// 字符串转属性文字
/// @param isYuan 是否显示符号
/// @param bigFont 大字体
/// @param smallFont 小字体//
///
- (NSMutableAttributedString *) attributedWithYuan:(BOOL)isYuan bigFont:(int) bigFont  smallFont:(int)smallFont{
    
    NSString *string = [NSString stringWithFormat:@"%.2f",self.floatValue];
    if (isYuan) {
        string = [NSString stringWithFormat:@"¥%@", string];
    }
    NSMutableAttributedString *attriStr = [[NSMutableAttributedString alloc] initWithString:string];
    [attriStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:bigFont]} range:NSMakeRange(0, string.length)];
    [attriStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:smallFont]} range:NSMakeRange(string.length-2, 2)];
    return attriStr;

}

/// 字符串转属性文字
/// @param isAdd 是否显示符号
/// @param bigFont 大字体
/// @param smallFont 小字体//
///
- (NSMutableAttributedString *) attributedWithAdd:(BOOL)isAdd bigFont:(int) bigFont  smallFont:(int)smallFont{
    NSString *string = [NSString stringWithFormat:@"%.2f",self.floatValue];
    if (isAdd) {
        string = [NSString stringWithFormat:@"+%@", string];
    }else{
        
        string = [NSString stringWithFormat:@"-%@", string];
    }

    NSMutableAttributedString *attriStr = [[NSMutableAttributedString alloc] initWithString:string];
    [attriStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:bigFont]} range:NSMakeRange(0, string.length)];
    [attriStr addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:smallFont]} range:NSMakeRange(string.length-2, 2)];
    return attriStr;

}

@end
