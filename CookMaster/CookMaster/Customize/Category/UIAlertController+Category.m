//
//  UIAlertController+Category.m
//  daycareParent+
//
//  Created by 钧泰科技 on 2018/2/5.
//  Copyright © 2018年 XueHui. All rights reserved.
//

#import "UIAlertController+Category.h"
@implementation UIAlertController (Category)
#pragma mark - 默认颜色弹出框
+(UIAlertController *)actionSheetWithTitle:(NSString *)title message:(NSString *)message titlesArry:(NSArray *)titleArry indexBlock:(indexBlock)indexBlock
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i=0; i<titleArry.count; i++)
    {
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:titleArry[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            indexBlock(i,titleArry[i]);
        }];
        [alertController addAction:okAction];
//        [okAction setValue:RGB(50, 50, 50) forKey:@"titleTextColor"];
    }
    UIAlertAction *cancleAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
//    [cancleAction setValue:RGB(157, 204, 122) forKey:@"titleTextColor"];
    [alertController addAction:cancleAction];
    
    
    [[self currentVC] presentViewController:alertController animated:YES completion:nil];
    
    return alertController;
}
#pragma mark - 标准弹出框
+(UIAlertController *)actionSheetNormalWithTitle:(NSString *)title message:(NSString *)message titlesArry:(NSArray *)titleArry indexBlock:(indexBlock)indexBlock okColor:(UIColor *)okColor cancleColor:(UIColor *)cancleColor isHaveCancel:(BOOL)isHaveCancel
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i=0; i<titleArry.count; i++)
    {
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:titleArry[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            indexBlock(i,titleArry[i]);
        }];
        if (okColor) {
            [okAction setValue:okColor forKey:@"titleTextColor"];
        }
        [alertController addAction:okAction];
        
    }
    if (isHaveCancel)
    {
        UIAlertAction *cancleAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        if (cancleColor) {
            [cancleAction setValue:cancleColor forKey:@"titleTextColor"];
        }
        [alertController addAction:cancleAction];
    }
   
    [[self currentVC] presentViewController:alertController animated:YES completion:nil];
    
    return alertController;
}
#pragma mark - 默认颜色弹出框
+(UIAlertController *)alertViewWithTitle:(NSString *)title message:(NSString *)message block:(Block)block
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    if (title) {
        //修改标题字体、颜色
        NSMutableAttributedString*alertTitleStr = [[NSMutableAttributedString alloc]initWithString:title];
        [alertTitleStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0,title.length)];
        [alertTitleStr addAttribute:NSForegroundColorAttributeName value:RGB(50, 50, 50)range:NSMakeRange(0,title.length)];
        [alertController setValue:alertTitleStr forKey:@"attributedTitle"];
    }
    
    if (message) {
        //修改内容字体、颜色
          NSMutableAttributedString*alertMessageStr = [[NSMutableAttributedString alloc]initWithString:message];
          [alertMessageStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14]range:NSMakeRange(0,message.length)];
          [alertMessageStr addAttribute:NSForegroundColorAttributeName value:RGB(50,50,50)range:NSMakeRange(0,message.length)];
          [alertController setValue:alertMessageStr forKey:@"attributedMessage"];
    }
  

    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        block();
    }];
    [okAction setValue:MainColor forKey:@"titleTextColor"];
    [alertController addAction:okAction];
    
    UIAlertAction *cancleAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
    [cancleAction setValue:RGB(50, 50, 50) forKey:@"titleTextColor"];
    [alertController addAction:cancleAction];
    
    [[self currentVC] presentViewController:alertController animated:YES completion:nil];
    return alertController;
}
#pragma mark - 标准弹出框
+(UIAlertController *)alertViewNormalWithTitle:(NSString *)title message:(NSString *)message titlesArry:(NSArray *)titleArry indexBlock:(indexBlock)indexBlock okColor:(UIColor *)okColor cancleColor:(UIColor *)cancleColor isHaveCancel:(BOOL)isHaveCancel
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    for (int i=0; i<titleArry.count; i++)
    {
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:titleArry[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            indexBlock(i,titleArry[i]);
        }];
        if (okColor) {
            [okAction setValue:okColor forKey:@"titleTextColor"];
        }
        [alertController addAction:okAction];
        
    }
    
    if (isHaveCancel)
    {
        UIAlertAction *cancleAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        if (cancleColor) {
           [cancleAction setValue:cancleColor forKey:@"titleTextColor"];
        }
        [alertController addAction:cancleAction];
    }
    
    [[self currentVC] presentViewController:alertController animated:YES completion:nil];
    return alertController;
}

#pragma mark - 默认弹出框（带输入框）
+(UIAlertController *)addtextFeildWithTitle:(NSString *)title message:(NSString *)message indexBlock:(indexBlock)indexBlock
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
    }];
    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (alertController.textFields.firstObject.text.length==0)
        {
            [LJTools showNOHud:@"输入内容不能为空" delay:1];
        }
        else
        {
            indexBlock(0,alertController.textFields.firstObject.text);
        }
    }];
    [okAction setValue:MainColor forKey:@"titleTextColor"];
    [alertController addAction:okAction];
    
    UIAlertAction *cancleAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//        indexBlock(1,alertController.textFields.firstObject.text);
    }];
    [cancleAction setValue:RGB(50, 50, 50) forKey:@"titleTextColor"];
    [alertController addAction:cancleAction];
    
    [[self currentVC] presentViewController:alertController animated:YES completion:nil];
    
    return alertController;
}
#pragma mark - 默认弹出框（带输入框）
+(UIAlertController *)addtextFeildCustomWithTitle:(NSString *)title message:(NSString *)message indexBlock:(indexBlock)indexBlock
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
    }];
    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (alertController.textFields.firstObject.text.length==0)
        {
            [LJTools showNOHud:@"输入内容不能为空" delay:1];
            indexBlock(0,alertController.textFields.firstObject.text);
            [alertController.textFields.firstObject resignFirstResponder];
        }
        else
        {
            indexBlock(1,alertController.textFields.firstObject.text);
            [alertController.textFields.firstObject resignFirstResponder];
        }
    }];
    [okAction setValue:MainColor forKey:@"titleTextColor"];
    [alertController addAction:okAction];
    [[self currentVC] presentViewController:alertController animated:YES completion:nil];
    return alertController;
}
#pragma mark 获得当前的controller
+ (UIViewController *)currentVC
{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *controller = [self getCurrentVCFrom:rootViewController];
    return controller;
}
+ (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC
{
    UIViewController *currentVC;
    
    if ([rootVC presentedViewController])
    {
        // 视图是被presented出来的
        currentVC = [rootVC presentedViewController];
    }
    
    if ([rootVC isKindOfClass:[UITabBarController class]])
    {
        // 根视图为UITabBarController
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
        
    }
    else if ([rootVC isKindOfClass:[UINavigationController class]])
    {
        // 根视图为UINavigationController
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
        
    }
    else
    {
        // 根视图为非导航类
        currentVC = rootVC;
    }
    
    return currentVC;
}

@end
