//
//  NSString+Category.h
//  daycareParent
//
//  Created by Git on 2017/11/27.
//  Copyright © 2017年 XueHui. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <CommonCrypto/CommonCrypto.h>
#include <CommonCrypto/CommonDigest.h>
#include <zlib.h>

@interface NSString (Category)

/**
 @param title 传入的对象
 @return NSString 返回“ @"" ”的字符串
 */
+(NSString*)safeString:(NSString*)title;
+(NSString*)safeStringZero:(NSString*)title;

#pragma mark 字符串安全保护，防止字符串为 nil <null> 导致程序崩溃问题
-(NSString *)safeString;
-(NSString *)safeStringZero;
//时间字符串转换指定格式时间字符串
+ (NSString *)chageDateString:(NSString *)dateString fromeFormatter:(NSString *)formatter toFormatter:(NSString *)toFormatter;
//全时间字符串转换指定格式时间字符串
+ (NSString *)chageDateString:(NSString *)dateString formatter:(NSString *)toFormatter;
/**
 获取字符串首字母(传入汉字字符串, 返回大写拼音首字母)

 @return 转换之后的大写首字母
 */
- (NSString *)getFirstLetterFromString;

+(NSString *)dateWithDateFormatter:(NSString *)dateFormatter Date:(NSDate *)date;
#pragma mark- 去除小数点后末尾的零
+(NSString*)removeFloatAllZeroByString:(NSString *)testNumber;
#pragma mark- 电话号码加星显示
+(NSString *)telephoneEncrypt:(NSString *)phone;
+(NSString *)chageNumber:(NSString *)string;
- (BOOL)isPureInt:(NSString*)string;
//大数相加或者乘
+ (NSString*)additionOfString:(NSString*)strOne AndString:(NSString*)strTwo;
+ (NSString*)mutiplyOfString:(NSString*)strOne AndString:(NSString*)strTwo;

/// 字符串转属性文字
/// @param isYuan 是否显示符号
/// @param bigFont 大字体
/// @param smallFont 小字体//
///
- (NSMutableAttributedString *) attributedWithYuan:(BOOL)isYuan bigFont:(int) bigFont  smallFont:(int)smallFont;

/// 字符串转属性文字
/// @param isAdd 是否显示符号
/// @param bigFont 大字体
/// @param smallFont 小字体//
///
- (NSMutableAttributedString *) attributedWithAdd:(BOOL)isAdd bigFont:(int) bigFont  smallFont:(int)smallFont;

@end
