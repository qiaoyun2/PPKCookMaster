//
//  NSString+Addition.m
//  PinChaoPhone
//
//  Created by 克奎  岳 on 15/9/1.
//  Copyright (c) 2015年 null. All rights reserved.
//

#import "NSString+Addition.h"

@implementation NSString (Addition)

- (CGFloat)commonStringWidthForFont:(CGFloat)fontSize
{
    CGFloat width = [self boundingRectWithSize:CGSizeMake(MAXFLOAT, 0) options:NSStringDrawingUsesLineFragmentOrigin  attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]} context:nil].size.width;
    return width;
}

- (CGFloat)commonStringHeighforLabelWidth:(CGFloat)width withFontSize:(CGFloat)fontSize
{
    CGFloat heigh = [self boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]} context:nil].size.height;
    return heigh;
}

- (CGPoint)commonStringLastPointWithLabelFrame:(CGRect)frame withFontSize:(CGFloat)fontSize;
{
    CGPoint lastPoint;
    CGSize sz = [self boundingRectWithSize:CGSizeMake(MAXFLOAT, 0) options:NSStringDrawingUsesLineFragmentOrigin  attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]} context:nil].size;
    CGSize lineSize = [self boundingRectWithSize:CGSizeMake(frame.size.width, MAXFLOAT) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]} context:nil].size;
    if(sz.width <= lineSize.width) //判断是否折行
    {
        lastPoint = CGPointMake(frame.origin.x + sz.width, frame.origin.y);
    }
    else
    {
        lastPoint = CGPointMake(frame.origin.x + (int)sz.width % (int)lineSize.width,lineSize.height + sz.height);
    }
    return lastPoint;
}

//判断字符串，正责表达式
- (BOOL)isUserName
{
    NSString *      regex = @"(^[A-Za-z0-9]{3,20}$)";
    NSPredicate *   pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [pred evaluateWithObject:self];
}

- (BOOL)isPassword
{
    NSString * regex = @"^(?![0-9]+$)(?![a-zA-Z]+$)[0-9a-zA-Z]{6,12}";
    NSPredicate * pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [pred evaluateWithObject:self];
}

- (BOOL)isEmail
{
    NSString *      regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *   pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [pred evaluateWithObject:self];
}

- (BOOL)isUrl
{
    NSString *      regex = @"http(s)?:\\/\\/([\\w-]+\\.)+[\\w-]+(\\/[\\w- .\\/?%&=]*)?";
    NSPredicate *   pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [pred evaluateWithObject:self];
}

- (BOOL)isTelephone
{
    NSString *phoneRegex1=@"1[3456789]([0-9]){9}";
    NSPredicate *phoneTest1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex1];
    return [phoneTest1 evaluateWithObject:self];
}
-(BOOL) isidentityCard
{
    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    return [identityCardPredicate evaluateWithObject:self];
}
- (BOOL)validateQQ {
    NSString *qqRegex = @"^[0-9]{5,12}$";
    NSPredicate *qqTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",qqRegex];
    return [qqTest evaluateWithObject:self];
}
/**
 *  判断字符串是否为空
 *
 *  @return BOOL
 */
- (BOOL)isBlankString{
    if (self == nil || self == NULL || [self isEqualToString:@""] || [self isEqualToString:@"(null)"]) {
        return YES;
    }
    if ([self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}

// 在文本中间添加横划线
- (NSMutableAttributedString *)addTextCenterLine{
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:self];
    
    [attrString addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, self.length)];
    [attrString addAttribute:NSStrikethroughColorAttributeName value:RGB(168, 168, 170) range:NSMakeRange(0, self.length)];
    
    return attrString;
}

 //纯数字
- (BOOL)isNumber
{
    NSString *      regex = @"^[0-9]*$";
    NSPredicate *   pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [pred evaluateWithObject:self];
}

/// 正整数
- (BOOL)validateNumber {
    NSString *numberRegex = @"^\\+?[1-9][0-9]*$";
    NSPredicate *numberPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegex];
    return [numberPredicate evaluateWithObject:self];
}

/// 金额验证
- (BOOL)validateMoney {
    NSString *moneyRegex = @"^[0-9]+(.[0-9]{1,2})?$";
    NSPredicate *moneyPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", moneyRegex];
    return [moneyPredicate evaluateWithObject:self];
}

@end
