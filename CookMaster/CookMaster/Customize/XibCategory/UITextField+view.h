//
//  UITextField+view.h
//  MusicHall
//
//  Created by null on 2020/5/30.
//  Copyright © 2020 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (view)
//textfield 左右
@property(nonatomic, strong) IBInspectable UIImage *rightImage;
@property(nonatomic, strong) IBInspectable UIImage *leftImage;

//多语言设置
@property(nonatomic, strong) IBInspectable NSString * localizedPlaceText;
//place
@property(nonatomic, strong) IBInspectable UIColor * placeTextColor;

@end

NS_ASSUME_NONNULL_END
