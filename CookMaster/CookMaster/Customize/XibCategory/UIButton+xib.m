//
//  UIButton+xib.m
//  ZZR
//
//  Created by null on 2020/8/4.
//  Copyright © 2020 null. All rights reserved.
//

#import "UIButton+xib.h"
#import "LocalizedHelper.h"

@implementation UIButton (xib)

- (void)setDefaultBackColor:(UIColor *)defaultBackColor{
    [self setBackgroundImage:[UIImage imageWithColor:defaultBackColor] forState:(UIControlStateNormal)];
}

- (void)setSelectBackColor:(UIColor *)selectBackColor{
    [self setBackgroundImage:[UIImage imageWithColor:selectBackColor] forState:(UIControlStateSelected)];
}

- (void)setTitleNum:(NSInteger)titleNum{
    [self.titleLabel setNumberOfLines:titleNum];
}

- (void)setLocalizedText:(NSString *)localizedText{
    
    if (localizedText.length != 0) {
        [self setTitle:LocalizedString(localizedText) forState:(UIControlStateNormal)];
    }
}

- (void)setLocalizedSelectText:(NSString *)localizedSelectText{
    if (localizedSelectText.length != 0) {
        [self setTitle:LocalizedString(localizedSelectText) forState:(UIControlStateSelected)];
    }
}



@end
