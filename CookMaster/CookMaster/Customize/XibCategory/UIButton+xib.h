//
//  UIButton+xib.h
//  ZZR
//
//  Created by null on 2020/8/4.
//  Copyright © 2020 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (xib)

@property(nonatomic, strong) IBInspectable UIColor *defaultBackColor;
@property(nonatomic, strong) IBInspectable UIColor *selectBackColor;
@property(nonatomic, assign) IBInspectable NSInteger titleNum;

//多语言设置
@property(nonatomic, strong) IBInspectable NSString * localizedText;

//多语言设置
@property(nonatomic, strong) IBInspectable NSString * localizedSelectText;

@end

NS_ASSUME_NONNULL_END
