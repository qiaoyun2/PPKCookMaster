//
//  UITextField+view.m
//  MusicHall
//
//  Created by null on 2020/5/30.
//  Copyright © 2020 null. All rights reserved.
//

#import "UITextField+view.h"
#import "LocalizedHelper.h"

@implementation UITextField (view)
@dynamic rightImage;
@dynamic leftImage;

- (void)setRightImage:(UIImage *)rightImage{
    
    if (rightImage) {
        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height, self.frame.size.height)];
        UIImageView *view = [[UIImageView alloc] initWithImage:rightImage];
        [view sizeToFit];
        [view setCenter:CGPointMake(self.frame.size.height/2.0, self.frame.size.height/2.0)];
        view.mj_x = backView.mj_w - view.mj_w;
        [backView addSubview:view];
        [self setRightView:backView];
        [self setRightViewMode:(UITextFieldViewModeAlways)];
    }
}

- (void)setLeftImage:(UIImage *)leftImage{
    if (leftImage) {

        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height, self.frame.size.height)];
        UIImageView *view = [[UIImageView alloc] initWithImage:leftImage];
        [view setCenter:CGPointMake(self.frame.size.height/2.0, self.frame.size.height/2.0)];
        [backView addSubview:view];
        [self setLeftView:backView];
        [self setLeftViewMode:(UITextFieldViewModeAlways)];
    }
}

- (void)setLocalizedPlaceText:(NSString *)localizedPlaceText{
    if (localizedPlaceText.length != 0) {
        [self setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:LocalizedString(localizedPlaceText)]];
    }
}

- (void)setPlaceTextColor:(UIColor *)placeTextColor{
    if (placeTextColor) {
        [self setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName:placeTextColor}]];
    }
}

@end
