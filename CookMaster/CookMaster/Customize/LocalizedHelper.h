//
//  LocalizedHelper.h
//  TXLiteAVDemo_Smart
//
//  Created by null on 2019/5/13.
//  Copyright © 2019 Tencent. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
#define LocalizedString(key) [[LocalizedHelper standardHelper] stringWithKey:key]
@interface LocalizedHelper : NSObject
+ (instancetype)standardHelper;

- (NSBundle *)bundle;

- (NSString *)currentLanguage;

- (void)setUserLanguage:(NSString *)language;

- (NSString *)stringWithKey:(NSString *)key;
@end

NS_ASSUME_NONNULL_END
