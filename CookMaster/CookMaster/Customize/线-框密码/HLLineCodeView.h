//
//  HLLineCodeView.h
//  VCloud
//
//  Created by null on 2019/12/17.
//  Copyright © 2019 锤子科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

//MARK: ------ 💓💓 HLLineCodeView 💓💓 ------ */
@interface HLLineCodeView : UIView

@property (nonatomic, weak) UITextField *textField;

@property (copy, nonatomic) void(^hl_lineCode)(NSString *lineCode);
@property (copy, nonatomic) void(^hl_lineRun)(void);


- (instancetype)initWithCount:(NSInteger)count margin:(CGFloat)margin;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new UNAVAILABLE_ATTRIBUTE;

@end

//MARK: ------ 💓💓 HL_lineView 💓💓 ------ */
@interface HL_lineView : UIView

@property (nonatomic, weak) UIView *colorView;

- (void)animation;

@end

NS_ASSUME_NONNULL_END
