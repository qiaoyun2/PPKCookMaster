//
//  ACMColorAppearanceHeader.h
//  HappyLoan
//
//  Created by tanxiaokang on 2018/5/1420.
//  Copyright © 2018年 runze. All rights reserved.
//

#ifndef ACMColorAppearanceHeader_h
#define ACMColorAppearanceHeader_h
#import "UIView+AZGradient.h"


// 颜色

///*******颜色规范*******/
//#define RGBA_COLOR(R, G, B, A) [UIColor colorWithRed:((R) / 255.0f) green:((G) / 255.0f) blue:((B) / 255.0f) alpha:A]
//
//#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define RGBA_HexCOLOR(rgbValue, A) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:A]
//
//#define RandomColor RGBA_COLOR(arc4random_uniform(256),arc4random_uniform(256),arc4random_uniform(256),1)
//
///* 文字颜色 */
//#define COLOR_FOR_B28A5A RGBA_HexCOLOR(0xB28A5A, 1)
//#define COLOR_FOR_000000 RGBA_HexCOLOR(0x000000, 1)
//#define COLOR_FOR_111111 RGBA_HexCOLOR(0x111111, 1)
//#define COLOR_FOR_333333 RGBA_HexCOLOR(0x333333, 1)
//#define COLOR_FOR_666666 RGBA_HexCOLOR(0x666666, 1)
//#define COLOR_FOR_969696 RGBA_HexCOLOR(0x969696, 1)
//#define COLOR_FOR_999999 RGBA_HexCOLOR(0x999999, 1)
//#define COLOR_FOR_282828 RGBA_HexCOLOR(0x282828, 1)
//#define COLOR_FOR_C3C3C3 RGBA_HexCOLOR(0xC3C3C3, 1)
//#define COLOR_FOR_0085FF RGBA_HexCOLOR(0x0085FF, 1)
//#define COLOR_FOR_EED297 RGBA_HexCOLOR(0xEED297, 1)
//#define COLOR_FOR_FF0000 RGBA_HexCOLOR(0xFF0000, 1)
#define COLOR_FOR_FF6F61 RGBA_HexCOLOR(0xFF6F61, 1)     //按钮渐变色A
#define COLOR_FOR_E04839 RGBA_HexCOLOR(0xE04839, 1)     //按钮渐变色A
//#define COLOR_FOR_CUTLINE RGBA_HexCOLOR(0xE5E5E5, 1)    //分割线颜色
//#define COLOR_FOR_BUTTON RGBA_HexCOLOR(0x0085FF, 1)     //按钮颜色
//#define Color_Main RGBA_COLOR(0xFF, 0x6F, 0x61, 1)      //主色
//#define Color_Background RGBA_HexCOLOR(0xF8F8F8, 1)//背景色
//
#define CDKCardLabelsGradientColorToE5CA9B @[RGBA_HexCOLOR(0xE5CA9B, 1),RGBA_HexCOLOR(0xCAA67B, 1)]
#define CDKCardLabelsGradientColorToC9C9C9 @[RGBA_HexCOLOR(0xC9C9C9, 1),RGBA_HexCOLOR(0x909090, 1)]
#define CDKCardLabelsGradientColorToD1B4A1 @[RGBA_HexCOLOR(0xD1B4A1, 1),RGBA_HexCOLOR(0xAB8172, 1)]
#define CDKCardLabelsGradientColorToA4ACB6 @[RGBA_HexCOLOR(0xA4ACB6, 1),RGBA_HexCOLOR(0x80879A, 1)]

/**
 项目主题色按钮背景渐变色
 
 CDKBtnBackgroundGradient(self.submitBtn);
 
 @param view 需要传入的View
 */
static inline void CDKBtnBackgroundGradient(UIView *view) {
    [view mdf_setGradientBackgroundWithColors:@[RGBA_HexCOLOR(0x2C85FF, 1),RGBA_HexCOLOR(0x004FCC, 1)] locations:nil startPoint:CGPointMake(0, 0) endPoint:CGPointMake(1, 0)];
}
/**
 渐变色

 @param view 需要渐变的view
 @param array 需要渐变的数组
 */
static inline void CDKOtherBtnBackgroundGradient(UIView *view, NSArray *array) {
    [view mdf_setGradientBackgroundWithColors:array locations:nil startPoint:CGPointMake(0, 0) endPoint:CGPointMake(1, 0)];
}

/**
 渐变色
 
 @param view 需要渐变的view
 @param colors 需要渐变的数组
 */
static inline void CDKViewGradient(UIView *view,NSArray <UIColor *>*colors)
{
    if (colors.count != 2) {
        return;
    }
    if ([colors.firstObject isKindOfClass:[UIColor class]] &&
        [colors.lastObject isKindOfClass:[UIColor class]]) {
        [view mdf_setGradientBackgroundWithColors:@[colors.firstObject,colors.lastObject] locations:nil startPoint:CGPointMake(0, 0) endPoint:CGPointMake(1, 0)];
    }
}
/**
 渐变色 角度渐变
 
 @param view 需要渐变的view
 @param colors 需要渐变的数组
 */
static inline void CDKViewGradientAngle(UIView *view,CGFloat x,NSArray <UIColor *>*colors)
{
    if (colors.count != 2) {
        return;
    }
    if ([colors.firstObject isKindOfClass:[UIColor class]] &&
        [colors.lastObject isKindOfClass:[UIColor class]]) {
        [view mdf_setGradientBackgroundWithColors:@[colors.firstObject,colors.lastObject] locations:nil startPoint:CGPointMake(x, 0) endPoint:CGPointMake(1, 1)];
    }
}

/**
 渐变色 上下
 
 @param view 需要渐变的view
 @param colors 需要渐变的数组
 */
static inline void CDKViewGradientTopToBottom(UIView *view,NSArray <UIColor *>*colors)
{
    if (colors.count != 2) {
        return;
    }
    if ([colors.firstObject isKindOfClass:[UIColor class]] &&
        [colors.lastObject isKindOfClass:[UIColor class]]) {
        [view mdf_setGradientBackgroundWithColors:@[colors.firstObject,colors.lastObject] locations:nil startPoint:CGPointMake(1, 0) endPoint:CGPointMake(1, 1)];
    }
}
static inline NSArray <NSArray <UIColor *>*> *CDKGradientColors(NSInteger count)
{
    NSArray <NSArray *>*colors  = @[CDKCardLabelsGradientColorToE5CA9B,CDKCardLabelsGradientColorToC9C9C9,CDKCardLabelsGradientColorToD1B4A1,CDKCardLabelsGradientColorToA4ACB6];
    
    NSMutableArray <NSArray <UIColor *>*>* array = [NSMutableArray array];
    for (int i=2; i<count; i++) {
//        [array addObject:[colors mdf_safeObjectAtIndex:i%4]];
        [array addObject:[colors objectAtIndex:i%4]];
    }
    [array insertObject:CDKCardLabelsGradientColorToE5CA9B atIndex:0];
    [array insertObject:CDKCardLabelsGradientColorToE5CA9B atIndex:0];
    return array;
}
#endif /* ACMColorAppearanceHeader_h */
