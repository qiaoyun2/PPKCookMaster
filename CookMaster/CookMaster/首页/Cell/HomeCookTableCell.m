//
//  HomeCookTableCell.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/12.
//

#import "HomeCookTableCell.h"
@interface HomeCookTableCell ()
///订单编号
@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
///订单编号
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
///商品图
@property (weak, nonatomic) IBOutlet UIImageView *goodsImageView;
///标题
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
///规格
@property (weak, nonatomic) IBOutlet UILabel *specLabel;
///时间
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
///价格
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
///姓名
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
///电话
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
///地址
@property (weak, nonatomic) IBOutlet UILabel *addresslabel;

@end

@implementation HomeCookTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
