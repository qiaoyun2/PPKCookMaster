//
//  HomeCookDetailTableCell.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/12.
//

#import "HomeCookDetailTableCell.h"

@interface HomeCookDetailTableCell ()
///订单编号：1234567891
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
///订单时间：2022.9.23 14:23:32
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
///¥ 50.00
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
///状态已完成
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end
@implementation HomeCookDetailTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
