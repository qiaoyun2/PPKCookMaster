//
//  HomeCookDetailController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/12.
//

#import "HomeCookDetailController.h"
#import "HomeCookDetailTableCell.h"
#import "SignInView.h"

@interface HomeCookDetailController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeViewHeight;
///筛选区间
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic, copy) NSString *start_data;
@property (nonatomic, copy) NSString *end_data;
@end

@implementation HomeCookDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"查看明细";
    [self setNavigationRightBarButtonWithImage:@"路径 19589"];
}
#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCookDetailTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCookDetailTableCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HomeCookDetailTableCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

#pragma mark - 删除
- (IBAction)deleteBtnClick:(id)sender {
    self.timeView.hidden = YES;
    self.timeViewHeight.constant = 0;
    self.start_data = self.end_data = nil;
    //    [self refresh];
}

#pragma mark – 选择时间
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender {
    MJWeakSelf;
    [SignInView title:@"选择时间" show:^(NSString * _Nonnull start, NSString * _Nonnull end) {
        weakSelf.start_data = start;
        weakSelf.end_data = end;
        weakSelf.timeLabel.text = [NSString stringWithFormat:@"筛选区域：%@ 至%@",start,end];
        //        [weakSelf refresh];
    }];
}

@end
