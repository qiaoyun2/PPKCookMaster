//
//  HomeViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/12.
//

#import "HomeViewController.h"
#import "HomeCookTableCell.h"
#import "ScrollNoticeCell.h"
#import <SDCycleScrollView.h>
#import "HomeCookDetailController.h"
#import "HomeCookVerifyViewController.h"


@interface HomeViewController ()<SDCycleScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navViewTop;
@property (weak, nonatomic) IBOutlet UIView *noticeBgView;
@property (strong, nonatomic) IBOutlet UIView *headerView;

@property (nonatomic, strong) SDCycleScrollView *noticeView;

@property (nonatomic, strong) NSArray *noticeArray;
@end

@implementation HomeViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
   
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
   
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = UIColorFromRGB(0xF6F7F9);
    self.navViewTop.constant = StatusHight;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 435);
    self.tableView.tableHeaderView = self.headerView;
    [self initNoticeView];
}
#pragma mark - UI
-(void)initNoticeView{
    _noticeView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH-56), 32) delegate:self placeholderImage:nil];
    _noticeView.scrollDirection = UICollectionViewScrollDirectionVertical;
    _noticeView.localizationImageNamesGroup = @[@"",@""];
    _noticeView.delegate = self;
    _noticeView.showPageControl = NO;
    _noticeView.autoScrollTimeInterval = 3;
    _noticeView.backgroundColor = [UIColor clearColor];
    [_noticeView disableScrollGesture];
    [self.noticeBgView addSubview:_noticeView];
}
#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCookTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCookTableCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HomeCookTableCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
}
/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if (self.noticeArray.count) {
//        LMJobHeaderNoticeModel *model = self.noticeArray[index];
//        WKWebViewController *vc = [[WKWebViewController alloc] init];
//        vc.contentStr = model.content;
//        vc.titleStr = model.title;
//        [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
    }
}

/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index
{
    
}

/** 如果你需要自定义cell样式，请在实现此代理方法返回你的自定义cell的Nib。 */
- (UINib *)customCollectionViewCellNibForCycleScrollView:(SDCycleScrollView *)view
{
    if (view != self.noticeView) {
        return nil;
    }
    return [UINib nibWithNibName:@"ScrollNoticeCell" bundle:nil];
}

/** 如果你自定义了cell样式，请在实现此代理方法为你的cell填充数据以及其它一系列设置 */
- (void)setupCustomCell:(UICollectionViewCell *)cell forIndex:(NSInteger)index cycleScrollView:(SDCycleScrollView *)view
{
    ScrollNoticeCell *myCell = (ScrollNoticeCell *)cell;
    myCell.backgroundColor = [UIColor whiteColor];
//    if (self.noticeArray.count) {
//        LMJobHeaderNoticeModel *model = self.noticeArray[index];
//        myCell.contentLabel.text = model.title;
//    }
}


#pragma mark - 验证
- (IBAction)verifyBtnClick:(id)sender {
    HomeCookVerifyViewController *vc = [HomeCookVerifyViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 详情
- (IBAction)detailBtnClick:(id)sender {
    HomeCookDetailController *vc = [HomeCookDetailController new];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
