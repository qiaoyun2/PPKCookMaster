//
//  TKTruckingOrderDetailVC.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "TKTruckingOrderDetailVC.h"
#import "FoundListImage9TypographyView.h"
#import "LMMapViewController.h"
#import "RWOrderDetailModel.h"
#import "CancelOrderView.h"
#import "GBStarRateView.h"
//#import "UpdateImageView.h"
#import "UploadManager.h"
#import "FinishOrderController.h"

@interface TKTruckingOrderDetailVC ()

#pragma mark -- 订单状态
@property (weak, nonatomic) IBOutlet UILabel *statusLabel; // 订单状态
@property (weak, nonatomic) IBOutlet UILabel *statusDetailLabel; // 详细描述
@property (weak, nonatomic) IBOutlet UIView *unConfirmTipView; // 待回收状态提示
@property (weak, nonatomic) IBOutlet UILabel *cancelTipLabel; // 取消状态提示
@property (weak, nonatomic) IBOutlet UIView *reserveTimeView; // 预约上门时间View
@property (weak, nonatomic) IBOutlet UILabel *reserveTimeLabel; // 预约上门时间
@property (weak, nonatomic) IBOutlet UIView *startTimeView; // 开始时间
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *daodaTimeBaseView;
@property (weak, nonatomic) IBOutlet UILabel *daodaTimeLabel;

#pragma mark -- 用户评价
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UILabel *commentTimeLabel; // 评论时间
@property (weak, nonatomic) IBOutlet UILabel *introduceLbel; // 描述
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;
@property (weak, nonatomic) IBOutlet UILabel *commentNameLabel;
@property (weak, nonatomic) IBOutlet GBStarRateView *starView;
#pragma mark -- 售后信息
@property (weak, nonatomic) IBOutlet UIView *cancelView;
@property (weak, nonatomic) IBOutlet UILabel *cancelReasonLabel; // 取消原因
@property (weak, nonatomic) IBOutlet UIView *cancleImgBaseView;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *cancleimg9TypographyView;
#pragma mark -- 订单详细信息
@property (weak, nonatomic) IBOutlet UILabel *typeLabel; // 用车类型
@property (weak, nonatomic) IBOutlet UILabel *xinghaoLabel; // 用车型号
@property (weak, nonatomic) IBOutlet UILabel *useTimeLabel; // 预约用车时间
@property (weak, nonatomic) IBOutlet UILabel *useAddressLabel; // 用车地点
@property (weak, nonatomic) IBOutlet UILabel *personNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel; // 备注
@property (weak, nonatomic) IBOutlet UIView *surePicBaseView;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *surePicImgsView;
#pragma mark -- 图片
@property (weak, nonatomic) IBOutlet UIView *imgBaseView;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *orderImgsView;
#pragma mark -- 客户信息
@property (weak, nonatomic) IBOutlet UILabel *nameLabel; // 姓名
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel; // 联系方
@property (weak, nonatomic) IBOutlet UILabel *addOrderTimeLabel; // 接单时间
@property (weak, nonatomic) IBOutlet UIView *letgoTimeView; // 出发view
@property (weak, nonatomic) IBOutlet UILabel *letgoTimeLabel; // 出发时间
@property (weak, nonatomic) IBOutlet UIView *arriveBaseView; // 到达时间
@property (weak, nonatomic) IBOutlet UILabel *arriveTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *cancelTimeView; // 取消时间
@property (weak, nonatomic) IBOutlet UILabel *cancelTimeLabel;
#pragma mark -- 按钮
@property (weak, nonatomic) IBOutlet UIButton *cancelButton; // 取消接单
@property (weak, nonatomic) IBOutlet UIButton *letgoBtn; // 开始出发
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;  // 删除订单
@property (weak, nonatomic) IBOutlet UIButton *linkBtn; // 联系客户
@property (weak, nonatomic) IBOutlet UIButton *picBtn;
@property (weak, nonatomic) IBOutlet UIStackView *buttonsStackView;
// model
@property (nonatomic, strong) RWOrderDetailModel *detailModel;
@property (nonatomic, strong) CancelOrderView *reasonView; // 取消原因

@end

@implementation TKTruckingOrderDetailVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self requestForOrderDetail];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单详情";
    [self initUI];
}

#pragma mark – UI
- (void)initUI{
    self.starView.allowClickScore = NO;
    self.starView.allowSlideScore = NO;
    self.starView.spacingBetweenStars = 2;
    self.starView.starSize = CGSizeMake(10, 10);
    self.starView.starImage = [UIImage imageNamed:@"star_off"];
    self.starView.currentStarImage = [UIImage imageNamed:@"star_on"];
    self.starView.style = GBStarRateViewStyleIncompleteStar;
    self.starView.isAnimation = NO;
}
- (void)updateSubviews{
    // 隐藏控件
    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    // 0-全部；3-已接单待出发 ；4-待确认（可上传图片）; 5-已完成(待评价-用户点完成，但是必须师傅先上传图片);6-已取消(订单已取消)；7已关闭（退款成功或失败，未支付关闭的订单不会展示在师傅端）
    NSInteger status = [self.detailModel.status integerValue];
    self.statusLabel.text = [LJTools getMTStatusFormat:status];
    /// ///refundStatus 0-未申请;1-售后中;2-售后失;3-售后成功
    if (self.detailModel.refundStatus.intValue > 0 ) {
        self.unConfirmTipView.hidden = YES;
        self.deleteButton.hidden = NO;
        self.linkBtn.hidden = NO;
        self.cancelView.hidden = NO;
        self.cancelReasonLabel.text = self.detailModel.refundReason;
        if (K_DEFULT(self.detailModel.refundPictures).length > 0) {
            self.cancleimg9TypographyView.hidden = NO;
            NSArray *images = [_detailModel.refundPictures componentsSeparatedByString:@","];
            self.cancleimg9TypographyView.imageArray= images;
        }else{
            self.cancleimg9TypographyView.hidden = YES;
        }
        switch (self.detailModel.refundStatus.intValue) {
            case 1:
                self.statusLabel.text = @"售后中";
                break;
            case 2:
                self.statusLabel.text = @"售后失败";
                self.unConfirmTipView.hidden = K_DEFULT(self.detailModel.refundFailRemark).length == 0;
                self.statusDetailLabel.text = [NSString stringWithFormat:@"失败原因：%@", self.detailModel.refundFailRemark];
                if (status == 5) {
                    self.picBtn.hidden = K_DEFULT(self.detailModel.masterPicture).length > 0;
                }
                break;
            case 3:
                self.statusLabel.text = @"售后成功";
                break;
            default:
                break;
        }
    }else{
        if (status == 3) {
            self.statusDetailLabel.text = @"和客户确认后再出发";
            self.reserveTimeView.hidden = NO;
            self.cancelButton.hidden = NO;
            self.letgoBtn.hidden = NO;
        }
        if (status == 4) {
            self.statusDetailLabel.text = @"等待客户，确认到达";
            self.linkBtn.hidden = NO;
            self.letgoTimeView.hidden = NO;
            self.picBtn.hidden = K_DEFULT(self.detailModel.masterPicture).length > 0;
        }
        if (status == 5) {
            self.unConfirmTipView.hidden = YES;
            self.startTimeView.hidden = NO;
            self.letgoTimeView.hidden = NO;
            self.deleteButton.hidden = NO;
            self.linkBtn.hidden = NO;
        }
        if (status == 6) {
            // 师傅端没有取消状态
    //        self.statusDetailLabel.text = @"订单取消已扣除费用";
    //        self.deleteButton.hidden = NO;
    //        self.cancelView.hidden = NO;
        }
        if (status == 7) {
            self.unConfirmTipView.hidden = YES;
            self.deleteButton.hidden = NO;
            self.cancelView.hidden = NO;
            self.cancelReasonLabel.text = self.detailModel.refundReason;
            if (K_DEFULT(self.detailModel.refundPictures).length > 0) {
                self.cancleimg9TypographyView.hidden = NO;
                NSArray *images = [_detailModel.refundPictures componentsSeparatedByString:@","];
                self.cancleimg9TypographyView.imageArray= images;
            }else{
                self.cancleimg9TypographyView.hidden = YES;
            }
        }
    }
    // 顶部信息
    self.reserveTimeLabel.text = self.detailModel.reserveTime;
    self.startTimeLabel.text = self.detailModel.departureTime; // 开始时间就是出发时间
    // 评价信息
    self.commentView.hidden = K_DEFULT(self.detailModel.commentVO.orderId).length == 0;
//    commentModel
    self.commentTimeLabel.text = self.detailModel.commentVO.createTime;
    self.introduceLbel.text = self.detailModel.commentVO.content;
    self.imgsView.hidden = K_DEFULT(self.detailModel.commentVO.picture).length == 0;
    self.commentNameLabel.text = self.detailModel.commentVO.isAnonymous.intValue == 1?@"匿名":self.detailModel.commentVO.nickname;
    if (K_DEFULT(self.detailModel.commentVO.picture).length > 0) {
        NSArray *images = [self.detailModel.commentVO.picture componentsSeparatedByString:@","];
        self.imgsView.imageArray= images;
    }
    self.starView.currentStarRate = [self.detailModel.commentVO.starNum floatValue];
    // 订单详细信息
    self.typeLabel.text = self.detailModel.firstClassify;
    self.xinghaoLabel.text = self.detailModel.secondClassify;
    self.useTimeLabel.text = self.detailModel.reserveTime;
    self.useAddressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",self.detailModel.province,self.detailModel.city,self.detailModel.area,self.detailModel.street];;
    self.personNumLabel.text = [NSString stringWithFormat:@"%ld人", self.detailModel.followNum.integerValue];
    self.weightLabel.text = K_DEFULT(self.detailModel.goodsWeight);
    self.remarkLabel.text = self.detailModel.remark;
    if (K_DEFULT(self.detailModel.masterPicture).length > 0) {
        self.surePicBaseView.hidden = NO;
        NSArray *images = [_detailModel.masterPicture componentsSeparatedByString:@","];
        self.surePicImgsView.imageArray= images;
    }else{
        self.surePicBaseView.hidden = YES;
    }
    // 图片
    if (K_DEFULT(_detailModel.video).length >0) {
        self.imgBaseView.hidden = NO;
        self.orderImgsView.videoPath = _detailModel.video;
        self.orderImgsView.imageArray= @[_detailModel.videoPicture];
    }else {
        self.imgBaseView.hidden = K_DEFULT(self.detailModel.picture).length == 0;
        if (K_DEFULT(self.detailModel.picture).length > 0) {
            NSArray *imageUrls = [self.detailModel.picture componentsSeparatedByString:@","];
            self.orderImgsView.imageArray = imageUrls;
        }
    }
    // 客户信息
    self.nameLabel.text = self.detailModel.receiver;
    self.mobileLabel.text = self.detailModel.telphone;
    self.addOrderTimeLabel.text = self.detailModel.receiveTime;
    self.letgoTimeLabel.text = self.detailModel.departureTime;
}
#pragma mark - Network
#pragma mark -- 获取订单详情数据
- (void)requestForOrderDetail{
    [NetworkingTool getWithUrl:kTKTruckingOrderDetailURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[RWOrderDetailModel alloc] initWithDictionary:dataDic];
            [self updateSubviews];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - Delegate
#pragma mark – Function
#pragma mark -- 确认出发
- (void)requestForLetGoOrder:(NSString *)orderId
{
    WeakSelf
    [NetworkingTool postWithUrl:kTKTruckingSetOutURL params:@{@"orderId":orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self requestForOrderDetail];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 取消
- (void)showCancelReasonView
{
    if (!self.reasonView) {
        self.reasonView = [[[NSBundle mainBundle] loadNibNamed:@"CancelOrderView" owner:nil options:nil] firstObject];
        self.reasonView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.reasonView];
    }
    [self.reasonView show];
    WeakSelf
    [self.reasonView setDidSelectBlock:^(NSString * _Nonnull selectedStr, NSString * _Nonnull ID) {
        [weakSelf requestForCancelOrder:ID reasonStr:selectedStr];
    }];
}
- (void)requestForCancelOrder:(NSString *)ID reasonStr:(NSString *)reason
{
    [NetworkingTool postWithUrl:kTKTruckingCancleURL params:@{@"orderId":self.orderId, @"cancelId":ID,@"cancelRemark":reason} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"取消成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 *NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 删除
- (void)requestForDeleteOrder
{
    [NetworkingTool postWithUrl:kTKTruckingDeleteOrderURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark – XibFunction
#pragma mark - 按钮的点击
// 0:取消  1:开始出发 2删除  3联系客户 4上传图片
- (IBAction)buttonsAction:(UIButton *)sender {
    NSInteger tag = sender.tag;
    WeakSelf
    // 取消接单
    if (tag==0) {
        [weakSelf showCancelReasonView];
        return;
    }
    // 开始出发
    if (tag==1) {
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"出发后不可取消订单？" titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForLetGoOrder:self.orderId];
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        return;
    }
    // 删除订单
    if (tag==2) {
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForDeleteOrder];
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        return;
    }
    // 联系客户
    if (tag==3) {
        [self callButtonAction:nil];
        return;
    }
    // 上传图片
    if (tag==4) {
        FinishOrderController *vc = [[FinishOrderController alloc] init];
        vc.orderId = self.orderId;
        vc.type = 2;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
}
#pragma mark - 联系用户
- (IBAction)callButtonAction:(id)sender {
    [LJTools call:self.detailModel.telphone];
}

#pragma mark - 导航
- (IBAction)guideViewTap:(id)sender {
    if ([self.detailModel.status integerValue] == 3) {
        LMMapViewController *vc = [[LMMapViewController alloc] init];
        vc.coordinate = CLLocationCoordinate2DMake([_detailModel.latitude floatValue], [_detailModel.longitude floatValue]);
        vc.city = _detailModel.city;
        vc.district = _detailModel.area;
        vc.address = _detailModel.street;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - Lazy Loads


@end
