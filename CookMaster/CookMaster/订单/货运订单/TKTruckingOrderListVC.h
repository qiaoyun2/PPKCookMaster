//
//  TKTruckingOrderListVC.h
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TKTruckingOrderListVC : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSInteger status; // FS状态 0全部 1待出发 2待确认 3已完成 4已取消

@end

NS_ASSUME_NONNULL_END
