//
//  TKTruckingOrderListCell.h
//  PPKMaster
//
//  Created by null on 2022/4/15.
//

#import <UIKit/UIKit.h>
#import "RWOrderListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TKTruckingOrderListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *carTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (weak, nonatomic) IBOutlet UIButton *deleteButton; // 删除
///联系
@property (weak, nonatomic) IBOutlet UIButton *contectBtn;
///出发
@property (weak, nonatomic) IBOutlet UIButton *letgoBtn;
@property (weak, nonatomic) IBOutlet UIStackView *buttonsStackView;
@property (weak, nonatomic) IBOutlet UIButton *picBtn;


// 2:删除  3:联系客户 4:开始出发
@property (nonatomic, copy) void(^onButtonsClick)(NSInteger tag);
@property (nonatomic, strong) RWOrderListModel *model;



@end

NS_ASSUME_NONNULL_END
