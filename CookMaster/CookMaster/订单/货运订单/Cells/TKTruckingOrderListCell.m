//
//  TKTruckingOrderListCell.m
//  PPKMaster
//
//  Created by null on 2022/4/15.
//

#import "TKTruckingOrderListCell.h"


static dispatch_source_t _timer;

@implementation TKTruckingOrderListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(RWOrderListModel *)model
{
    _model = model;
    NSInteger status = [model.status integerValue];
    self.orderNumLabel.text = model.orderId;
    self.statusLabel.text = [LJTools getMTStatusFormat:status];
    self.nicknameLabel.text = model.receiver;
//    self.carTypeLabel.text = [NSString stringWithFormat:@"%@ %@", model.firstClassifyName, model.secondClassifyName];
    self.timeLabel.text = model.reserveTime;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.area,model.street];
    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    // 0-全部；3-已接单待出发 ；4-待确认（可上传图片）; 5-已完成(待评价-用户点完成，但是必须师傅先上传图片);6-已取消(订单已取消)；7已关闭（退款成功或失败，未支付关闭的订单不会展示在师傅端）
    if (model.refundStatus.intValue > 0 ) {
        self.contectBtn.hidden = NO;
        switch (model.refundStatus.intValue) {
            case 1:
                self.statusLabel.text = @"售后中";
                break;
            case 2:
                self.statusLabel.text = @"售后失败";
                break;
            case 3:
                self.statusLabel.text = @"售后成功";
                self.deleteButton.hidden = NO;
                break;
            default:
                break;
        }
    }else{
        if (status == 3) {
            self.contectBtn.hidden = NO;
            self.letgoBtn.hidden = NO;
        }
        if (status == 4) {
            self.contectBtn.hidden = NO;
            self.picBtn.hidden = K_DEFULT(model.masterPicture).length > 0;
        }
        if (status == 5 || status == 6 || status == 7) {
            self.deleteButton.hidden = NO;
        }
    }
}

// 2:删除  3:联系客户   4:开始出发 5上传图片
- (IBAction)buttonsAction:(UIButton *)sender {
    if (self.onButtonsClick) {
        self.onButtonsClick(sender.tag - 10);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
