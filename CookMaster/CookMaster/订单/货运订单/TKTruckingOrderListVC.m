//
//  TKTruckingOrderListVC.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "TKTruckingOrderListVC.h"
#import "TKTruckingOrderListCell.h"
#import "TKTruckingOrderDetailVC.h"
#import "ConfirmAmountView.h"
#import "PayViewController.h"
#import "CancelOrderView.h"
#import "UploadManager.h"
#import "FinishOrderController.h"

@interface TKTruckingOrderListVC () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) CancelOrderView *reasonView;

@end

@implementation TKTruckingOrderListVC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = false;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setValue:@(_page) forKey:@"pageNo"];
    [params setValue:@(10) forKey:@"pageSize"];
    // FS状态 0全部 1待出发 2待确认 3已完成 4已取消
    // 0-全部；3-已接单待出发 ；4-待确认（可上传图片）; 5-已完成(待评价-用户点完成，但是必须师傅先上传图片);6-已取消(订单已取消)；7已关闭（退款成功或失败，未支付关闭的订单不会展示在师傅端）
    [params setValue:@(self.status) forKey:@"status"];
    WeakSelf
    [NetworkingTool getWithUrl:kTKTruckingMyOrderListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [weakSelf.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                RWOrderListModel *model = [[RWOrderListModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf addBlankOnView:weakSelf.tableView];
        weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - 取消
- (void)requestForCancelOrder:(RWOrderListModel *)model reasonId:(NSString *)ID reasonStr:(NSString *)reason
{
    WeakSelf
    [NetworkingTool postWithUrl:kTKTruckingCancleURL params:@{@"orderId":model.orderId, @"cancelId":ID,@"cancelRemark":reason} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"取消成功" delay:1.5];
            [weakSelf.dataArray removeObject:model];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf refresh];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 删除
- (void)requestForDeleteOrder:(RWOrderListModel *)model
{
    WeakSelf
    [NetworkingTool postWithUrl:kTKTruckingDeleteOrderURL params:@{@"orderId":model.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            [weakSelf.dataArray removeObject:model];
            [weakSelf.tableView reloadData];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 确认出发
- (void)requestForLetGoOrder:(RWOrderListModel *)model
{
    WeakSelf
    [NetworkingTool postWithUrl:kTKTruckingSetOutURL params:@{@"orderId":model.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self refresh];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKTruckingOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TKTruckingOrderListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TKTruckingOrderListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    RWOrderListModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    WeakSelf
    [cell setOnButtonsClick:^(NSInteger tag) {
        /// ///   2:删除 3联系客服户 4开始出发
        if (tag==2){
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForDeleteOrder:model];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }else if (tag==3){
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删拨打电话？" titlesArry:@[@"立即拨打"] indexBlock:^(NSInteger index, id obj) {
                [LJTools call:model.telphone];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }else if (tag==4){
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"出发后不可取消订单？" titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForLetGoOrder:model];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }else if (tag == 5){
            FinishOrderController *vc = [[FinishOrderController alloc] init];
            vc.orderId = model.orderId;
            vc.type = 2;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RWOrderListModel *model = self.dataArray[indexPath.row];
    TKTruckingOrderDetailVC *vc = [[TKTruckingOrderDetailVC alloc] init];
    vc.orderId = model.orderId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 24;
}

#pragma mark - Function
//- (void)showConfirmAmountView
//{
//    ConfirmAmountView *view = [[[NSBundle mainBundle] loadNibNamed:@"ConfirmAmountView" owner:nil options:nil] firstObject];
//    view.frame = [UIScreen mainScreen].bounds;
//    [[UIApplication sharedApplication].keyWindow addSubview:view];
//    [view showPopView];
//    [view setOnCofirmAmount:^(NSString * _Nonnull money) {
//        PayViewController *vc = [[PayViewController alloc] init];
//    }];
//}

- (void)showCancelReasonView:(RWOrderListModel *)model
{
    if (!self.reasonView) {
        self.reasonView = [[[NSBundle mainBundle] loadNibNamed:@"CancelOrderView" owner:nil options:nil] firstObject];
        self.reasonView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.reasonView];
    }
    [self.reasonView show];
    WeakSelf
    [self.reasonView setDidSelectBlock:^(NSString * _Nonnull selectedStr, NSString * _Nonnull ID) {
        [weakSelf requestForCancelOrder:model reasonId:ID reasonStr:selectedStr];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
