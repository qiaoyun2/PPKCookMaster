//
//  CancelReasonCell.h
//  PPKMaster
//
//  Created by null on 2022/4/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CancelReasonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *reasonLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectedButton;


@end

NS_ASSUME_NONNULL_END
