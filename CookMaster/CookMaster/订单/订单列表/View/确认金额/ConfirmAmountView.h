//
//  ConfirmAmountView.h
//  PPK
//
//  Created by null on 2022/4/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConfirmAmountView : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *moneyField;

@property (nonatomic, copy) void(^onCofirmAmount)(NSString *money);

//展示出现
-(void)showPopView;
//隐藏出现
-(void)dismissPopView;

@end

NS_ASSUME_NONNULL_END
