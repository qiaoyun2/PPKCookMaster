//
//  RWOrderListCell.h
//  PPK
//
//  Created by liuhan on 2022/4/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RWOrderListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *reserveTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *countDownLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton; // 取消
@property (weak, nonatomic) IBOutlet UIButton *contactButton; // 联系
@property (weak, nonatomic) IBOutlet UIButton *commentButton; // 评论
@property (weak, nonatomic) IBOutlet UIButton *deleteButton; // 删除

/// 0:取消  1:联系  2:评论  3:删除
@property (nonatomic, copy) void(^onButtonsClick)(NSInteger tag);

@end

NS_ASSUME_NONNULL_END
