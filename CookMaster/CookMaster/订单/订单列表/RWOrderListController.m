//
//  RWOrderListController.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWOrderListController.h"
#import "RWOrderListCell.h"
#import "RWOrderDetailController.h"
//#import "UpdateImageView.h"
#import "ConfirmAmountView.h"
#import "PayViewController.h"
#import "CancelOrderView.h"
#import "UploadManager.h"
#import "FinishOrderController.h"

@interface RWOrderListController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) CancelOrderView *reasonView;

@end

@implementation RWOrderListController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([LJTools islogin]) {
        [self requestForUserInfo];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = false;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authenticationSuccess) name:@"AuthenticationSuccess" object:nil];
    
}
#pragma mark - 刷新认证信息
- (void) authenticationSuccess{
    [self requestForUserInfo];
}
#pragma mark - 获取认证信息
- (void)requestForUserInfo
{
    WeakSelf
    [NetworkingTool getWithUrl:kGetUserInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            User *user = [User mj_objectWithKeyValues:responseObject[@"data"]];
            [User saveUser:user];
            if (user.authMasterStatus.integerValue==2) {
                [weakSelf refresh];
            }else {
                [weakSelf.dataArray removeAllObjects];
                [weakSelf addBlankOnView:weakSelf.tableView];
                weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
                [weakSelf.tableView reloadData];
            }
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
  
    //// 1:货运师傅；2：维修师傅；3：废品回收师傅
    NSString *url = @"";
    if ([User getAuthMasterType].intValue == 3){
        url = kRWOrderListURL;
        params[@"status"] = @(self.status);
    }else if ([User getAuthMasterType].intValue == 2){
        url = kMTRepairMyOrderListURL;
        params[@"status"] = @(self.typeStatus);
    }else{
        url = kRWOrderListURL;
        params[@"status"] = @(self.status);
    }
//
    
    WeakSelf
    [NetworkingTool getWithUrl:url params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [weakSelf.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                RWOrderListModel *model = [[RWOrderListModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf addBlankOnView:weakSelf.tableView];
        weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - 取消
- (void)requestForCancelOrder:(RWOrderListModel *)model reasonId:(NSString *)ID reasonStr:(NSString *)reason
{
    NSString *url = @"";
    if ([User getAuthMasterType].intValue == 3){
        url = kRWCancelOrderURL;
    }else if ([User getAuthMasterType].intValue == 2){
        url = kMTRepairOrderCancelURL;
    }
    NSMutableDictionary *pama = [NSMutableDictionary dictionary];
    pama[@"orderId"] = model.orderId;
    pama[@"cancelId"] = ID;
    pama[@"cancelRemark"] = reason;
    
    WeakSelf
    [NetworkingTool postWithUrl:url params:pama success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"取消成功" delay:1.5];
            [weakSelf.dataArray removeObject:model];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf refresh];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 删除
- (void)requestForDeleteOrder:(RWOrderListModel *)model
{
    NSString *url = @"";
    if ([User getAuthMasterType].intValue == 3){
        url = kRWDeleteOrderURL;
    }else if ([User getAuthMasterType].intValue == 2){
        url = kMTRepairDeleteOrderURL;
    }
    WeakSelf
    [NetworkingTool postWithUrl:url params:@{@"orderId":model.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            [weakSelf.dataArray removeObject:model];
            [weakSelf.tableView reloadData];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 完成
- (void)requestForFinishOrder:(RWOrderListModel *)model
{
    WeakSelf
    [NetworkingTool postWithUrl:kRWCompleteOrderURL params:@{@"orderId":model.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [weakSelf refresh];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 确认出发
- (void)requestForLetGoOrder:(RWOrderListModel *)model
{
    WeakSelf
    [NetworkingTool postWithUrl:kMTQueryHomeSetOutURL params:@{@"orderId":model.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"已确认出发" delay:1.5];
            [weakSelf refresh];
            
//            FinishOrderController *vc = [[FinishOrderController alloc] init];
//            vc.orderId = model.orderId;
//            vc.type = 1;
//            [weakSelf.navigationController pushViewController:vc animated:YES];
 
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 上传师傅照片
- (void)requestForUploadPicture:(NSString *)orderId image:(NSString *)imageStr
{
    NSDictionary *dict = @{
        @"orderId":orderId,
        @"masterPicture":imageStr
    };
    WeakSelf
    [NetworkingTool postWithUrl:kMTRepairUploadPictureURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"上传成功" delay:1.5];
            [weakSelf refresh];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RWOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RWOrderListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"RWOrderListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    RWOrderListModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    WeakSelf
    [cell setOnButtonsClick:^(NSInteger tag) {
        /// /// 0:取消  1:完成    2:删除 3联系客服户 4开始出发
        if (tag==0) {
            [weakSelf showCancelReasonView:model];
        }
        else if (tag==1) {
            FinishOrderController *vc = [[FinishOrderController alloc] init];
            vc.orderId = model.orderId;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        } else if (tag==2){
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForDeleteOrder:model];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }else if (tag==3){
            
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删拨打电话？" titlesArry:@[@"立即拨打"] indexBlock:^(NSInteger index, id obj) {
                [LJTools call:model.telphone];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }else if (tag==5){
            
            FinishOrderController *vc = [[FinishOrderController alloc] init];
            vc.orderId = model.orderId;
            vc.type = 1;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else if (tag==4){
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"出发后不可取消订单？" titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForLetGoOrder:model];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RWOrderListModel *model = self.dataArray[indexPath.row];
    RWOrderDetailController *vc = [[RWOrderDetailController alloc] init];
    vc.orderId = model.orderId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 24;
}

#pragma mark - Function
//- (void)showConfirmAmountView
//{
//    ConfirmAmountView *view = [[[NSBundle mainBundle] loadNibNamed:@"ConfirmAmountView" owner:nil options:nil] firstObject];
//    view.frame = [UIScreen mainScreen].bounds;
//    [[UIApplication sharedApplication].keyWindow addSubview:view];
//    [view showPopView];
//    [view setOnCofirmAmount:^(NSString * _Nonnull money) {
//        PayViewController *vc = [[PayViewController alloc] init];
//    }];
//}

- (void)showCancelReasonView:(RWOrderListModel *)model
{
    if (!self.reasonView) {
        self.reasonView = [[[NSBundle mainBundle] loadNibNamed:@"CancelOrderView" owner:nil options:nil] firstObject];
        self.reasonView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.reasonView];
    }
    [self.reasonView show];
    WeakSelf
    [self.reasonView setDidSelectBlock:^(NSString * _Nonnull selectedStr, NSString * _Nonnull ID) {
        [weakSelf requestForCancelOrder:model reasonId:ID reasonStr:selectedStr];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
