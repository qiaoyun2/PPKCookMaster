//
//  RWOrderDetailModel.m
//  PPK
//
//  Created by null on 2022/4/20.
//

#import "RWOrderDetailModel.h"

@implementation RWOrderDetailModel

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
    if ([key isEqualToString:@"commentVO"]) {
        NSDictionary *dic = (NSDictionary *)value;
        self.commentVO = [[RWOrderCommentModel alloc] initWithDictionary:dic];
    }
}

@end
