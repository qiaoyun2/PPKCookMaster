//
//  RWOrderDetailModel.h
//  PPK
//
//  Created by null on 2022/4/20.
//

#import "BaseModel.h"
#import "RWOrderCommentModel.h"

NS_ASSUME_NONNULL_BEGIN

//@interface RWOrderCommentModel : BaseModel
//
//@end

@interface RWOrderDetailModel : BaseModel

@property (nonatomic, strong) NSString *area; //地区
@property (nonatomic, strong) NSString *cancelContent;//取消原因
@property (nonatomic, strong) NSString *city; //城市
@property (nonatomic, strong) NSString *completeTime; // 完成时间
@property (nonatomic, strong) NSString *country;//国家
@property (nonatomic, strong) NSString *firstClassify; // 电子产品
@property (nonatomic, strong) NSString *goodsNum; // 物品数量
@property (nonatomic, strong) NSString *goodsWeight; // 物品重量
@property (nonatomic, strong) NSString *followNum; // 跟车人数
@property (nonatomic, strong) NSString *ID; // 数据id
@property (nonatomic, strong) NSString *latitude;//纬度
@property (nonatomic, strong) NSString *longitude;//经度
@property (nonatomic, strong) NSString *orderId; // 订单编号
@property (nonatomic, strong) NSString *picture; //
@property (nonatomic, strong) NSString *province; // 装货地址省份
@property (nonatomic, strong) NSString *receiveTime; // 接单时间
@property (nonatomic, strong) NSString *receiver; // 收件人  // 装货地址联系人
@property (nonatomic, strong) NSString *remark; // 订单备注
@property (nonatomic, strong) NSString *reserveTime; // 预约时间段 (用车时间)
@property (nonatomic, strong) NSString *resourceType; // 资源类型：1-图片；2-视频
@property (nonatomic, strong) NSString *secondClassify; // 笔记本电脑
@property (nonatomic, strong) NSString *status; // 订单状态：1-预约中；2-待回收；3-已完成；4-用户已取消
@property (nonatomic, strong) NSString *street; // 装货地址街道
@property (nonatomic, strong) NSString *telphone; // 装货地址用户手机号
@property (nonatomic, strong) NSString *video; // 视频地址
@property (nonatomic, strong) NSString *videoPicture; // 视频封面
@property (nonatomic, strong) NSString *createTime; // 下单时间
@property (nonatomic, strong) RWOrderCommentModel *commentVO; // //评论

@property (nonatomic, strong) NSString *cancelTime; // 剩余可取消时间，单位：秒


@property (nonatomic, strong) NSString *refundFailRemark; // 失败原因
@property (nonatomic, strong) NSString *refundPictures; // 售后图片
@property (nonatomic, strong) NSString *refundReason; // 售后原因
@property (nonatomic, strong) NSString *refundStatus; // 售后状态0未申请 1申请中 2申请失败 3申请成功
///售后时间
@property (nonatomic, strong) NSString *refundTime;

/**-------------维修订单详情----------*/

@property (nonatomic, strong) NSString *departureTime; // 出发时间
@property (nonatomic, strong) NSString *unArea; //  // 卸货地址区
@property (nonatomic, strong) NSString *unCity; // // 卸货地址城市
@property (nonatomic, strong) NSString *unCountry; // // 卸货地址国家
@property (nonatomic, strong) NSString *unLatitude; // 卸货地址纬度
@property (nonatomic, strong) NSString *unLongitude; // // 卸货地址经度
@property (nonatomic, strong) NSString *unProvince; // // 卸货地址省份
@property (nonatomic, strong) NSString *unReceiver; // 卸货地址用户昵称
@property (nonatomic, strong) NSString *unStreet; // 卸货地址
@property (nonatomic, strong) NSString *unTelphone; // 卸货地址用户手机号
///师傅上传的图片
@property (nonatomic, strong) NSString *masterPicture;
//@property (nonatomic, strong) NSString *unStreet;
/**-------------货运订单详情----------*/
@property (nonatomic, copy) NSString *distance; // 距离


@end

NS_ASSUME_NONNULL_END
