//
//  RWOrderController.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWOrderController.h"
#import "RWOrderListController.h"
#import "YNPageViewController.h"
#import "UIView+YNPageExtend.h"
#import "ReLayoutButton.h"
#import "TKTruckingOrderListVC.h" // 货运订单列表

@interface RWOrderController ()<YNPageViewControllerDataSource, YNPageViewControllerDelegate>

@property (nonatomic, strong) NSArray *titles;


@end

@implementation RWOrderController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单";
    self.titles = @[@"全部",@"待回收",@"已完成",@"已取消"];
    //// 1:货运师傅；2：维修师傅；3：废品回收师傅
    if ([User getAuthMasterType].intValue == 3){
        self.titles = @[@"全部",@"待回收",@"已完成",@"已取消"];
    }else if ([User getAuthMasterType].intValue == 2 || [User getAuthMasterType].intValue == 1){
        //维修订单的状态：0-全部；3-已接单待出发 ；4-待确认（可上传图片）; 5-已完成(待评价-用户点完成，但是必须师傅先上传图片);6-已取消(订单已取消)；7已关闭（退款成功或失败，未支付关闭的订单
        self.titles = @[@"全部",@"待出发",@"待确认",@"已完成",@"已取消"];
    }
    [self setupPageVC];
}

- (void)setupPageVC {
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    configration.pageStyle = YNPageStyleTop;
    /// 控制tabbar 和 nav
    configration.showTabbar = YES;
    configration.showNavigation = YES;
    //scrollMenu = NO,aligmentModeCenter = NO 会变成平分
    configration.scrollMenu = NO;
    configration.aligmentModeCenter = NO;
    configration.showBottomLine = NO;
    configration.showScrollLine = NO;
    configration.itemMargin = 0;
//    configration.bottomLineBgColor = RGBA(218, 98, 57, 1);
//    configration.scrollViewBackgroundColor = UIColorFromRGB(0xF5F5F5);
    configration.menuHeight = 50;
    configration.normalItemColor = RGBA(51, 51, 51, 1);
    configration.selectedItemColor = RGBA(51, 51, 51, 1);
    configration.itemFont = [UIFont systemFontOfSize:16];
    configration.selectedItemFont = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    configration.cutOutHeight = 0;
    
    NSMutableArray *buttonArrayM = [NSMutableArray array];
    for (int i = 0; i<self.titles.count; i++) {
        ReLayoutButton *button = [ReLayoutButton buttonWithType:UIButtonTypeCustom];
        button.layoutType = 3;
        button.margin = 3;
        UIImage *selectImage = [UIImage imageNamed:@"矩形 1884"];
        [button setImage:[UIImage imageNamed:@"矩形 1884-1"] forState:UIControlStateNormal];
        [button setImage:selectImage forState:UIControlStateSelected];
        [buttonArrayM addObject:button];
    }
    configration.buttonArray = buttonArrayM;
    
    YNPageViewController *vc = [YNPageViewController pageViewControllerWithControllers:self.getArrayVCs titles:self.titles  config:configration];
    vc.dataSource = self;
    vc.delegate = self;
    /// 指定默认选择index 页面
    vc.pageIndex = 0;
    /// 作为自控制器加入到当前控制器
    [vc addSelfToParentViewController:self];
}

- (NSArray *)getArrayVCs {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSInteger i = 0; i<self.titles.count; i++) {
        if ([User getAuthMasterType].intValue == 1) {
            TKTruckingOrderListVC *vc = [[TKTruckingOrderListVC alloc] init];
            if (i==0) {
                vc.status = i;
            }else {
                //货运订单的状态：0-全部；3-已接单待出发 ；4-待确认（可上传图片）; 5-已完成(待评价-用户点完成，但是必须师傅先上传图片);6-已取消(订单已取消)；7已关闭（退款成功或失败，未支付关闭的订单
                vc.status = i+2;
            }
            [tempArray addObject:vc];
        }else{
            RWOrderListController *vc = [[RWOrderListController alloc] init];
            if (i==0) {
                vc.status = i;
                vc.typeStatus = i;
            }else {
                //维修订单的状态：0-全部；3-已接单待出发 ；4-待确认（可上传图片）; 5-已完成(待评价-用户点完成，但是必须师傅先上传图片);6-已取消(订单已取消)；7已关闭（退款成功或失败，未支付关闭的订单
                if ([User getAuthMasterType].intValue == 2) {
                    vc.typeStatus = i+2;
                }else{
                    /// 0全部 2-待回收；3-已完成；4-已取消
                    vc.status = i+1;
                }

            }
            [tempArray addObject:vc];
        }
    }
    return [tempArray copy];
}

#pragma mark - YNPageViewControllerDataSource
- (UIScrollView *)pageViewController:(YNPageViewController *)pageViewController pageForIndex:(NSInteger)index {
    UIViewController *vc = pageViewController.controllersM[index];
    if ([User getAuthMasterType].intValue == 1) {
        return [(TKTruckingOrderListVC *)vc tableView];
    }
    return [(RWOrderListController *)vc tableView];
}

#pragma mark - YNPageViewControllerDelegate
- (void)pageViewController:(YNPageViewController *)pageViewController
            contentOffsetY:(CGFloat)contentOffset
                  progress:(CGFloat)progress {
}

- (void)pageViewController:(YNPageViewController *)pageViewController
                 didScroll:(UIScrollView *)scrollView
                  progress:(CGFloat)progress
                 formIndex:(NSInteger)fromIndex
                   toIndex:(NSInteger)toIndex
{

}



@end
