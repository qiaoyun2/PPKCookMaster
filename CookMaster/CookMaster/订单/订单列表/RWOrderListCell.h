//
//  RWOrderListCell.h
//  PPKMaster
//
//  Created by null on 2022/4/15.
//

#import <UIKit/UIKit.h>
#import "RWOrderListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWOrderListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *reserveTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *countDownLabel;
@property (weak, nonatomic) IBOutlet UIView *countDownView;
@property (weak, nonatomic) IBOutlet UIView *customerView;
@property (weak, nonatomic) IBOutlet UIView *classView;
@property (weak, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *classLabel;


@property (weak, nonatomic) IBOutlet UIButton *cancelButton; // 取消
@property (weak, nonatomic) IBOutlet UIButton *finishButton; // 完成
@property (weak, nonatomic) IBOutlet UIButton *deleteButton; // 删除
///联系
@property (weak, nonatomic) IBOutlet UIButton *contectBtn;
///出发
@property (weak, nonatomic) IBOutlet UIButton *letgoBtn;
///上传图片
@property (weak, nonatomic) IBOutlet UIButton *uploadImageBtn;


@property (weak, nonatomic) IBOutlet UIStackView *buttonsStackView;


/// 0:取消  1:完成    2:删除 3联系客服户 4开始出发
@property (nonatomic, copy) void(^onButtonsClick)(NSInteger tag);
@property (nonatomic, strong) RWOrderListModel *model;



@end

NS_ASSUME_NONNULL_END
