//
//  RWOrderListController.h
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWOrderListController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, assign) NSInteger typeStatus;
@end

NS_ASSUME_NONNULL_END
