//
//  RWOrderListCell.m
//  PPKMaster
//
//  Created by null on 2022/4/15.
//

#import "RWOrderListCell.h"


//static dispatch_source_t _timer;
@interface RWOrderListCell()
#pragma mark -- <倒计时>
/** timer */
@property(nonatomic , strong)dispatch_source_t timer;

@end
@implementation RWOrderListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(RWOrderListModel *)model
{
    _model = model;
    self.orderNumLabel.text = model.orderId;
    self.reserveTimeLabel.text = model.reserveTime;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.area,model.street];
    
    NSInteger status = [model.status integerValue];
   

    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    self.countDownView.hidden = YES;
    if ([User getAuthMasterType].intValue == 3) {
       
        self.statusLabel.text = [LJTools getRWStatusFormat:status];
        if (status==2) {
            // 待回收
            self.finishButton.hidden = NO;
            if (model.cancelTime.intValue>0) {
                self.countDownView.hidden = NO;
                self.cancelButton.hidden = NO;
                [self startTimer:model.cancelTime.intValue];
            }else {
                self.countDownView.hidden = YES;
                self.cancelButton.hidden = YES;
            }
        }
        else if (status==3) {
            // 已完成
            self.deleteButton.hidden = NO;
        }
        else {
            // 已取消
            self.deleteButton.hidden = NO;
        }
    }else if ([User getAuthMasterType].intValue == 2){
        self.classView.hidden = self.customerView.hidden = NO;
        self.statusLabel.text = [LJTools getMTStatusFormat:status];
        self.classLabel.text = [NSString stringWithFormat:@"%@ %@",model.firstClassifyName,model.secondClassifyName];
        self.customerNameLabel.text = model.receiver;
        ///3-已接单待出发 ；4-待确认（可上传图片）; 5-已完成(待评价-用户点完成，但是必须师傅先上传图片);6-已取消(订单已取消)；7已关闭（退款成功或失败，未支付关闭的订单
        if (model.refundStatus.intValue > 0 ) {
            switch (model.refundStatus.intValue) {
                case 1:
                    self.statusLabel.text = @"售后中";
                    break;
                case 2:
                    self.statusLabel.text = @"售后失败";
                {
                     if (status == 4) {
                        self.uploadImageBtn.hidden = model.masterPicture.length > 0;
                        // 已完成
                        self.contectBtn.hidden = NO;
                    }else {
                        // 已取消
                        self.deleteButton.hidden = NO;
                    }
                }
                    break;
                case 3:
                    self.statusLabel.text = @"售后成功";
                    self.deleteButton.hidden = NO;
                    break;
                default:
                    break;
            }
        }else{
            if (status==3) {////    status==3,师傅可以取消，并且有倒计时
                self.letgoBtn.hidden = self.contectBtn.hidden = NO;
            }
            else if (status==4) {
                self.uploadImageBtn.hidden = model.masterPicture.length > 0;
                // 已完成
                self.contectBtn.hidden = NO;
            }
            else {
                // 已取消
                self.deleteButton.hidden = NO;
            }
        }
      
    }
    
   
}

- (void)startTimer:(int)time
{
    __block int timeout=time;
    //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
//            [self cancelCountdownWithEndString:endStr];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.countDownView.hidden = YES;
                self.cancelButton.hidden = YES;
            });
        }else{
            int seconds = timeout;
            //format of minute
            NSString *str_minute = [NSString stringWithFormat:@"%02d",(seconds%3600)/60];
            //format of second
            NSString *str_second = [NSString stringWithFormat:@"%02d",seconds%60];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.countDownLabel.text = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

- (IBAction)buttonsAction:(UIButton *)sender {
    if (self.onButtonsClick) {
        self.onButtonsClick(sender.tag);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
