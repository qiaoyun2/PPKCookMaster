//
//  FinishOrderController.h
//  PPKMaster
//
//  Created by null on 2022/5/5.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FinishOrderController : BaseViewController

@property (nonatomic, strong) NSString *orderId;
///0 完成 1，维修出发上传图片 2货运
@property (nonatomic, assign) NSInteger type;
@end

NS_ASSUME_NONNULL_END
