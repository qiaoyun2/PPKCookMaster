//
//  RWOrderDetailController.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWOrderDetailController.h"
#import "FoundListImage9TypographyView.h"
#import "LMMapViewController.h"
#import "RWOrderDetailModel.h"
#import "CancelOrderView.h"
#import "GBStarRateView.h"
#import "UploadManager.h"
#import "FinishOrderController.h"

@interface RWOrderDetailController ()

/** 状态*/
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
///详细描述
@property (weak, nonatomic) IBOutlet UILabel *statusDetailLabel;
@property (weak, nonatomic) IBOutlet UIView *unConfirmTipView; // 待回收状态提示
@property (weak, nonatomic) IBOutlet UILabel *cancelTipLabel; // 取消状态提示
@property (weak, nonatomic) IBOutlet UIView *reserveTimeView; // 预约上门时间
@property (weak, nonatomic) IBOutlet UILabel *reserveTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *startTimeView; // 开始时间
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *topPayTimeView; // 付款时间
///取消订单倒计时
@property (weak, nonatomic) IBOutlet UILabel *countDownLabel;
@property (weak, nonatomic) IBOutlet UIView *countDownView;
@property (weak, nonatomic) IBOutlet UILabel *topPayTimeLabel;
//FIXME: qy -- 回收地址
@property (weak, nonatomic) IBOutlet UIView *rwAddressView;
/** 用户评价*/
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UILabel *commentTimeLabel; // 评论时间
@property (weak, nonatomic) IBOutlet UILabel *introduceLbel; // 描述
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;
@property (weak, nonatomic) IBOutlet UILabel *commentNameLabel;
@property (weak, nonatomic) IBOutlet GBStarRateView *starView;
///回收类型/维修类型/？
@property (weak, nonatomic) IBOutlet UILabel *classNameLabel;
///回收数量/预约服务时间
@property (weak, nonatomic) IBOutlet UILabel *numAndtimeLabel;
///完成时间
@property (weak, nonatomic) IBOutlet UIView *finishView;
@property (weak, nonatomic) IBOutlet UILabel *finishTimeLabel;
/** 取消信息*/
@property (weak, nonatomic) IBOutlet UIView *cancelView;
@property (weak, nonatomic) IBOutlet UILabel *cancelReasonLabel; // 取消原因
/** 订单详细信息*/
@property (weak, nonatomic) IBOutlet UILabel *typeLabel; // 回收类型
@property (weak, nonatomic) IBOutlet UILabel *numLabel; // 回收数量
//FIXME: qy -- 回收：回收重量，维修：预约服务地址
@property (weak, nonatomic) IBOutlet UILabel *weightLabel; // 回收数量
//FIXME: qy -- 回收：回收数量名字，维修：预约服务地址
@property (weak, nonatomic) IBOutlet UILabel *numAndAddressLabel;
//FIXME: qy -- 维修图片
///维修图片
@property (weak, nonatomic) IBOutlet UIView *mtImageBgView;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *mtImageListView;
@property (weak, nonatomic) IBOutlet UIView *rwImageBgView;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel; // 备注
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *orderImgsView;
///回收数量view
@property (weak, nonatomic) IBOutlet UIView *reworkNumView;
/** 信息 */
@property (weak, nonatomic) IBOutlet UILabel *nameLabel; // 姓名
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel; // 联系方
@property (weak, nonatomic) IBOutlet UILabel *addOrderTimeLabel; // 接单时间
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *payTimeView; // 付款时间
@property (weak, nonatomic) IBOutlet UILabel *payTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *cancelTimeView; // 取消时间
@property (weak, nonatomic) IBOutlet UILabel *cancelTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton; // 取消  0
@property (weak, nonatomic) IBOutlet UIButton *finishButton; // 完成  2
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;  // 删除  3
@property (weak, nonatomic) IBOutlet UIStackView *buttonsStackView;
///售后时间view
@property (weak, nonatomic) IBOutlet UIView *refundTimeView;
///售后时间
@property (weak, nonatomic) IBOutlet UILabel *refundTimeLabel;
///出发
@property (weak, nonatomic) IBOutlet UIButton *letgoBtn;
///出发view
@property (weak, nonatomic) IBOutlet UIView *letgoTimeView;
///出发时间
@property (weak, nonatomic) IBOutlet UILabel *letgoTimeLabel;
///上传图片
@property (weak, nonatomic) IBOutlet UIButton *uploadImageBtn;
///图片视频。维修：维修确认图
@property (weak, nonatomic) IBOutlet UILabel *imageNameLabel;
@property (nonatomic, strong) RWOrderDetailModel *detailModel;
@property (nonatomic, strong) CancelOrderView *reasonView;
@property (weak, nonatomic) IBOutlet UILabel *yuyueNameLabel;
#pragma mark -- <倒计时>
/** timer */
@property(nonatomic , strong)dispatch_source_t timer;

@end

@implementation RWOrderDetailController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self requestForOrderDetail];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单详情";
    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    self.imgsView.imageArray = @[@"girl", @"girl2", @"girl3",@"girl", @"girl2", @"girl3",@"girl", @"girl2", @"girl3"];
    self.starView.allowClickScore = NO;
    self.starView.allowSlideScore = NO;
    self.starView.spacingBetweenStars = 2;
    self.starView.starSize = CGSizeMake(10, 10);
    self.starView.starImage = [UIImage imageNamed:@"star_off"];
    self.starView.currentStarImage = [UIImage imageNamed:@"star_on"];
    self.starView.style = GBStarRateViewStyleIncompleteStar;
    self.starView.isAnimation = NO;
    WeakSelf
    [self.addressView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        [weakSelf addressClick];
    }];
    
}


#pragma mark - Network
- (void)requestForOrderDetail
{
    //
    NSString *url = @"";
    if ([User getAuthMasterType].intValue == 3){
        url = kRWOrderDetailURL;
        
    }else if ([User getAuthMasterType].intValue == 2){
        url = kMTQueryMyOrderByIdURL;
        
    }
    [NetworkingTool getWithUrl:url params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[RWOrderDetailModel alloc] initWithDictionary:dataDic];
            [self updateSubviews];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 确认出发
- (void)requestForLetGoOrder:(NSString *)orderId
{
    WeakSelf
    [NetworkingTool postWithUrl:kMTQueryHomeSetOutURL params:@{@"orderId":orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"已确认出发" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 *NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestForOrderDetail];
            });
            
//            FinishOrderController *vc = [[FinishOrderController alloc] init];
//            vc.orderId = self.orderId;
//            vc.type = 1;
//            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 上传师傅照片
- (void)requestForUploadPicture:(NSString *)orderId image:(NSString *)imageStr
{
    NSDictionary *dict = @{
        @"orderId":orderId,
        @"masterPicture":imageStr
    };
    [NetworkingTool postWithUrl:kMTRepairUploadPictureURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"上传成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 *NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestForOrderDetail];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 取消
- (void)requestForCancelOrder:(NSString *)ID reasonStr:(NSString *)reason
{
    NSString *url = @"";
    if ([User getAuthMasterType].intValue == 3){
        url = kRWCancelOrderURL;
    }else if ([User getAuthMasterType].intValue == 2){
        url = kMTRepairOrderCancelURL;
    }
    NSMutableDictionary *pama = [NSMutableDictionary dictionary];
    pama[@"orderId"] = self.orderId;
    pama[@"cancelId"] = ID;
    pama[@"cancelRemark"] = reason;

    [NetworkingTool postWithUrl:url params:pama success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"取消成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 *NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestForOrderDetail];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 删除
- (void)requestForDeleteOrder
{
    //
    NSString *url = @"";
    if ([User getAuthMasterType].intValue == 3){
        url = kRWDeleteOrderURL;
    }else if ([User getAuthMasterType].intValue == 2){
        url = kMTRepairDeleteOrderURL;
    }
    
    [NetworkingTool postWithUrl:url params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 完成
- (void)requestForFinishOrder
{
    [NetworkingTool postWithUrl:kRWCompleteOrderURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self requestForOrderDetail];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - Function
- (void)updateSubviews
{
  self.topPayTimeView.hidden = self.finishView.hidden = self.letgoTimeView.hidden = self.rwImageBgView.hidden = self.mtImageBgView.hidden = YES;
    NSInteger status = [self.detailModel.status integerValue];
    
    self.numLabel.text = [NSString stringWithFormat:@"%@",_detailModel.goodsNum];
    
    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    
    if ([User getAuthMasterType].intValue == 3){
       
        [self updateRWStatus:status];
        self.classNameLabel.text = @"回收类型";
        self.numAndAddressLabel.text = @"回收重量";
        self.numAndtimeLabel.text = @"回收数量";
        self.weightLabel.text = [NSString stringWithFormat:@"%@",_detailModel.goodsWeight];
        self.topPayTimeView.hidden = self.rwAddressView.hidden = self.rwImageBgView.hidden = NO;
        
        if (_detailModel.video.length>0) {
            self.rwImageBgView.hidden = NO;
            self.orderImgsView.videoPath = _detailModel.video;
            self.orderImgsView.imageArray= @[_detailModel.videoPicture];
        } else {
            if (_detailModel.picture.length>0) {
                self.rwImageBgView.hidden = NO;
                NSArray *images = [_detailModel.picture componentsSeparatedByString:@","];
                self.orderImgsView.imageArray= images;
            }else{
                self.rwImageBgView.hidden = YES;
            }
        }
        
        self.statusLabel.text = [LJTools getRWStatusFormat:status];
        
    }else   if ([User getAuthMasterType].intValue == 2){

        self.mtImageBgView.hidden = NO;
        self.statusLabel.text = [LJTools getMTStatusFormat:status];
        [self updateMTStatus:status];
        self.classNameLabel.text = @"维修类型";
        self.numAndtimeLabel.text = @"预约服务时间";
       self.reserveTimeView.hidden = self.rwAddressView.hidden =  YES;
        self.numAndAddressLabel.text = @"预约服务地址";
        self.weightLabel.text =  [NSString stringWithFormat:@"%@%@%@%@",_detailModel.province,_detailModel.city,_detailModel.area,_detailModel.street];
        
        if (_detailModel.video.length>0) {
            self.mtImageBgView.hidden = NO;
            self.mtImageListView.videoPath = _detailModel.video;
            self.mtImageListView.imageArray= @[_detailModel.videoPicture];
        } else {
            if (_detailModel.picture.length>0) {
                self.mtImageBgView.hidden = NO;
                NSArray *images = [_detailModel.picture componentsSeparatedByString:@","];
                self.mtImageListView.imageArray= images;
            }else{
                self.mtImageBgView.hidden = YES;
            }
        }
       
    }
    
    self.reserveTimeLabel.text = _detailModel.receiveTime;
    self.startTimeLabel.text = _detailModel.receiveTime;
    self.topPayTimeLabel.text = _detailModel.completeTime;
    self.finishTimeLabel.text = _detailModel.completeTime;

    if (_detailModel.commentVO.userId.length) {
        RWOrderCommentModel *commentModel = _detailModel.commentVO;
        self.introduceLbel.text = commentModel.content;
        self.commentTimeLabel.text = commentModel.createTime;
        self.commentNameLabel.text = commentModel.isAnonymous.intValue == 1?@"匿名":commentModel.nickname;
        
        if (commentModel.video.length>0) {
            self.imgsView.videoPath = commentModel.video;
            self.imgsView.imageArray= @[commentModel.videoPicture];
        } else {
            if (commentModel.picture.length>0) {
                NSArray *images = [commentModel.picture componentsSeparatedByString:@","];
                self.imgsView.imageArray= images;
            }
        }
        self.starView.currentStarRate = [commentModel.starNum floatValue];
    }
    
    self.cancelReasonLabel.text = _detailModel.cancelContent;
    self.typeLabel.text = [NSString stringWithFormat:@"%@ %@",_detailModel.firstClassify, _detailModel.secondClassify];
    self.remarkLabel.text = _detailModel.remark;
    
    
    self.nameLabel.text = _detailModel.receiver;
    self.mobileLabel.text = _detailModel.telphone;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",_detailModel.province,_detailModel.city,_detailModel.area,_detailModel.street];
    self.addOrderTimeLabel.text = _detailModel.receiveTime;
    self.payTimeLabel.text = _detailModel.receiveTime;
    self.letgoTimeLabel.text = _detailModel.departureTime;
    
}
#pragma mark - 维修的隐藏
-(void)updateMTStatus:(NSInteger)status{
    self.numLabel.text = self.detailModel.reserveTime;
    
    if (_detailModel.masterPicture.length>0) {
        self.rwImageBgView.hidden = NO;
        self.imageNameLabel.text = @"确认维修图";
        NSArray *images = [_detailModel.masterPicture componentsSeparatedByString:@","];
        self.orderImgsView.imageArray= images;
    }else{
        self.rwImageBgView.hidden = YES;
    }
    ///预约服务时间不隐藏
    self.reworkNumView.hidden = self.refundTimeView.hidden =  NO;
    /// ///refundStatus 0-未申请;1-售后中;2-售后失;3-售后成功
    if (self.detailModel.refundStatus.intValue > 0 ) {
        self.uploadImageBtn.hidden = self.detailModel.refundStatus.intValue != 2;
        self.cancelTipLabel.text = self.detailModel.refundReason;
        self.finishView.hidden =  self.reserveTimeView.hidden = self.unConfirmTipView.hidden = self.startTimeView.hidden = YES;
       self.commentView.hidden =  self.cancelView.hidden = self.payTimeView.hidden =  self.cancelTimeView.hidden = YES;
        
        switch (self.detailModel.refundStatus.intValue) {
            case 1:
                self.statusLabel.text = @"售后中";
                break;
            case 2:
                self.statusLabel.text = @"售后失败";
                
                if(self.detailModel.status.intValue == 4){
                    self.uploadImageBtn.hidden = self.detailModel.masterPicture.length > 0;
                }else if (self.detailModel.status.intValue == 5){
                    self.uploadImageBtn.hidden = YES;
                    self.letgoTimeView.hidden =   self.deleteButton.hidden = NO;
                    self.commentView.hidden = !_detailModel.commentVO.userId.length;
                }
                break;
            case 3:
                self.statusLabel.text = @"售后成功";
                self.deleteButton.hidden = NO;
                break;
            default:
                break;
        }
      
    }else{
        self.refundTimeView.hidden = YES;
        ///3-已接单待出发 ；4-待确认（可上传图片）; 5-已完成(待评价-用户点完成，但是必须师傅先上传图片);6-已取消(订单已取消)；7已关闭（退款成功或失败，未支付关闭的订单
        switch (status) {
            case 3:
                // 已接单
                self.letgoBtn.hidden =  self.cancelButton.hidden =  self.unConfirmTipView.hidden =  NO;
                self.statusDetailLabel.text = @"和客户确认后再出发";
                self.startTimeView.hidden =   self.topPayTimeView.hidden =  self.cancelTipLabel.hidden = self.cancelView.hidden = self.payTimeView.hidden =  self.cancelTimeView.hidden = YES;
                self.commentView.hidden = !_detailModel.commentVO.userId.length;
                
                break;
            case 4:
                self.uploadImageBtn.hidden = self.detailModel.masterPicture.length > 0;
                // 待确认
                self.letgoTimeView.hidden =  self.unConfirmTipView.hidden =   NO;
                self.statusDetailLabel.text = @"等待客户，确认到达";
                self.startTimeView.hidden =  self.cancelTipLabel.hidden =  self.cancelView.hidden = self.payTimeView.hidden = self.cancelTimeView.hidden = YES;
                self.commentView.hidden = !_detailModel.commentVO.userId.length;

                break;
            case 5:
                self.cancelTipLabel.hidden = self.unConfirmTipView.hidden = self.cancelView.hidden = self.payTimeView.hidden = self.cancelTimeView.hidden = YES;
                self.statusDetailLabel.text = @"等待客户，确认到达";
                self.finishView.hidden = self.letgoTimeView.hidden =  self.startTimeView.hidden =   self.deleteButton.hidden = NO;
                self.commentView.hidden = !_detailModel.commentVO.userId.length;
               
                break;
            case 6:
                self.cancelView.hidden = self.cancelTipLabel.hidden = self.deleteButton.hidden = NO;
                self.reserveTimeView.hidden = self.startTimeView.hidden =    self.commentView.hidden = self.payTimeView.hidden = self.unConfirmTipView.hidden = self.cancelTimeView.hidden = YES;
                self.statusDetailLabel.text = @"订单取消已扣除费用";
                break;
            case 7:
                self.cancelView.hidden = self.cancelTipLabel.hidden = self.deleteButton.hidden = NO;
                self.cancelTimeView.hidden = self.commentView.hidden =   self.payTimeView.hidden =self.unConfirmTipView.hidden = YES;
                self.statusDetailLabel.text = @"已关闭";
                break;
                
            default:
                break;
        }
    }
}
#pragma mark - 生活预约的隐藏
-(void)updateRWStatus:(NSInteger)status{
    if (status==2) {
        // 待回收
        self.cancelButton.hidden = [_detailModel.cancelTime integerValue] <= 0;
        self.finishButton.hidden = NO;
        
        self.unConfirmTipView.hidden = NO;
        self.reserveTimeView.hidden = NO;
        self.startTimeView.hidden = YES;
        self.topPayTimeView.hidden = YES;
        self.cancelTipLabel.hidden = YES;
        
        self.commentView.hidden = YES;
        
        self.cancelView.hidden = YES;
        
        self.payTimeView.hidden = YES;
        self.cancelTimeView.hidden = YES;
        
    }
    else if (status==3) {
        // 已完成
        self.deleteButton.hidden = NO;
        
        self.unConfirmTipView.hidden = YES;
        self.reserveTimeView.hidden = YES;
        self.startTimeView.hidden = NO;
        self.topPayTimeView.hidden = NO;
        self.cancelTipLabel.hidden = YES;
        
        self.commentView.hidden = !_detailModel.commentVO.userId.length;
        
        self.cancelView.hidden = YES;
        
        self.payTimeView.hidden = NO;
        self.cancelTimeView.hidden = YES;
    }
    else {
        // 已取消
        self.deleteButton.hidden = NO;
        
        self.unConfirmTipView.hidden = YES;
        self.reserveTimeView.hidden = YES;
        self.startTimeView.hidden = YES;
        self.topPayTimeView.hidden = YES;
        self.cancelTipLabel.hidden = NO;
        
        self.commentView.hidden = YES;
        
        self.cancelView.hidden = NO;
        
        self.payTimeView.hidden = YES;
        self.cancelTimeView.hidden = NO;
    }
}

- (void)showCancelReasonView
{
    if (!self.reasonView) {
        self.reasonView = [[[NSBundle mainBundle] loadNibNamed:@"CancelOrderView" owner:nil options:nil] firstObject];
        self.reasonView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.reasonView];
    }
    [self.reasonView show];
    WeakSelf
    [self.reasonView setDidSelectBlock:^(NSString * _Nonnull selectedStr, NSString * _Nonnull ID) {
        [weakSelf requestForCancelOrder:ID reasonStr:selectedStr];
    }];
}
#pragma mark - 按钮的点击
- (IBAction)buttonsAction:(UIButton *)sender {
    NSInteger tag = sender.tag;
    /// 0:取消  1:完成   2删除  3:出发
    WeakSelf
    if (tag==0) {
        [weakSelf showCancelReasonView];
    }
    else if (tag==1) {
        FinishOrderController *vc = [[FinishOrderController alloc] init];
        vc.orderId = self.orderId;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }
    else if (tag==2) {
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForDeleteOrder];
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
    }else if (tag==5) {
        FinishOrderController *vc = [[FinishOrderController alloc] init];
        vc.orderId = self.orderId;
        vc.type = 1;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    } else if (tag==3) {
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"出发后不可取消订单？" titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForLetGoOrder:self.orderId];
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
    }
}

#pragma mark - 联系用户
- (IBAction)callButtonAction:(id)sender {
    [LJTools call:self.detailModel.telphone];
}

#pragma mark - 导航
- (IBAction)guideViewTap:(id)sender {
    [self addressClick];
}
#pragma mark - 地址点击
-(void) addressClick {
    if (([self.detailModel.status integerValue]==2&&[User getAuthMasterType].intValue == 3 )||([User getAuthMasterType].intValue == 2)) {
        LMMapViewController *vc = [[LMMapViewController alloc] init];
        vc.coordinate = CLLocationCoordinate2DMake([_detailModel.latitude floatValue], [_detailModel.longitude floatValue]);
        vc.city = _detailModel.city;
        vc.district = _detailModel.area;
        vc.address = _detailModel.street;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - 倒计时
- (void)startTimer:(int)time
{
    __block int timeout=time;
    //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
//            [self cancelCountdownWithEndString:endStr];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.countDownView.hidden = YES;
                self.cancelButton.hidden = YES;
            });
        }else{
            int seconds = timeout;
            //format of minute
            NSString *str_minute = [NSString stringWithFormat:@"%02d",(seconds%3600)/60];
            //format of second
            NSString *str_second = [NSString stringWithFormat:@"%02d",seconds%60];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.countDownLabel.text = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

@end
